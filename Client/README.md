USAGE
=========================
La librairi client contient toutes les fonctions utiles au développement Arduino.

Installation 
=========================
Dans l'IDE Arduino : 
Croquis > Inclure une bibliothèque > Ajouter la bibliothèque .Zip



LICENSE 
=========================


[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
