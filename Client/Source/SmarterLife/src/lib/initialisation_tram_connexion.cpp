#include "initialisation_tram_connexion.h"

/*
    initialise la tram de connexion
    envoi le num_module pour vÃ©rification et le type de valeur mesurer par le module
*/
bool initialisation_tram_connexion(Tram *_t, char *_num_module, char _value[][SUBTYPE_SIZE])
{
  int i = 0;
  Data *d = NULL;

  /*Initialise data */
  d = (Data *) malloc(_t->total_data * sizeof(*d));
  if (d == NULL)
  {
    DEBUG_PRINTF("d malloc failed");
    free(d);
    return false;
  }

  d[0] = {TYPE_MODULE, (char *) _num_module};
  for (i = 1 ; i < _t->total_data ; i++)
  {
    d[i] = {TYPE_PARAMETRE, (char *) _value[i-1]};
  }

  _t->type = TRAM_CONNEXION;
  _t->data = d;

  return true;
}
