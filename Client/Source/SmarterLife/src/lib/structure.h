#ifndef _STRUCTURE_H
#define _STRUCTURE_H

/* La valeur null du numero de module lors de la lecture eeprom */
#define TAILLE_NUM_MODULE_NULL 6
#define NUM_MODULE_CHAR_NULL "65535"

#define MIN_TRAM_SIZE   4 + TYPE_SIZE + TOTAL_SIZE /* OPEN_COM + OPEN_TRAM + END_TRAM + CLOSE_COM */
#define MAX_TRAM_SIZE   1024
#define TAILLE_MAX_VALUE 32

typedef struct Data {
  int param;    /* Will contain a pointer of the param name*/
  char *value;    /* Will contain a pointer of the value */
} Data;

typedef char Stram[MAX_TRAM_SIZE];

typedef struct Tram {
  int type;       /* Type of communication  */
  int total_data;   /* Total of data to send */
  struct Data *data;     /* Array of Data */

  Stram string_tram;
} Tram;

/* Parametre need for connexion obtain with initialistaion storage in eeprom */
typedef struct Param_conn {
  char num_module[TAILLE_NUM_MODULE_NULL] = NUM_MODULE_CHAR_NULL;
  char ip_serveur[20]; /* ip of domotique server */
  char port_serveur[8]; /* port of domotique server */
} Param_conn;


#endif /* _STRUCTURE_H included */
