#ifndef _COMMUNICATION_H
#define _COMMUNICATION_H

#include "type_tram.h"
#include "type_valeur_tram.h"
#include "structure.h"
#include "error.h"

#define DELAY_ENTRE_DONNEE_COMM 10 

#define DELAY_RECOIT_TRAM_COMM 10000 

/** 
 * Recuperation of value send by domotique server
 * 
 * %param _t : Tram fill
 * %param _param_conn : Parametre of Type_valeur_tram
 * %param _value : Value associate in tram char 
 *
 * %return : true or false
 */
class SL {
public:
	static bool SL_recup_valeur_communication(Tram * _t, int _param_conn, char *_value);
	static bool SL_recup_valeur_communication(Tram * _t, int _param_conn, float *_value);
	static bool SL_recup_valeur_communication(Tram * _t, int _param_conn, int *_value);
	static bool SL_recup_valeur_communication(Tram * _t, int _param_conn, bool *_value);
};

#endif /* _COMMUNICATION_H included */
