#include "util.h"

/*Variable globale */
WiFiClient client;
Tram g_tram;
Param_conn g_param_conn;

bool SL_Setup(WiFiClient *_client, int _taille_data_conn, int *_data_connexion)
{
  EEPROM.begin(8); /* EEPROM pour stocker la donnee en memoire 4 addresses de stockage */
  int init_done = false;
  char *ssid = NULL;
  char *pass = NULL;

  DEBUG_PRINTLN("------- DEBUT INIT ----------");

  /* Flag pour refaire l'init */
  attachInterrupt(digitalPinToInterrupt(PIN_INIT), reset_eeprom, RISING);

  /* Charge la valeur des param est NULL si pas de valeur */
  if (lecture_eeprom(ADD_EEPROM, &g_param_conn) == false)
  {
    DEBUG_PRINTLN("Erreur dans la lecture eeprom");
    return false;
  }

  /* Si module inconnu on le creer rajoute une condition avec un bouton ?*/
  
  if (strcmp(g_param_conn.num_module, NUM_MODULE_CHAR_NULL) == 0)
  {
    if ((init_done = gere_init(&g_tram, _client, _taille_data_conn, _data_connexion)) != true)
    {
      DEBUG_PRINTLN("Erreur dans la gere init");
      return false;
    }
    DEBUG_PRINTLN("gere init effectue");
  }

//  if (verification_num_module_init(g_param_conn.num_module) == false)
//  {
//    DEBUG_PRINTLN("numero de module incoherent");
//    return false;
//  }

  if ( init_done ) {
    recup_valeur_init(&g_tram, &ssid, &pass, &g_param_conn);
    free(g_tram.data);

    if (ecriture_eeprom(ADD_EEPROM, &g_param_conn) == false)
      return false;

    if (premiere_connexion_wifi(ssid, pass) == false)
      return false;
  }

  DEBUG_PRINTLN("Init end !");
  return true;
}

bool SL_verif_connexion(int _taille_data_conn, int *_data_connexion)
{
  int reco = false;

  DEBUG_PRINTLN("------- DEBUT CONNEXION ----------");
  
  if ((reco = reconnect()) == false) return false;

  /* je ne lis eeprom que si elle n'est pas deja charge */
  if (strcmp(g_param_conn.num_module, NUM_MODULE_CHAR_NULL) == 0)
  {
    lecture_eeprom(ADD_EEPROM, &g_param_conn);
  }

  if (verification_num_module_connexion(g_param_conn.num_module) == false)
  {
    DEBUG_PRINTLN("numero de module incoherent");
    /* On relance un STD_Setup() ou on fait sleep le module ? */
    return false;
  }

  if (gere_connexion(&g_tram, &client, &g_param_conn, _taille_data_conn, _data_connexion) == false)
  {
    DEBUG_PRINTLN("erreur dans gere connexion");
    return false;
  }
  
  /* On tempo pour par envoyer trop rapidement une tram de communication */
  delay(500);
  return true;
}

/*
    recoit une tram
*/
int SL_recoit_tram(Tram *_tram, WiFiClient *_client, int _delay_entre_donnee, int _delay_timeout)
{
  char value = ' ';
  int tmp = _delay_timeout;

  /* Vide la tram d'avant pour ne pas avoir de merde dedans */
  strcpy(_tram->string_tram, "");

  /* attend jusqu'a que le client envoie une donnee et timeout si pas de donnee recu*/
  while (!_client->available()) {
    if (tmp <= 0) {
      DEBUG_PRINTLN("Timeout SL_recoit_tram");
      return tmp;
    }

    tmp -= _delay_entre_donnee;
    delay(_delay_entre_donnee);
  }

  tmp = -1;
  while (_client->available() > 0) {
    value = _client->read();
    if (value != '\r' && value != '\n' && value != '\0') {
      ++tmp;
      _tram->string_tram[tmp] = value;

    }
  }

  DEBUG_PRINTF("tram recu : %s \n", _tram->string_tram);
  _client->flush();

  return tmp;
}


/* envoi une tram */
bool SL_envoi_tram(Tram *_tram, WiFiClient *_client)
{
  _client->print(_tram->string_tram);
  DEBUG_PRINTF("envoi de la tram : %s\n", _tram->string_tram);
  delay(DELAY_ENVOI_TRAM);
  return true;
}

/* Entre les parametre meme sens que data_connexion avec NULL en dernier arguemnts*/
bool SL_make_tram_char(Tram *_t, int _taille_data_conn, int *_data_connexion, char *_data, ...)
{
  int i = 0;
  Data *d = NULL;
  va_list ap;
  
  /* Test pour malloc */
  if (_taille_data_conn == 0) {
    DEBUG_PRINTLN("erreur data_connexion vide");
    return false;
  }
	  
  /*Initialise data */
  _t->total_data = _taille_data_conn;
  d = (Data *) malloc(_t->total_data * sizeof(*d));
  if (d == NULL)
  {
    DEBUG_PRINTF("malloc failed\n");
    free(d);
    return false;
  }
  
  /* Copie dans un tableau de char */
  va_start(ap, _data);
  while(_data != NULL)
  {
    d[i] = {_data_connexion[i], _data};
    
    /* Test si la taille alloué est identique a la taille de data_connexion */
    if(i > _taille_data_conn) 
    {
        DEBUG_PRINTF("Pas meme taille entre data connexion et data ...\n");
        free(d);
        return false;
    }
    i++;
    _data = va_arg(ap, char*);
  }
  va_end(ap);
	
  _t->data = d;
  _t->type = TRAM_COMMUNICATION;

  return true;
}

/*
    vide la memoire esp
*/
void ICACHE_RAM_ATTR reset_eeprom()
{
  DEBUG_PRINTLN("Reboot eeprom and ESP");

  ecriture_eeprom_module(ADD_EEPROM, NUM_MODULE_CHAR_NULL);
  DEBUG_PRINTLN("eeprom null value ok");

  ESP.restart();

  /*TODO Revoir reboot */
  /* resetFunc(); */
}
