#ifndef _PARSEUR_H
#define _PARSEUR_H

/*
 * Define du parseur 
 */
 
#include "structure.h"
#include "type_tram.h"
#include "type_valeur_tram.h"
#include "error.h"

#define OPEN_COM     '1'
#define OPEN_TRAM     '2'
#define CLOSE_TRAM    '3'
#define CLOSE_COM     '4'
#define DELIM_BLOCK   "|"
#define DELIM_DATA    ":"

#define TYPE_SIZE     3
#define SUBTYPE_SIZE  3
#define TOTAL_SIZE    3

#define SIZE_OF_ARRAY(X) sizeof(X) / sizeof(X[0])


typedef bool (Checker)(Tram *_t, char *_data, int _data_lng);

/** 
 *  Check the size of _data
 *
 *  %param _data : Data to check (UNUSED)
 *  %param _data_lng : Length of the data
 *  %param _t : Struct to fill (UNUSED)
 *
 *  %return true upon success false otherwise
 */
bool checkSize(Tram *_t, char *_data, int _data_lng);

/** 
 * Check the start of tram
 *
 * %param _data : Data to check
 * %param _data_lng : Length of the data (UNUSED)
 * %param _t : Struct to fill
 *
 * %return true upon success false otherwise
 * -> Fill 'type' in _t
 */
bool checkStart(Tram *_t, char *_data, int _data_lng);

/** 
 * Check the end of the tram
 *
 * %param _data : Data to check
 * %param _data_lng : Length of the data
 * %param _t : Struct to fill
 *
 * %return true upon success false otherwise
 * -> Fill 'total_data' in _t
 */
bool checkEnd(Tram *_t, char *_data, int _data_lng);

/** 
 * Parse the actual data
 *
 * %param _data : Data to check
 * %param _data_lng : Length of the data
 * %param _t : Struct to fill
 * 
 * %return true upon success false otherwise
 * -> Fill 'data' in _t
 */
bool parseData(Tram *_t, char *_data, int _data_lng);

/** 
 * Call all check / parsing function
 *
 * %param _data : Data to check
 * %param _data_lng : Length of the data
 * %param _t : Struct to fill
 * 
 * %return true upon success false otherwise
 */
bool SL_parse(Tram *_t, char *_data, int _data_lng);

/** 
 * Call all check / create tram from data
 * 
 * %param _t : Struct to fill
 * %param _data : Data to make
 *
 * %return : true or false
 */
bool SL_toTram(Tram *_t, char *_data);

/** 
 * parsing function ip
 * 
 * %param _str : ip to parse
 * %param _ip_out : result of parse
 *
 * %return : true or false
 */
bool parseur_ip(char *_str, int *_ip_out);

#endif /* _PARSEUR_H included */
