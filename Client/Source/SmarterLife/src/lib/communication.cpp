#include "communication.h"

/*
   Recuperation des valeurs de la communication
*/
char* __get_data(Tram *_t, int _param_conn) 
{
	int i = 0;

	for(; i < _t->total_data; i++)
		if(_t->data[i].param == _param_conn) 
			return _t->data[i].value;

	return NULL;
}

bool SL::SL_recup_valeur_communication(Tram * _t, int _param_conn, char *_value)
{
	char *d = __get_data(_t, _param_conn);
	if(d == NULL) return false;

	strcpy(_value, d);

	return true;
}

bool SL::SL_recup_valeur_communication(Tram * _t, int _param_conn, float *_value)
{
	char *d = __get_data(_t, _param_conn);
	if(d == NULL) return false;

	*_value = atof(d);

	return true;
}

bool SL::SL_recup_valeur_communication(Tram * _t, int _param_conn, int *_value)
{
	char *d = __get_data(_t, _param_conn);
	if(d == NULL) return false;

	*_value = atoi(d);

	return true;
}

bool SL::SL_recup_valeur_communication(Tram * _t, int _param_conn, bool *_value)
{
	char *d = __get_data(_t, _param_conn);
	if(d == NULL) return false;

	*_value = (atoi(d) != 0);

	return true;
}
