#ifndef _INITIALISATION_TRAM_CONNEXION_H
#define _INITIALISATION_TRAM_CONNEXION_H

#include "type_tram.h"
#include "type_valeur_tram.h"
#include "structure.h"
#include "parseur.h"
#include "error.h"

#define SUBTYPE_SIZE  3
/** 
 * Create tram for connexion
 * 
 * %param _t : Struct to fill
 * %param _num_module : Number of client associte with server
 * %param _value : Data_connexion parametre used by server
 *
 * %return : true or false
 */
bool initialisation_tram_connexion(Tram *_t, char *_num_module, char _value[][SUBTYPE_SIZE]);

#endif /* _INITIALISATION_TRAM_CONNEXION_H included */
