#include "eeprom.h"

void(* resetFunc) (void) = 0;

bool ecriture_eeprom_module(int _addr, char *_num_module)
{
  /* Change le type en int pour le stockage */
  int data_tmp = atoi(_num_module);

  /* ecriture du num_module */
  EEPROM.write(_addr, highByte(data_tmp));
  EEPROM.write(_addr + 1, lowByte(data_tmp));
  EEPROM.commit();
  return true;
}

/* ecriture de l'ip */
bool ecriture_eeprom_ip(int _addr, char *_ip)
{
  int ip_out[4];
  parseur_ip(_ip, ip_out);
  EEPROM.write(_addr + 2, ip_out[0]);
  EEPROM.write(_addr + 3, ip_out[1]);
  EEPROM.write(_addr + 4, ip_out[2]);
  EEPROM.write(_addr + 5, ip_out[3]);
  EEPROM.commit();
  return true;
}

bool ecriture_eeprom_port(int _addr, char *_port)
{
  /* ecriture de lu port */
  int data_tmp = atoi(_port);
  EEPROM.write(_addr + 6, highByte(data_tmp));
  EEPROM.write(_addr + 7, lowByte(data_tmp));
  EEPROM.commit();
  return true;
}

/*
    eciture dans la memoire de esp
*/
bool ecriture_eeprom(int _addr, Param_conn *_param)
{
  DEBUG_PRINTF("ecriture eeprom ... \n");
  ecriture_eeprom_module(_addr, _param->num_module);
  ecriture_eeprom_ip(_addr, _param->ip_serveur);
  ecriture_eeprom_port(_addr, _param->port_serveur);
  DEBUG_PRINTF("ecriture eeprom ok \n");
  return true;
}


/*
    lecture eeprom en char (sans limitation)
*/
bool lecture_eeprom_module(int _addr, char *_data)
{
  int data_int;
  data_int = word(EEPROM.read(_addr), EEPROM.read(_addr + 1));

  sprintf(_data, "%d", data_int);
  return true;
}

/*
    lecture eeprom en char (sans limitation)
*/
bool lecture_eeprom_ip(int _addr, char *_data)
{

  int data_int[4];
  data_int[0] = EEPROM.read(_addr + 2);
  data_int[1] = EEPROM.read(_addr + 3);
  data_int[2] = EEPROM.read(_addr + 4);
  data_int[3] = EEPROM.read(_addr + 5);

  sprintf(_data, "%d.%d.%d.%d", data_int[0], data_int[1], data_int[2], data_int[3]);
  return true;
}

/*
    lecture eeprom en char (sans limitation)
*/
bool lecture_eeprom_port(int _addr, char *_data)
{
  int data_int;
  data_int = word(EEPROM.read(_addr + 6), EEPROM.read(_addr + 7));

  sprintf(_data, "%d", data_int);
  return true;
}

bool lecture_eeprom(int _addr, Param_conn *_param)
{
  DEBUG_PRINTLN("lecture eeprom ... \n");
  lecture_eeprom_module(_addr, _param->num_module);
  lecture_eeprom_ip(_addr, _param->ip_serveur);
  lecture_eeprom_port(_addr, _param->port_serveur);
  DEBUG_PRINTF("lecture eeprom ok num_module : %s ip serveur : %s port_serveur :  %s\n",
               _param->num_module, _param->ip_serveur, _param->port_serveur);
  DEBUG_PRINTLN("lecture eeprom ok \n");
  return true;
}
