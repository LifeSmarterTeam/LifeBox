#ifndef _UTIL_H
#define _UTIL_H

/*
	Inclure pour avoir les references de base arduino
*/
#include <Arduino.h>
#include <ESP8266WiFi.h>

#include "type_tram.h"
#include "structure.h"
#include "type_valeur_tram.h"
#include "parseur.h"
#include "connexion.h"
#include "init.h"
#include "communication.h"
#include "eeprom.h"
#include "error.h"

/*Variable globale */
extern WiFiClient client;
extern Tram g_tram;
extern Param_conn g_param_conn;

#define STR(X) SSTR(X)
#define SSTR(X) #X

/* addresse de lecture de l'eeprom */
#define ADD_EEPROM 0

/* Temps pour chaque envoie de tram evite la surcharge de la comm (deux tram en une) */
#define DELAY_ENVOI_TRAM 1

/* Bouton reboot */
#define PIN_INIT D6 /* Arbitraire */

/** 
 *  Make initialisation of client
 * 
 * %param _client :
 *
 * %return : 
 */
bool SL_Setup(WiFiClient *_client, int _taille_data_conn, int *_data_connexion);

/** 
 * Make connexion of client verifacation wifi server, ...
 *
 * %param _data_connexion : data_connexion parameter send to server
 *
 * %return : true or false
 */
bool SL_verif_connexion(int _taille_data_conn, int *_data_connexion);

/** 
 * reveive tram from server with delay in tram.string_tram
 * 
 * %param _tram : tram for receive information
 * %param _client : client of WiFiClient 
 * %param _delay_entre_donnee : delay in reveive data
 * %param _delay_timeout : delay for timeout of function
 *
 * %return : lengt of reveive data 
 */
int SL_recoit_tram(Tram *_tram, WiFiClient *_client, int _delay_entre_donnee, int _delay_timeout);

/** 
 * Send good tram to server from tram.string_tram
 * 
 * %param _tram : tram for send information
 * %param _client : client of WiFiClient 
 *
 * %return : true or false 
 */
bool SL_envoi_tram(Tram *_tram, WiFiClient *_client);

/** 
 * Make data of tram with char and data_connexion
 * 
 * %param _t : Tram will be make 
 * %param _data_connexion : data_connexion to make tram
 * %param _data : data want in tram same order of data_connexion
 * %param ... : to use n parameter to make tram
 *
 * %return : true or false
 */
bool SL_make_tram_char(Tram *_t, int _taille_data_conn, int *_data_connexion, char *_data, ...);

/** 
 * reset eeprom
 *
 * %return : true or false
 */
void reset_eeprom();

#endif /* _UTIL_H included */
