#include "init.h"

/*
    Parametre pour le serveur init
*/

IPAddress IP_INIT(192, 168, 2, 5);
IPAddress GATEWAY_INIT(192, 168, 1, 1);
IPAddress SUBNET_INIT(255, 255, 255, 0);

/*
   Recuperation des valeurs de l'init ssid, pass, num_module IP, ...
*/
void recup_valeur_init(Tram * _t, char **_ssid, char **_pass, Param_conn *_param)
{
  int i = 0;

  for (i = 0; i < _t->total_data; i++)
  {
    switch (_t->data[i].param)
    {
      case TYPE_SSID:
        *_ssid = _t->data[i].value;
        continue;

      case TYPE_PASSWORD:
        *_pass = _t->data[i].value;
        continue;

      case TYPE_MODULE:
        strcpy(_param->num_module, _t->data[i].value);
        continue;

      case TYPE_IP:
        strcpy(_param->ip_serveur, _t->data[i].value);
        continue;

      case TYPE_PORT:
        strcpy(_param->port_serveur, _t->data[i].value);
        continue;

      case TYPE_PARAMETRE:
      default:
        DEBUG_PRINTF("Erreur, type [%i] inconnu \n", _t->data[i].param);
        break;
    }
  }
}

/*
    Creation du reseau wifi pour le reseau
*/
bool creation_wifi_init()
{

  /*Creer le reseau */
  DEBUG_PRINTLN("creation du reseau");

  /*parametre le serveur */
  WiFi.mode(WIFI_AP); //ou _AP
  WiFi.softAPConfig (IP_INIT, GATEWAY_INIT, SUBNET_INIT);
  WiFi.softAP(SSID_INIT, PASSWORD_INIT); // (SSID,PASS,CHANNEL,HIDDEN,MAX CLIENTS)

  DEBUG_PRINTLN("IP address: ");
  DEBUG_PRINTLN(WiFi.softAPIP());
  DEBUG_PRINTLN("PORT: ");
  DEBUG_PRINTLN(PORT);
  return true;
  /* Fin creation reseau */
}

/*
    Creation serveur sur le reseau ou pas
*/
bool creation_serveur_init(WiFiServer *_server)
{

  _server->begin();
  DEBUG_PRINTLN("Serveur init lance !");
  return true;
}

/*
    test des connexion d'un client
*/
bool connexion_client_init(WiFiServer *_server, WiFiClient *_client)
{
  int t0 = millis();
  int t = 0;

  while (t < DELAY_INIT) {
    *_client = _server->available();

    if (_client->connected()) {
      DEBUG_PRINTLN("Client connecte !");
      return true;
    }

    DEBUG_PRINTLN("attente d'un client ...");
    delay(DELAY_ENTRE_CLIENT);
    t = millis() - t0;
  }

  DEBUG_PRINTF("Connexion timeout delay depasse %d < %d \n", t, DELAY_INIT);
  return false;
}

/*
    recoit la tram init gere le deconnxion du client
*/
int SL_recoit_tram_init(Tram *_tram, WiFiClient *_client)
{
  int total_read_init = 0;

  total_read_init = SL_recoit_tram(_tram, _client, DELAY_ATTENTE_DONNEE_INIT, DELAY_RECOIT_TRAM_INIT);

  DEBUG_PRINTLN("Client deconnecte");
  _client->stop();

  return total_read_init;
}

/*
  Verifiaction que le num_module est bien bon
*/
bool verification_num_module_init(char *_num_module)
{
  if (strcmp(_num_module, NUM_MODULE_CHAR_NULL) == 0)
  {
    return true;
  }
  else
  {
    return false;
  }
}


bool deconnexion_serveur_init(WiFiServer *_server)
{
  _server->close();
  return true;
}

/*
  Gere la deconnexion du mode ap
*/
bool deconnexion_ap()
{
  WiFi.disconnect();
  WiFi.mode(WIFI_OFF);
  WiFi.forceSleepBegin();
  return true;
}

bool premiere_connexion_wifi(char *_ssid, char *_pass)
{
  int t = 0;
  int t0 = millis();

  String ssid_str = _ssid;
  String pass_str = _pass;

  DEBUG_PRINTF("nouveau ssid\n[%s]\n", _ssid);
  DEBUG_PRINTLN("Ancien ssid :");
  DEBUG_PRINTLN( WiFi.SSID());
  DEBUG_PRINTF("nouveau pass\n[%s]\n", _pass);
  DEBUG_PRINTLN("Ancien pass :");
  DEBUG_PRINTLN( WiFi.psk());

  DEBUG_PRINTLN("First connect to ");

  WiFi.mode(WIFI_STA);
  delay(100);
  if (WiFi.SSID() == ssid_str && WiFi.psk() == _pass)
  {
    WiFi.begin();
    DEBUG_PRINTLN("Wifi.begin ancien ssid & pass identique");
  }
  else
  {
    WiFi.begin(_ssid, _pass);
    DEBUG_PRINTLN("Wifi.begin ok nouveau ssid & pass");
  }

  /* premiere connexion Ã  la wifi. on ajoute un timeout ? */
  while (WiFi.status() != WL_CONNECTED) {
    delay(DELAY_WIFI_CONNEXION);
    DEBUG_PRINTF(".");
    if (t > PREMIERE_CONNEXION_WIFI)
    {
      DEBUG_PRINTLN("\nErreur connexion STA - Redemerage ESP");
      delay(DELAY_WIFI_CONNEXION);
      reset_eeprom();
    }
    t = millis() - t0;
  }
  DEBUG_PRINTF("\n");

  DEBUG_PRINTLN("\nWiFi connected");
  DEBUG_PRINTLN("IP address locale esp: ");
  DEBUG_PRINTLN(WiFi.localIP());

  return true;
}

bool gere_init(Tram *_tram, WiFiClient *_client, int _taille_data_conn, int *_data_connexion)
{
  int total_read = 0;
  int i = 0;
  WiFiServer server(PORT);
  
  _tram->total_data = _taille_data_conn;
  char param[_tram->total_data][SUBTYPE_SIZE];

  for (i = 0 ; i < _tram->total_data ; i++)
  {
    sprintf(param[i], "%d", _data_connexion[i]);
  }
  
  /* Creation wifi & serveur & attend la connexion du client*/
  if (creation_wifi_init() != true) return false;
  if (creation_serveur_init(&server) != true) return false;

  if (connexion_client_init(&server, _client) != true) return false;

  /* Initialise la tram init pour le serveur & parse en tram */

  if (initialisation_tram_init(_tram, param) != true) return false;
  DEBUG_PRINTLN("initialisation tram init effectue");

  if (SL_toTram(_tram, _tram->string_tram) != true) return false;
  DEBUG_PRINTLN("convertion de la tram pour envoi effectue");

  /* Envoi de la tram */
  if (SL_envoi_tram(_tram, _client) != true) return false;
  DEBUG_PRINTLN("envoi init effectue");

  free(_tram->data);

  /*Recoit la tram du serveur (SSID & MDP & IP) */
  if ((total_read = SL_recoit_tram_init(_tram, _client)) < 0) return false;
  DEBUG_PRINTLN("reception init effectue");
 
  /* Deconnexion de la wifi  */
  if (deconnexion_serveur_init(&server) != true) return false;
  DEBUG_PRINTLN("deconnexion serveur effectue");

  if (deconnexion_ap() != true) return false;
  DEBUG_PRINTLN("deconnexion ap effectue");

  if (SL_parse(_tram, _tram->string_tram, total_read) != true) return false;
  DEBUG_PRINTLN("parse tram init effectue");

  return true;
}
