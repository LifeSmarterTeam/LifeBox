#ifndef _EEPROM_H
#define _EEPROM_H

#include <EEPROM.h>
#include "structure.h"
#include "parseur.h"
#include "error.h"

/* addresse de lecture de l'eeprom */
#define ADD_EEPROM 0

/** 
 * Write in eeprom module number
 * 
 * %param _addr : eeprom addresse 
 * %param _num_module : module number to storage
 *
 * %return : true or false
 */
bool ecriture_eeprom_module(int _addr, char *_num_module);

/** 
 * Write in eeprom ip server
 * 
 * %param _addr : eeprom addresse 
 * %param _ip : ip to storage
 *
 * %return : true or false
 */
bool ecriture_eeprom_ip(int _addr, char *_ip);

/** 
 * Write in eeprom port server
 * 
 * %param _addr : eeprom addresse 
 * %param _port : port to storage
 *
 * %return : true or false
 */
bool ecriture_eeprom_port(int _addr, char *_port);

/** 
 * Gestion of writing in eeprom
 * 
 * %param _addr : eeprom addresse 
 * %param _param : Param_conn fill
 *
 * %return : true or false
 */
bool ecriture_eeprom(int _addr, Param_conn *_param);

/** 
 * 
 * 
 * %param _addr : eeprom addresse 
 * %param _data : module number to read
 *
 * %return : true or false
 */
bool lecture_eeprom_module(int _addr, char *_data);

/** 
 * 
 *  
 * %param _addr : eeprom addresse 
 * %param _data : ip to read
 *
 * %return : true or false
 */
bool lecture_eeprom_ip(int _addr, char *_data);

/** 
 * 
 * 
 * %param _addr : eeprom addresse 
 * %param _data : port to read
 *
 * %return : true or false
 */
bool lecture_eeprom_port(int _addr, char *_data);

/** 
 * 
 * 
 * %param _addr : eeprom addresse 
 * %param _param : Param_conn fill
 *
 * %return : true or false
 */
bool lecture_eeprom(int _addr, Param_conn *_param);

#endif /* _EEPROM_H included */
