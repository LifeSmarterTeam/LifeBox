#include "parseur.h"

bool checkSize( Tram *_t, char *_data, int _data_lng)
{
  (void) _data;
  (void) _t;

  DEBUG_PRINTF("Calling \n");

  if (_data_lng < MIN_TRAM_SIZE && _data_lng > MAX_TRAM_SIZE) {
    DEBUG_PRINTF("Wrong data length : [%d] < [%d] < [%d] \n", MIN_TRAM_SIZE, _data_lng, MAX_TRAM_SIZE);
    return false;
  }

  return true;
}

bool checkStart(Tram *_t, char *_data, int _data_lng)
{
  int tram_type;
  char tmp[TYPE_SIZE + 1] = {0};

  (void) _data_lng;

  DEBUG_PRINTF("Calling \n");

  if (_data[0] != OPEN_COM) {
    DEBUG_PRINTF("Wrong data openner [%c] waiting [%c] \n", _data[0], OPEN_COM);
    return false;
  }

  if (_data[TYPE_SIZE + 1] != OPEN_TRAM) {
    DEBUG_PRINTF("Wrong tram openner [%c] waiting [%c] \n", _data[TYPE_SIZE + 1], OPEN_TRAM);
    return false;
  }

  strncpy(tmp, _data + 1, TYPE_SIZE);
  tram_type = atoi(tmp);

  if (tram_type < 0) {
    DEBUG_PRINTF("Wrong type tram [%d] from [%s] \n", tram_type, tmp);
    return false;
  }

  _t->type = tram_type;

  return true;
}

bool checkEnd(Tram *_t, char *_data, int _data_lng)
{
  int total_data;
  char tmp[TYPE_SIZE + 1] = {0};

  DEBUG_PRINTF("Calling\n");

  if (_data[_data_lng] != CLOSE_COM) {
    DEBUG_PRINTF("Wrong data closer [%c] waiting [%c] \n", _data[_data_lng], CLOSE_COM);
    return false;
  }

  if (_data[_data_lng - TOTAL_SIZE - 1] != CLOSE_TRAM) {
    DEBUG_PRINTF("Wrong tram closer [%c] waiting [%c] \n", _data[_data_lng - TOTAL_SIZE - 1], CLOSE_TRAM);
    return false;
  }

  strncpy(tmp, _data + _data_lng - TOTAL_SIZE, TOTAL_SIZE);
  total_data = atoi(tmp);

  if (total_data < 0) {
    DEBUG_PRINTF("Wrong total data size [%d] from [%s] \n", total_data, tmp);
    return false;
  }

  _t->total_data = total_data;

  return true;
}

bool parseData( Tram *_t, char *_data, int _data_lng)
{
  int i, j;
  char *start, *substart;
  char *token, *subtoken;
  char *sv1, *sv2;

  _data[_data_lng - TOTAL_SIZE - 1] = 0;
  start = _data + TYPE_SIZE + 2; /* SOH & ETX */

  _t->data = (Data *) calloc(_t->total_data, sizeof(*(_t->data)));

  if (_t->data == NULL)
  {
    DEBUG_PRINTF("_t->data calloc failed");
    free(_t->data);
    return false;
  }

  for (i = 0; i < _t->total_data; i++, start = NULL) {
    token = strtok_r(start, DELIM_BLOCK, &sv1);
    if (token == NULL) {
      DEBUG_PRINTF("Something wrong [%d] token instead of [%d] \n", i, _t->total_data);
      return false;
    }

    for (j = 0, substart = token; j < 2; j++, substart = NULL) {
      subtoken = strtok_r(substart, DELIM_DATA, &sv2);
      if (subtoken == NULL) {
        DEBUG_PRINTF("Something wrong parsing [%s] \n", token);
        return false;
      }

      if ( j == 0 ) _t->data[i].param = atol(subtoken);
      else          _t->data[i].value = subtoken;
    }
  }

  return true;
}

bool SL_parse(Tram *_t, char *_data, int _data_lng)
{
  int i = 0;
  Checker *checker[] = {checkSize, checkStart, checkEnd, parseData};
  int checker_size = SIZE_OF_ARRAY(checker);

  DEBUG_PRINTF("Tram to parse [%s] size [%d] \n", _data, _data_lng);

  for (i = 0; i < checker_size; i++) {
    if ( checker[i]( _t, _data, _data_lng) == false ) return false;
  }

  DEBUG_PRINTF("Parsing OK - Type [%d] - Total [%d] \n", _t->type, _t->total_data);

  for (i = 0; i < _t->total_data; i++ )
    DEBUG_PRINTF("Param : [%d] - Value [%s] \n", _t->data[i].param, _t->data[i].value);

  return true;
}

bool SL_toTram(Tram *_t, char *_data)
{
  int i, pos;

  _data[pos] = OPEN_COM; pos++;
  pos += sprintf(_data + pos, "%0*d", TYPE_SIZE, _t->type);
  _data[pos] = OPEN_TRAM; pos++;

  for (i = 0; i < _t->total_data; i++ ) {
    pos += sprintf(_data + pos, "%0*d", SUBTYPE_SIZE, _t->data[i].param);
    _data[pos] = DELIM_DATA[0]; pos++;
    pos += sprintf(_data + pos, "%s", _t->data[i].value);

    if ( i < _t->total_data - 1) _data[pos++] = DELIM_BLOCK[0];
  }

  _data[pos] = CLOSE_TRAM; pos++;
  pos += sprintf(_data + pos, "%0*d", TYPE_SIZE, _t->total_data);
  _data[pos] = CLOSE_COM; pos++;
  _data[pos] = 0; pos++;

  for (i = 0; i < _t->total_data; i++ ) {
    DEBUG_PRINTF("Param : [%d] - Value [%s] \n", _t->data[i].param, _t->data[i].value);
  }

  return true;
}

bool parseur_ip(char *_str, int *_ip_out)
{
  unsigned char value[4] = {0};
  char *str2;
  size_t index = 0;

  str2 = _str; /* save the pointer */
  while (*_str) {
    if (isdigit((unsigned char)*_str)) {
      value[index] *= 10;
      value[index] += *_str - '0';
    } else {
      index++;
    }
    _str++;
  }
  
  _ip_out[0] = value[0];
  _ip_out[1] = value[1];
  _ip_out[2] = value[2];
  _ip_out[3] = value[3];

  return true;
}
