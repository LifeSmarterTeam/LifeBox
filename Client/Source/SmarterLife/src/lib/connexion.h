#ifndef _CONNEXION_H
#define _CONNEXION_H

#include "structure.h"
#include "parseur.h"
#include "error.h"
#include "util.h"
#include "initialisation_tram_connexion.h"

/*
  Librairies
*/

/*
  delay pour le timeout de la recherche du client en ms
*/
#define DELAY_RECONNECT 10000
#define DELAY_END_RECONNECT 100000

/* Temps attente entre chaque test que le client envoie une donnee */
#define DELAY_ATTENTE_DONNEE_CONNEXION 10

/* Temps ou on Ã©coute le serveur avant d'executer le code (important de bien le parametre il gere le temps de reception) */
#define DELAY_RECOIT_TRAM_CONNEXION 30000

/* Le nombre de fois que l'esp essaye de se connecter au serveur si il est deco */
#define NB_TENTATIVE_SERVEUR 10

/* Test pour la connexion du serveur */
#define SERVEUR_DECONNECTE 0
#define SERVEUR_OK 1
#define SERVEUR_RECONNECTE 2

#define CONNEXION_OK "Connexion_ok"

/** 
 * Recuperation of value connexion in tram
 * 
 * %param _t : Tram fill
 * %param _check : Value will be back to test
 *
 */
void recup_valeur_connexion(Tram * _t, char **_check);

/** 
 * Test module number connexion
 * 
 * %param _num_module : module number
 * %return : true or false
 */
bool verification_num_module_connexion(char *_num_module);

/** 
 * Reconnact to wifi network
 *
 * %return : true or false
 */
bool reconnect();

/** 
 *	Connexion with server domotique
 * 
 * %param _client : WiFiClient fill
 * %param _param : Param_conn fill
 *
 * %return : Value of connexion (Ok,reconnect, disconnect)
 */
int connexion_serveur(WiFiClient *_client, Param_conn *_param);

/** 
 * Gestion of all connexion 
 * 
 * %param _tram : Tram fill
 * %param _client : WiFiClient fill
 * %param _param_conn : Param_conn fill
 * %param _data_connexion : data_connexion send to server
 *
 * %return : true or false
 */
bool gere_connexion(Tram * _tram, WiFiClient * _client, Param_conn * _param_conn, int _taille_data_conn, int *_data_connexion);

#endif /* _CONNEXION_H included */
