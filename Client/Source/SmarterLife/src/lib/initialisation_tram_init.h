#ifndef _INITIALISATION_TRAM_INIT_H
#define _INITIALISATION_TRAM_INIT_H

/*
 * Valeur des init
 */
#include "type_tram.h"
#include "type_valeur_tram.h"
#include "structure.h"
#include "parseur.h"
#include "error.h"

#define SUBTYPE_SIZE  3
/** 
 * Create tram for initialisation 
 * 
 * %param _tram : Struct to fill
 *
 * %return : true or flase
 * allocation of _tram.data so need to free
 */
bool initialisation_tram_init(Tram *_t, char _value[][SUBTYPE_SIZE]);

#endif /* _INITIALISATION_TRAM_INIT_H included */