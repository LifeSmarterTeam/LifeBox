#include "initialisation_tram_init.h"

bool initialisation_tram_init(Tram *_t, char _value[][SUBTYPE_SIZE])
{
  int i = 0;
  Data *d = NULL;

  /*Initialise data */
  d = (Data *) malloc(_t->total_data * sizeof(*d));
  if (d == NULL)
  {
    DEBUG_PRINTF("d malloc failed");
    free(d);
    return false;
  }

  for (i = 0 ; i < _t->total_data ; i++)
  {
    d[i] = {TYPE_PARAMETRE, (char *) _value[i]};
  }

  /*Initialise tram */
  _t->type = TRAM_INIT;
  _t->data = d;
  
  return true;
}