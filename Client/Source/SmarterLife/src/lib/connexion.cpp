#include "connexion.h"

/*
   Recuperation des valeurs de l'init ssid, pass, num_module IP, ...
*/
void recup_valeur_connexion(Tram * _t, char **_check)
{
  int i = 0;

  for (i = 0; i < _t->total_data; i++)
  {
    switch (_t->data[i].param)
    {
      case TYPE_PARAMETRE:
        *_check = _t->data[i].value;

      default:
        DEBUG_PRINTLN("Erreur, type inconnu");
        break;
    }
  }
}

/*
  Verifiaction que le num_module est bien bon
*/
bool verification_num_module_connexion(char *_num_module) 
{
  if (strcmp(_num_module, NUM_MODULE_CHAR_NULL) != 0)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/*
    Reconnection de la wifi si elle est deconnecte
*/
bool reconnect()
{
  int t0 = millis();

  if (WiFi.status() != WL_CONNECTED)
  {
    DEBUG_PRINTLN("connecting to SSID :");
    DEBUG_PRINTLN( WiFi.SSID());
    DEBUG_PRINTLN("connecting to pass :");
    DEBUG_PRINTLN( WiFi.psk());

    WiFi.begin();
    while (WiFi.status() != WL_CONNECTED) {

      if ((millis() - t0) > DELAY_END_RECONNECT) {
        DEBUG_PRINTLN("Erreur, reconnect wifi timeout");
        return false;
      }
      DEBUG_PRINTF(".");
      delay(DELAY_RECONNECT);
    }
    DEBUG_PRINTLN("\nreconnection wifi ok");
  }

  DEBUG_PRINTLN("Wifi deja connecte !");
  return true;
}

int connexion_serveur(WiFiClient *_client, Param_conn *_param)
{
  int nb_tentavive = 0;
  DEBUG_PRINTLN("connecting to ");
  DEBUG_PRINTF("ip_serveur : %s, port_serveur : %s\n", _param->ip_serveur, _param->port_serveur);
  while (!_client->connected())
  {
    // Use WiFiClient class to create TCP connections
    if (!_client->connect(_param->ip_serveur, atoi(_param->port_serveur)))
    {
      nb_tentavive++;
      DEBUG_PRINTF("Serveur deconnecte tentative [%d]/[%d]\n", nb_tentavive, NB_TENTATIVE_SERVEUR);
      if (nb_tentavive == NB_TENTATIVE_SERVEUR)
      {
        _client->stop();
        return SERVEUR_DECONNECTE;
      }
      delay(1000);
    }
    else
    {
      DEBUG_PRINTLN("serveur reconnecte");
      return SERVEUR_RECONNECTE;
    }
  }
  DEBUG_PRINTLN("serveur deja connecte");
  return SERVEUR_OK;
}

bool gere_connexion(Tram * _tram, WiFiClient * _client, Param_conn * _param_conn, int _taille_data_conn, int *_data_connexion)
{
  int total_read = 0;
  char *check;
  int test_connexion = SERVEUR_DECONNECTE;

  /* Gestion de l'initialisation à faire dans une fonction ? Oui mais allocation .... */
  int i = 0;

  _tram->total_data = _taille_data_conn + 1;
  char param[_tram->total_data][SUBTYPE_SIZE];
  DEBUG_PRINTF("total tram gere connexion [%i] \n", _tram->total_data);
  for (i = 0 ; i < _tram->total_data - 1 ; i++)
  {
    sprintf(param[i], "%d", _data_connexion[i]);
  }
  /*Fin gestion */

  /* Connexion au serveur domototique */
  test_connexion = connexion_serveur(_client, _param_conn);

  /* Test le retour de la fonction connexion, selon les cas pas la meme gestion */
  switch (test_connexion) {
    case SERVEUR_OK:
      return true;
    case SERVEUR_RECONNECTE:
      break;
    case SERVEUR_DECONNECTE:
    default:
      return false;
  }

  /* Initialisation tram connexion */
  if (initialisation_tram_connexion(_tram, _param_conn->num_module, param) == false) return false;

  /* SL_toTram de la tram de connexion */
  if (SL_toTram(_tram, _tram->string_tram) == false) return false;

  /* Envoi la tram de connexion au serveur */
  if (SL_envoi_tram(_tram, _client) == false) return false;
    
  return true; 
     
  /* Recoit la tram de connexion du serveur */
  if ((total_read = SL_recoit_tram(_tram, _client, DELAY_ATTENTE_DONNEE_CONNEXION, DELAY_RECOIT_TRAM_CONNEXION)) < 1)
  {
    DEBUG_PRINTF("total read connexion <= 0 total_read : [%d]\n");
	_client->stop();
    return false;
  }

  /* Parse la tram de connexion */
  if (SL_parse(_tram, _tram->string_tram, total_read) != true) return false;
  
  /* Verification de la valeur de retour du serveur */
  recup_valeur_connexion(_tram, &check);
  if (strcmp(check, CONNEXION_OK) != 0)
    {
    DEBUG_PRINTF("check reception connexion incohérent recu:[%s] != attendu:[%s] /n", check, CONNEXION_OK);
    return false;
    }
    
  return true;
}
