#ifndef _INIT_H
#define _INIT_H

/*
  Librairies
*/
#include "structure.h"
#include "parseur.h"
#include "error.h"
#include "util.h"
#include "initialisation_tram_init.h"

/*
    Parametre pour le wifi de l'init
*/
#define SSID_INIT "abcdefg"
#define PASSWORD_INIT "123456789"
#define PORT 2018

/* delay pour le timeout de la recherche du client en ms */
#define DELAY_INIT 1000000
/* Delay entre chaque recherche de client*/
#define DELAY_ENTRE_CLIENT 1000

/* Temps attente entre chaque test que le client envoie une donnee */
#define DELAY_ATTENTE_DONNEE_INIT 10

/* Temps de reponse du serveur pour envoyer une tram reponse a l'init */
#define DELAY_RECOIT_TRAM_INIT 10000

/* Temps de entre tentative de deconnexion du mode ap */
#define DELAY_DECONNEXION_AP_TEST 1

/* Temps pour tester la premiere connexion avant reboot */
#define PREMIERE_CONNEXION_WIFI 100000

/* delay de connexion sur le wifi du serveur */
#define DELAY_WIFI_CONNEXION 500

/** 
 * Recupartion values of initialisation
 * 
 * %param _t : Struct to fill
 * %param _ssid : Name of network
 * %param _pass : Pass of network
 * %param _param : Struct to fill 
 */
void recup_valeur_init(Tram * _t, char **_ssid, char **_pass, Param_conn *_param);

/** 
 * Create Wifi network
 *
 * %return : true or false
 */
bool creation_wifi_init();

/** 
 * Create server on wifi network
 * 
 * %param _server : WiFiServer fill
 *
 * %return : true or false
 */
bool creation_serveur_init(WiFiServer *_server);

/** 
 * test connexion of client
 * 
 * %param _server : WiFiServer fill
 * %param _client : WiFiClient fill
 *
 * %return : true or false
 */
bool connexion_client_init(WiFiServer *_server, WiFiClient *_client);

/** 
 * Receive tram to server call SL_recoit_tram but with deconnexion of client
 * 
 * %param _client :
 * %param _tram : 
 *
 * %return : 
 */
int SL_recoit_tram_init(Tram *_tram, WiFiClient *_client);

/** 
 * Verification of number of module 
 * 
 * %param _num_module : number of module 
 *
 * %return : true or false
 */
bool verification_num_module_init(char *_num_module);

/** 
 * Deconnexion of initialisation server 
 * 
 * %param _server : WiFiServer fill
 *
 * %return : true or false
 */
bool deconnexion_serveur_init(WiFiServer *_server);

/** 
 * First connexion to wifi server domotique 
 * 
 * %param _ssid : name of network
 * %param _pass : pass of network
 *
 * %return : true or false
 */
bool premiere_connexion_wifi(char *_ssid, char *_pass);

/** 
 * Gestion of initialisation 
 * 
 * %param _tram : Tram fill
 * %param _client : WiFiClient fill
 *
 * %return : 
 */
bool gere_init(Tram *_tram, WiFiClient *_client, int _taille_data_conn, int *_data_connexion);

#endif /* _INIT_H included */
