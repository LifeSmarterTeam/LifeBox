#ifndef _ERROR_H
#define _ERROR_H

#include "util.h"
/*
  ------------DEBUG----------------
  decommenter pour etre en mode debug
*/

#define DEBUG

/*
  Degub message
*/
#ifdef DEBUG
#define DEBUG_PRINTLN Serial.println
#else
#define DEBUG_PRINTLN //
#endif

#ifdef DEBUG
#define DEBUG_PRINTF Serial.printf
#else
#define DEBUG_PRINTF //
#endif


#endif /* _ERROR_H included */
