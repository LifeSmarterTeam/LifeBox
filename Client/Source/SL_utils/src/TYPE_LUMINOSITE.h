#ifndef _TYPE_LUMINOSITE_H
#define _TYPE_LUMINOSITE_H

#include <Arduino.h>
#include "error.h"

/** 
 * Mesure luminositi with analogread
 * 
 * %return : value of luminosite in char *
 */
void mesure_luminosite(char *_buffer, size_t _taille);

#endif