#include "TYPE_BME280.h"
Adafruit_BME280 bme;

void bme280_mesure_temperature(char *_buffer, size_t _taille) {
  snprintf(_buffer, _taille, "%.2f", bme.readTemperature());
  DEBUG_PRINTF("temperature bme280 %s\n", _buffer);
}

void bme280_mesure_humidite(char *_buffer, size_t _taille) { 
  snprintf(_buffer, _taille, "%.3f", bme.readHumidity());
  DEBUG_PRINTF("humidite bme280 %s\n", _buffer);
}

void bme280_mesure_pression(char *_buffer, size_t _taille) {
  float pressure;
  
  pressure = bme.readPressure();
  snprintf(_buffer, _taille, "%.2f", pressure);
  DEBUG_PRINTF("pression bme280 %s\n", _buffer);
}

void bme280_mesure_altitude(char *_buffer, size_t _taille) {
  snprintf(_buffer, _taille, "%.2f", bme.readAltitude(1013.25));
  DEBUG_PRINTF("altitude bme280 %s\n", _buffer);
}

void bme280_init() {
  bme.begin(0x76);
}
