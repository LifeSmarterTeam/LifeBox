#include "TYPE_LUMINOSITE.h"

void mesure_luminosite(char *_buffer, size_t _taille) {
  snprintf(_buffer, _taille, "%d", analogRead(A0));
  DEBUG_PRINTF("Luminosite %s\n", _buffer);
}
