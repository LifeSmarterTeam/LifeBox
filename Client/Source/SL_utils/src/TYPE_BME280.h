#ifndef _TYPE_BME280_H
#define _TYPE_BME280_H

#include <Arduino.h>
#include "error.h"

#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

/** 
 * Mesure luminositi with analogread
 * 
 * %return : value of luminosite in char *
 */
void bme280_mesure_temperature(char *_buffer, size_t _taille);

void bme280_mesure_humidite(char *_buffer, size_t _taille);

void bme280_mesure_pression(char *_buffer, size_t _taille);

void bme280_mesure_altitude(char *_buffer, size_t _taille);

void bme280_init();

#endif
