#include <SmarterLife.h>
#include <TYPE_LUMINOSITE.h>

#define DATA_CONNEXION {TYPE_LUMINOSITE}

void setup(void) {
  Serial.begin(115200);
  Serial.println("Photoresistor sensor by SmarterLife\n");

  /* S'occupe de l'init et de la premiere connexion a la wifi */
  if (SL_Setup(&client) == false )
  {
    DEBUG_PRINTLN("Erreur dans le setup, reboot ...");
    ESP.restart();
  }

}

void loop(void) {
  char luminiosite[10];
  int total_read = 0;
  int data_connexion[] = DATA_CONNEXION;

  /* Reconnect que si on a perdu la connexion & premiere connection au serveur */
  if (SL_verif_connexion(data_connexion) != false)
  {

    DEBUG_PRINTLN("------- DEBUT COMMUNICATION ----------");
    /* Recuperation des valeurs */
    mesure_luminosite(luminiosite, sizeof(luminiosite));

    if (SL_make_tram_char(&g_tram, data_connexion, luminiosite, NULL) == false )
    {
      DEBUG_PRINTLN("make error");
      return;
    }

    if (SL_toTram(&g_tram, (char *) g_tram.string_tram) == false)
    {
      DEBUG_PRINTLN("Erreur SL_toTram");
      return;
    }

    SL_envoi_tram(&g_tram, &client);

    free(g_tram.data);

    total_read = SL_recoit_tram(&g_tram, &client, DELAY_ENTRE_DONNEE_COMM, DELAY_RECOIT_TRAM_COMM);

    if ( total_read == 0)
    {
      DEBUG_PRINTLN("total_read est vide");
      return;
    }

    if (SL_parse(&g_tram, g_tram.string_tram, total_read) != true)
    {
      DEBUG_PRINTLN("parse tram communication effectue");
      return;
    }

    /* Recuperation valeur tram recu */ 
    if (SL_recup_valeur_communication(&g_tram, TYPE_LUMINOSITE, luminiosite) != true)
    {
      DEBUG_PRINTLN("parse tram communication effectue");
      return;
    }
  }

  DEBUG_PRINTLN("attente ...");
  delay(5000);

}
