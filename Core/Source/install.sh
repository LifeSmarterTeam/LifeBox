#!/bin/bash

if [ "$EUID" -ne 0 ]; then
    echo "This script must be run as root" 
    exit 1
fi


echo "Preparing installation (Make, copy ...)"

read -n 1 -p "Doing this step (Y/n)? " -r
echo ""

if [[ ! $REPLY =~ ^[Nn]$ ]]; then
	echo "Updating debug mode";
	sed -i "s/^DEBUG=yes/DEBUG=no/" Makefile 

	echo "Making all executable";
	make > /dev/null

	echo "Moving exe"
	mkdir -p ../Exe/Comm
	mkdir -p ../Exe/Pars
	mkdir -p ../Exe/Serv

	cp ./Comm/BIN/Out ../Exe/Comm
	cp ./Pars/BIN/Out ../Exe/Pars
	cp ./Serv/BIN/Out ../Exe/Serv

	echo "Compiling done"
	make clear > /dev/null
fi

echo "Installation"
read -n 1 -p "Doing this step (Y/n)? " -r
echo ""

if [[ ! $REPLY =~ ^[Nn]$ ]]; then
	(cd ../Tool && ./install.sh)
fi
