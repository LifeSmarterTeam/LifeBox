#ifndef STRUCT_CLIENT_H
#define STRUCT_CLIENT_H

#include "socket.h"

#define MAX_CONNEXION 999

typedef struct Client {
	int module_number;					/* Number of the module */

    SOCKET id_socket;       			/* Id of the socket opened by the Client. */
    SOCKADDR_IN sock_info;  			/* Client's adress. */

	struct Client *next_client; 		/* Pointer on the next Client (Captain Obvious) */
	struct Client *prev_client; 		/* Pointer on the previous Client */ 
} Client;

#endif /* STRUCT_CLIENT_H included */
