#ifndef VERIFICATION_H
#define VERIFICATION_H

#include <stdio.h>

#include "boolean.h"

/** Funtion to verify the user imput for the boss' port.
 *  %param port : Port to verify.
 *  %return : If valid port then TRUE,
 *            else FALSE.
 */
bool verifPort(int _port);

#endif /* VERIFICATION_H included */
