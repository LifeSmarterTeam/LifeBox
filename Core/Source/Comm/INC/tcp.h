#ifndef TCP_H
#define TCP_H

#define SEND 0
#define RECEIVED 1

#include "socket.h"
#include "boolean.h"
#include "struct_client.h"

/** Sends/Receives data to/from a Client.
 * 
 *  %param _c: Client considered.
 *  %param _data: Data to send or to receive.
 *  %param _data_length: Number of bits in the data.
 *  %param _type: SEND to send data, RECEIVED to receive data.
 * 
 *  %return: the number of bit sended or received,
 *           -1 if incorrect type.
 */
int tcpAction(Client *_c, void *_data, int _data_length, int _type);

/** Receives data from a Client but with a timer on received
 * 
 *  %param _c: Client considered.
 *  %param _data: Data to send or to receive.
 *  %param _data_length: Number of bits in the data.
 *  %param _second : Number of second to wait
 *  %param _millisecond : Number of millisecond to wait
 * 
 *  %return: the number of bit received,
 */
int tcpActionDelay(Client *_c, void *_data, int _data_length, int _second, int _millisecond);

#endif /* TCP_H included */
