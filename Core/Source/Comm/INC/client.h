#ifndef CLIENT_H
#define CLIENT_H

#include "boolean.h"
#include "struct_server.h"
#include "struct_client.h"

/** init a Client
 * 
 *  %return : a client which one had been initialized.
 */
Client* initClient(void);

/** Accept a client from the 'server_socket' socket.
 * 
 *  %param _server_socket : socket of the server, which on you want to accept a client.
 *  %return : the information about the client.
 */
Client* acceptClient(int const _server_socket);

/** Add a new Client in the list of connected clients if the limit of connexions is not reached.
 * 
 *  %param _s : The instance of the local Server.
 *  %return : 1 if a Client is added, 
 *            0 otherwise.
 *  We do not use bool here cause we use this value to increment or not number of leacher
 */
int addClient(Server* _s);

/** Remove a client frop the array of client.
 * 
 *  %param _s : The instance of the local Server.
 *  %param _c : Client to remove
 *  %return   : Return the next client 
 */
Client *removeClient(Server* s, Client *_c);

/** Close an array of client.
 *
 *  %param _c : the array of client.
 *  %param total : total number of client.
 */
void closeClientArray(Client *_c);

#endif /* CLIENT_H included */
