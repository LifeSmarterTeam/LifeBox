#ifndef INOUT_H
#define INOUT_H

/** Asks the user the port
 *  %return : Which port to use
 */
int askPort(void);

/** Function to empty the stdin buffer.
 */
void emptyBuffer(void);

#endif /* INOUT_H included */
