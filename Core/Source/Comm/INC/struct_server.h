#ifndef STRUCT_SERVER_H
#define STRUCT_SERVER_H

#include <stdlib.h>

#include "socket.h"
#include "boolean.h"
#include "struct_client.h"

typedef struct Server {
    SOCKET server_socket;   /* Socket for the server */
    int max_socket;         /* Max id of all socket */
    
    Client *client;        	/* List of client */ 
    int nb_client;          /* Total number of client */
} Server;

#endif /* STRUCT_SERVER_H included */
