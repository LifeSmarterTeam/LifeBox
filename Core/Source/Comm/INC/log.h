#ifndef LOG_H
#define LOG_H

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>

extern FILE* LOGGER_OUTPUT;

#ifdef DEBUG
	#define DEBUG_PRINT DEBUG
    #define KEEP_IN_DEBUG(FILE) FALSE
#else
	#define DEBUG_PRINT 3 
    #define KEEP_IN_DEBUG(FILE) remove(FILE)
#endif

#define MAX(a,b) ({ __typeof__ (a) _a = (a); __typeof__ (b) _b = (b); _a > _b ? _a : _b; })

enum { LOG_FATAL, LOG_ERROR, LOG_WARN, LOG_INFO, LOG_DEBUG };

static const char *LVL_NAME[] = {
  "FATAL", "ERROR", "WARN ", "INFO ", "DEBUG"
};

#ifdef LOG_COLOR 
	static const char *LVL_COLOR[] = {
	  "\x1b[31m", "\x1b[33m", "\x1b[32m", "\x1b[36m", "\x1b[94m"
	};

	#define LOG_MSG_COLOR(LVL_LOG) do { fprintf(stderr, "[%s%-5s\x1b[0m] ", LVL_COLOR[LVL_LOG], LVL_NAME[LVL_LOG]); } while(0);
#else
	#define LOG_MSG_COLOR(LVL_LOG) do { fprintf(stderr, "[%s]", LVL_NAME[LVL_LOG]); } while(0);
#endif

#define LOG_PRINT(LVL_LOG, MSG, ...)                                                                                        \
do {                                                                                                                        \
	if(LVL_LOG <= DEBUG_PRINT) {                                                                                            \
		time_t t = time(NULL); fprintf(stderr, "\n%s", ctime(&t));                                                              \
		LOG_MSG_COLOR(LVL_LOG);                                                                                             \
		if(LVL_LOG >= LOG_DEBUG) fprintf(stderr, "File : %s - Line : %d - Function : %s() : ",__FILE__, __LINE__, __func__);\
		fprintf(stderr, MSG, ## __VA_ARGS__);                                                                               \
		fprintf(stderr, "\n");                                                                                              \
	}                                                                                                                       \
																															\
	if(LVL_LOG == LOG_FATAL) { perror(NULL); exit(EXIT_FAILURE); }                                                          \
} while(0);

#endif /* LOG_H included */
