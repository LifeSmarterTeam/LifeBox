#ifndef WINDOWS_H
#define WINDOWS_H

/** Initialize socket for windows 
 */
void initWindows(void);

/** Stop correctly using socket on widows 
 */
void endWindows(void);

#endif /*WINDOWS_H included */
