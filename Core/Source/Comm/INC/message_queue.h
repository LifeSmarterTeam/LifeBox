#ifndef SHARED_MEMORY_H
#define SHARED_MEMORY_H

#define SEND 0
#define RECEIVED 1

#define SHARED_FROM_PARS "/ParsToCommDom" /* Funny enougth to look like Condoms */
#define SHARED_TO_PARS "/CommToParsDom"

#define FLAG_MODE_OPEN 0777

#define MQ_FLAG O_NONBLOCK
#define MAX_MSG_TO_PARS 10

#include "struct_communication.h"

/** Create a new message queue for shared messages (Created)
 * 
 * %param _c : Communication which would countain the communication canal
 */
void createMessageQueue(Communication *_c);

/** Free and close the communication (close message queue)
 *
 * %param _c : Communication to free / close
 */
void freeMessageQueue(Communication *_c); 

/** Connect on a mesasge queue (Binding)
 *
 * %param _c : Communication which would countain the communication canal
 */
void connectMessageQueue(Communication *_c);

/** Close the message queue associate with _c
 *
 * %param _c : COmmunication canal to close
 */
void closeMessageQueue(Communication *_c);

/** Interaction with mesage queue
 *
 * %param _mqueue : Message queue identifier
 * %param _data : Data to send
 * %param _data_length : Size of the data
 * %param _prio : Priority of the message ( Send only )
 * %param _type : SEND or RECEIVED
 */
int messageQueueAction(int _mqueue, void *_data, int _data_length, int _prio, int _type); 

#endif /* SHARED_MEMORY_H included */
