#ifndef SERVER_H
#define SERVER_H

#include "struct_server.h"
#include "struct_communication.h"

#define MAX_SIZE_BUFF 100

/** Create a new server and allocate it/ initialize it according to a port
 *  %param _port : port of the new server
 */
Server* newServer(const int _port);

/** free the server Structure
 *  %param _s : Structure which them we want to free
 */
void freeServer(Server *_s);

/** init a socket for a server
 * 
 *  %param _port : port on which we need to init the server
 *  %return : the initialized socket for the server
 */ 
int initServer(const int _port);

/** close a server
 * 
 *  %param _server_socket : socket of the server that you need to close
 */
void closeServer(int _server_socket);

/** Start the server and select the socket who communicate
 * 
 *  %param _port : Port on which open the server
 */
void startServer(const int _port);

/** Init the selector of socket
 *
 *  %param _s : Socket of the server in order to select
 *  %param _c : Communication canals
 *  %param _rdfs : FD_SET to initialize
 */
void initFd(Server *_s, Communication *_c, fd_set *_rdfs);

/** Manage all the cient on the server.
 *  Reveive Data, and communicate them to parser
 *
 *  %param _s : Server information
 *  %param _c : Communication canals
 *  %param _rdfs : fd_set init containing socket info of clients
 */
void manageClient(Server *_s, Communication *_c, fd_set* _rdfs);


#endif /* SERVER_H included */

