#ifndef SOCKET_H
#define SOCKET_H

	#include <unistd.h>

	/* If We are on windows */
	#if _WIN32
		#include <winsock2.h>
	#elif __linux__

		#include <sys/types.h>
		#include <sys/socket.h>
		#include <netinet/in.h>
		#include <arpa/inet.h>
		
		#include <netdb.h>

		#define INVALID_SOCKET -1
		#define SOCKET_ERROR -1
		#define closesocket(s) close (s)

		typedef int SOCKET;
		typedef struct sockaddr_in SOCKADDR_IN;
		typedef struct sockaddr SOCKADDR;

	#endif
#endif /* SOCKET_H included */
