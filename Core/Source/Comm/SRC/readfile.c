#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "log.h"
#include "boolean.h"
#include "readfile.h"

#define MAX_PATH 1024
#define SEPARATOR '/'
#define TRIM_SLASH 2
#define CONCAT_PATH "/Exchange"

FILE* LOGGER_OUTPUT = NULL;
void defineFileLogger(char *_argv) 
{
	(void) _argv;
	LOGGER_OUTPUT = stderr;
#ifdef LOG_FILE
    char path[MAX_PATH];
    getRealPath(path, _argv);
	LOGGER_OUTPUT = fopen(path, "w+");
#endif
}

char *trimRealpath(char *_realpath) 
{
    char *tmp = NULL, *new_realpath = NULL;
    int size = 0, slash = 0;
    
    new_realpath = _realpath;
    for( tmp = _realpath; *tmp != '\0'; tmp++) {
        ++size;
        if ( *tmp == SEPARATOR ) ++slash;
    }

    if (slash > TRIM_SLASH ) {
        slash = 0;
        for(; size > 0; size--) {
            if(_realpath[size] == SEPARATOR) slash++;
            if(slash == TRIM_SLASH ) break;
        }
        
        new_realpath = malloc((size + sizeof(CONCAT_PATH)) * sizeof(char));
        if ( new_realpath == NULL ) LOG_PRINT(LOG_FATAL, "Error malloc tmp");

        strncpy(new_realpath, _realpath, size);
        strcat(new_realpath, CONCAT_PATH);    
        free(_realpath);
    }
    
    return new_realpath;
}

void getRealPath(char *_path, const char *_filename) 
{    
    snprintf(_path, MAX_PATH - 1, "%s%c%s", __REALPATH, SEPARATOR, _filename);
    LOG_PRINT(LOG_DEBUG, "Path : [%s]", _path);
}

char *callRealpath(char *_argv0) 
{
    char *real_path = calloc(18, sizeof(char));
    strcpy(real_path, "/tmp/SL_Exchange");
    (void) _argv0;

    /*
    real_path = realpath(_argv0, NULL);
    if (real_path == NULL) exit(EXIT_FAILURE);
    
    real_path = trimRealpath(real_path);
    DEBUG_PRINT("Path : [%s]", real_path);
    */
    return real_path;
}

void getFileName(char *_filename, int _total_file) 
{
    sprintf(_filename, "%u_%d_%s", (int)time(NULL), _total_file, EXTENSION);
}

FILE* createFile(char *_filename) 
{
    FILE* fp;
    fp = fopen(_filename, "w+");
    return fp;
}

void writeStramInFile(char *_filename, int _from, Stram *_stram)
{
    FILE* fp;
    char path[MAX_PATH] = {0};

    getFileName(_filename, 0);
    getRealPath(path, _filename);

    if ((fp = createFile(path)) != NULL) {
        LOG_PRINT(LOG_DEBUG, "Writing file : %s", path);
        fprintf(fp, "$%d$\n", _from);
        fprintf(fp, "$%s$\n", *_stram);
        fclose(fp);
    }
}

void loadStram(const char *_filename, int *_from, Stram _stram)
{
    FILE *fp;
    char path[MAX_PATH];

    getRealPath(path, _filename);
    if ((fp = fopen(path, "rb")) == NULL ) {
        LOG_PRINT(LOG_FATAL, "Error while opening the file\n");
    }

    if( fscanf(fp, "$%d$\n", _from) != 1) {
        LOG_PRINT(LOG_ERROR, "Difference between read and expected\n");
    }
 
    if( fscanf(fp, "$%[^$]$\n", _stram) != 1) {
        LOG_PRINT(LOG_ERROR, "Difference between read and expected\n");
    }

    fclose(fp);
    if(KEEP_IN_DEBUG(path) != FALSE) {
		LOG_PRINT(LOG_ERROR, "Fail to remove [%s]", path);
	}

    return;
}
