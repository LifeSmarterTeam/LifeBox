#include <string.h>

#include "log.h"
#include "client.h"

Client *initClient() 
{
    Client *c;

    if ( (c = calloc(1, sizeof(*c))) == NULL ) {
        LOG_PRINT(LOG_FATAL,"Can't allocate new client.");
    }

    memset(&c->sock_info, 0, sizeof(c->sock_info));

    c->id_socket = socket(AF_INET,SOCK_STREAM,0);  
    c->sock_info.sin_family = AF_INET;
    c->next_client = NULL;
    c->prev_client = NULL;

    if (c->id_socket == -1) {
        LOG_PRINT(LOG_FATAL, "Can't create the socket");
    }

    return c;
}

Client *acceptClient(int const _server_socket) 
{
    int struct_size;
    Client* new_client = initClient();

    struct_size = sizeof(SOCKADDR_IN);

    new_client->id_socket = accept(_server_socket, (struct sockaddr*) &new_client->sock_info, (__socklen_t *)&struct_size);

    LOG_PRINT(LOG_INFO, "\nNew client [%d]\n\t - Ip of client : %s\n", 
            new_client->id_socket, inet_ntoa(new_client->sock_info.sin_addr));

    return new_client;
}

int addClient(Server *_s)
{
    Client *c = NULL;
    Client *tmp = acceptClient(_s->server_socket);

    if(_s->nb_client >= MAX_CONNEXION) {
        LOG_PRINT(LOG_INFO, "Max number of client reached.\n");
        closesocket(tmp->id_socket);
        free(tmp);

        return 0; 
    }

    if ( _s->max_socket < tmp->id_socket ) {
        LOG_PRINT(LOG_DEBUG, "Change the max socket --> [%d]", tmp->id_socket);
        _s->max_socket = tmp->id_socket;
    }

    if (_s->client == NULL) 
        _s->client = tmp;
    else {
        for (c = _s->client; c->next_client != NULL; c = c->next_client) ; /* Go to the moon */
        c->next_client = tmp;
    }

    tmp->prev_client = c;    

    return 1;
}

Client *removeClient(Server* _s, Client *_c) 
{
    Client *c = NULL;
    int new_max_socket;

    LOG_PRINT(LOG_INFO, "Client %d disconnect\n", _c->id_socket);

    /* Seek new socket max */
    if ( _s->max_socket == _c->id_socket ) {
        new_max_socket = _s->server_socket;
        for (c = _s->client; c != NULL; c = c->next_client) {
            if (c->id_socket != _c->id_socket 
                    && c->id_socket > new_max_socket ) {

                new_max_socket = c->id_socket;
            }
        }
        _s->max_socket = new_max_socket;
    }

    c = _c->next_client;

    if (_c->id_socket == _s->client->id_socket) _s->client = _c->next_client;
    else {
        if (_c->prev_client != NULL) _c->prev_client->next_client = _c->next_client;
        if (_c->next_client != NULL) _c->next_client->prev_client = _c->prev_client;
    }

    --(_s->nb_client);

    closesocket(_c->id_socket);
    free(_c);

    return c;
}

void closeClientArray(Client *_c) 
{
    Client *c = _c;
    Client *next = _c;

    while(c != NULL) {
        next = c->next_client;

        closesocket(c->id_socket);
        free(c);

        c = next;
    }
}
