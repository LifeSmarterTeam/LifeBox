#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <netinet/tcp.h>

#include "log.h"
#include "server.h"
#include "client.h"
#include "boolean.h"
#include "tcp.h"
#include "readfile.h"

#include "struct_tram.h"
#include "message_queue.h"

bool stop = FALSE; 
void signal_trap(int _ignored) { (void) _ignored; stop = TRUE; }

Server *newServer(const int _port) 
{
    Server *s; 

    if ( (s = calloc(1, sizeof(*s))) == NULL ) {
        LOG_PRINT(LOG_FATAL, "Can't allocate new server.");
    }

    s->server_socket = initServer(_port);
    s->max_socket = s->server_socket;
    
    return s;
}

void freeServer(Server *_s) 
{ 
    closeClientArray(_s->client);
    free(_s);

    return;
}

int initServer(const int _port) 
{
    int optionReUse = 1;
    int optionKeepAlive = 1;
    int optionKeepCnt = 2;
    int optionKeepIdle = 5;
    int optionKeepIntVl = 5;
    int struct_size, serveur_socket;
    SOCKADDR_IN s_serveur;
    struct_size = sizeof(SOCKADDR_IN);
        
    s_serveur.sin_family = AF_INET;
    s_serveur.sin_addr.s_addr = INADDR_ANY;
    s_serveur.sin_port = htons(_port);    
    
    if ( (serveur_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1){
        LOG_PRINT(LOG_FATAL, "Can't create the socket : ");
    }

    if ( setsockopt(serveur_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&optionReUse, sizeof(optionReUse)) == SOCKET_ERROR ){
        LOG_PRINT(LOG_FATAL, "Can't set socket option : ");
    }
    
    if ( setsockopt(serveur_socket, SOL_SOCKET, SO_KEEPALIVE, (char *)&optionKeepAlive, sizeof(optionKeepAlive)) == SOCKET_ERROR) {
        LOG_PRINT(LOG_FATAL, "Can't set socket option : ");
    }

    if ( setsockopt(serveur_socket, SOL_TCP, TCP_KEEPCNT, (char *)&optionKeepCnt, sizeof(optionKeepCnt)) == SOCKET_ERROR) {
        LOG_PRINT(LOG_FATAL, "Can't set socket option : ");
    }

    if ( setsockopt(serveur_socket, IPPROTO_TCP, TCP_KEEPIDLE, (char *)&optionKeepIdle, sizeof(optionKeepIdle)) == SOCKET_ERROR) {
        LOG_PRINT(LOG_FATAL, "Can't set socket option : ");
    }

    if ( setsockopt(serveur_socket, SOL_TCP, TCP_KEEPINTVL, (char *)&optionKeepIntVl, sizeof(optionKeepIntVl)) == SOCKET_ERROR) {
        LOG_PRINT(LOG_FATAL, "Can't set socket option : ");
    }

    if ( bind(serveur_socket, (struct sockaddr *)&s_serveur, struct_size) == -1){
        LOG_PRINT(LOG_FATAL, "Can't bind the socket : ");
    }
    
    /* Hey listen */
    if ( listen(serveur_socket,MAX_CONNEXION) == -1 ){
        LOG_PRINT(LOG_FATAL, "Socket listen trouble : ");
    }
    
    return serveur_socket;
}

void closeServer(int _server_socket) 
{
    close(_server_socket);
    
    return;
}

void startServer(const int _port)
{
    int timer;
    fd_set rdfs;
    struct timeval tval;
    
    int from;
    Client tmp;
    Stram string_data;
    Communication c = {0};
    
    char filename[MAX_FILE_NAME];

    (void) signal(SIGUSR1, &signal_trap);

    Server *s = newServer(_port);
    LOG_PRINT(LOG_INFO, "Server start [%d]", s->server_socket);     

    tval.tv_sec  = 5;
    tval.tv_usec = 0; 
    
    LOG_PRINT(LOG_INFO, "Press Enter to Stop the Communication");  
 
    createMessageQueue(&c);    

    for ( ;; ) {
        connectMessageQueue(&c);
        
        initFd(s, &c, &rdfs);
        if( c.from.parser.mqd != -1)
            FD_SET(c.from.parser.mqd, &rdfs);
        
        LOG_PRINT(LOG_INFO, "Waiting for message ...");  
        LOG_PRINT(LOG_DEBUG, "MAX : [%d] [%d] -> [%d]", s->max_socket, c.max_mdq, MAX(s->max_socket, c.max_mdq));
        if( (timer = select(MAX(s->max_socket, c.max_mdq) + 1, &rdfs, NULL, NULL, &tval)) == -1) {
            if(errno == EINTR && stop == TRUE ) break;
            LOG_PRINT(LOG_FATAL, "Can't select : ");
        }

        if ( timer >= 0 ) {
            LOG_PRINT(LOG_INFO, "No message - Rechecking in few second [%d]", timer);  
            tval.tv_sec = 5;
        }

        #ifdef __linux__
        if( FD_ISSET(STDIN_FILENO, &rdfs) ) {
            break;
        }
        #endif
    
        if( c.from.parser.mqd != -1) {
            if( FD_ISSET(c.from.parser.mqd, &rdfs) ) {
                LOG_PRINT(LOG_INFO, "Message from parser. Processing !");  

                messageQueueAction(c.from.parser.mqd, filename, sizeof(filename), 0, RECEIVED);

                loadStram(filename, &from, string_data);
                tmp.id_socket = from;
                LOG_PRINT(LOG_DEBUG, "From : %d - %s", from, string_data);
                tcpAction(&tmp, string_data, strlen(string_data), SEND);   
            }
        }

        if( FD_ISSET(s->server_socket, &rdfs) ) {
            s->nb_client += addClient(s);
        }
        else {
            manageClient(s, &c, &rdfs);
        }
    }

    closeMessageQueue(&c);
    freeMessageQueue(&c);

    closeServer(s->server_socket);
    freeServer(s);

    return;
}

void initFd(Server* _s, Communication *_c, fd_set* _rdfs)
{
    Client *c = NULL;

    FD_ZERO(_rdfs);

    #ifdef __linux__
    FD_SET(STDIN_FILENO, _rdfs);
    #endif

    FD_SET(_s->server_socket, _rdfs);
    
    if( _c->from.parser.mqd != -1 ) 
        FD_SET(_c->from.parser.mqd, _rdfs);

    for(c = _s->client; c != NULL; c = c->next_client) {
        FD_SET(c->id_socket, _rdfs);
    }
}

void manageClient(Server* _s, Communication *_c, fd_set* _rdfs)
{
    Client *c = _s->client; 
    Stram tram_tmp = {0};
    char filename[MAX_FILE_NAME] = {0};
    int tmp;

    while(c != NULL) {
        if ( FD_ISSET(c->id_socket, _rdfs) ) {
            LOG_PRINT(LOG_INFO, "Message from client [%d]. Processing !", c->id_socket);  
            memset(tram_tmp, 0, MAX_TRAM_SIZE);
            tmp = tcpAction(c, tram_tmp, MAX_TRAM_SIZE, RECEIVED);

            if ( tmp <= 0 ) { /* I think he died */
                LOG_PRINT(LOG_INFO, "Message from client [%d]. Dead !", c->id_socket);  
                /* Construct tram */
                strcpy(tram_tmp, "1001230004");
                
                writeStramInFile(filename, c->id_socket, &tram_tmp);
                messageQueueAction(_c->to.parser.mqd, filename, sizeof(filename), 0, SEND);

                c = removeClient(_s, c);
                continue;
            } else {
                LOG_PRINT(LOG_DEBUG, "\n[INFO] (%d) message recu '%s' [Client : %d].\n", tmp, tram_tmp, c->id_socket);    
                
                writeStramInFile(filename, c->id_socket, &tram_tmp);
                messageQueueAction(_c->to.parser.mqd, filename, sizeof(filename), 0, SEND);
            }    
        }

        c = c->next_client;
    }
}
