#include "log.h"
#include "inout.h"
#include "verification.h"

int askPort() 
{
    int port;
    
    do {
        printf("\n[Q] On which port do you want to start the server ?\n");

        if(scanf("%d", &port) != 1){
            LOG_PRINT(LOG_WARN, "Nothing read when asking port.");
        }

        emptyBuffer();
    } while ( !verifPort(port) );
    
    return port;
}

void emptyBuffer() 
{
    int c = 0;
    while (c != '\n' && c != EOF) {
        c = getchar();
    }
    
    return;
}
