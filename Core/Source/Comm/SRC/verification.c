#include <stdlib.h>
#include <string.h>

#include "log.h"
#include "verification.h"

bool verifPort(int _port) 
{
    if(_port <= 1024) {
        LOG_PRINT(LOG_ERROR, "The port must be greater than 1024.\n");
        return FALSE;
    }

    if(_port > 65536) {
        LOG_PRINT(LOG_ERROR, "The port must be lower than 65536.\n");
        return FALSE;
    }

    return TRUE;
}
