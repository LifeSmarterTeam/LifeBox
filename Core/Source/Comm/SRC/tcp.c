#include "tcp.h"
#include "log.h"
#include "socket.h"

int tcpAction(Client *_c, void *_data, int _data_length, int _type) 
{        

    LOG_PRINT(LOG_DEBUG, "[%d] to %d, %d", _type, _c->id_socket, _data_length);

    if ( _type == SEND ) {
        return send(_c->id_socket, _data, _data_length, 0);
    }
    else if ( _type == RECEIVED ) {
        return recv(_c->id_socket, _data, _data_length, 0);
    }
   
    return -1;
}

int tcpActionDelay(Client *_c, void *_data, int _data_length, int _second, int _millisecond ) 
{
    struct timeval tv;
    
    tv.tv_sec = _second;
    tv.tv_usec = _millisecond; 
    
    setsockopt(_c->id_socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,sizeof(struct timeval)); /* Set a timer on respond */
    return tcpAction(_c, _data, _data_length, RECEIVED);
} 
