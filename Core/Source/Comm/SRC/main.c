#include "log.h"
#include "inout.h"
#include "server.h"
#include "readfile.h"
#include "verification.h"

int main(int _argc, char* _argv[]) 
{    
    int port = 0;
    
    __REALPATH = callRealpath(_argv[0]);
	defineFileLogger("SERV");
    
    #ifndef DEBUG
        if( _argc > 1 ) 
            if(_argv[1][0] == '-')
                if(_argv[1][1] == 'v')
                     printf("Version : %d.%d.%d\n", MAJOR_VERSION, MINOR_VERSION, REVISION_VERSION), exit(EXIT_SUCCESS);
    #endif

    if ( _argc > 1 ) {
        if ( !verifPort(port = atoi(_argv[1])) ) {
            LOG_PRINT(LOG_FATAL, "An error occur, please refere to previous error(s)\n");
        }
    } else {
        LOG_PRINT(LOG_INFO, "Remember, you could use program argument. \n\t %s <port>\n\t With <port> between 1024 and 65536.\n", _argv[0]);
    }

    if ( port == 0 ) 
        port = askPort();

    startServer(port);
    free(__REALPATH); 
#ifdef OUTPUT_FILE
	fclose(LOGGER_OUTPUT);
#endif
    
    exit(EXIT_SUCCESS);
}
