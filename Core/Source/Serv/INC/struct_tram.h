#ifndef STRUCT_TRAM_H
#define STRUCT_TRAM_H

#include "struct_data.h"

#define OPEN_COM 		'1'
#define OPEN_TRAM 		'2'
#define CLOSE_TRAM 		'3'
#define CLOSE_COM 		'4'
#define DELIM_BLOCK 	"|"
#define DELIM_DATA 		":"

#define TYPE_SIZE 		3
#define SUBTYPE_SIZE 	3
#define TOTAL_SIZE 		3

#define MIN_TRAM_SIZE 	4 + TYPE_SIZE + TOTAL_SIZE /* OPEN_COM + OPEN_TRAM + END_TRAM + CLOSE_COM */
#define MAX_TRAM_SIZE 	100

typedef char Stram[MAX_TRAM_SIZE];

typedef struct Tram {
	int type; 			/* Type of communication  */
	int total_data;		/* Total of data to send */
	Data *data;			/* Array of Data */

	Stram string_tram;
} Tram;

#endif /* STRUCT_TRAM_H included */
