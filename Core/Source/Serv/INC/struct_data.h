#ifndef STRUCT_DATA_H
#define STRUCT_DATA_H 

#define MAX_VALUE_SIZE 32

typedef struct Data {
	int param;		/* Will contain a pointer of the param name*/
	char value[MAX_VALUE_SIZE];		/* Will contain a pointer of the value */
} Data;

#endif /* STRUCT_DATA_H included */
