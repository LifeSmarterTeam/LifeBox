#ifndef REQUEST_H
#define REQUEST_H

#include "struct_tram.h"

#define EXTENSION "SERV"
#define MAX_FILE_NAME 32

#define LOCAL_IP "http://127.0.0.1:8000"

/**
 * Update data of a module
 *
 * %param _num_module : Num module to update
 * %param _type_valeur_tram : Type of sensor to update
 * %param _val : Value to add
 */
int get_data(int _num_module, int _type_valeur_tram, char* _val);

/**
 * Update status on a sensor
 *
 *
 * %param _num_module : Num module to update
 * %param _type_tram : Type of sensor to update
 * %param _status : New status
 */
int update_status(int _num_module, int _type_tram, int _status);
#endif
