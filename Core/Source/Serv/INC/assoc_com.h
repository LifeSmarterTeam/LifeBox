#ifndef ASSOC_COM
#define ASSOC_COM

#include "struct_assoc_com.h"

#define getAssocNumModule(ASSOC, NUM_MODULE) getAssoc(ASSOC, NUM_MODULE, -1);
#define getAssocNumSocket(ASSOC, NUM_SOCKET) getAssoc(ASSOC, -1, NUM_SOCKET);

/**
 * Create a new assoc
 *
 * %param _num_module : New module number
 * %param _num_socket : New socket number
 *
 * %return : New socket struct
 */
AssocCom* newAssocCom(int _num_module, int _num_socket);

/**
 * Empty the assoc stack
 *
 * %param _assoc : Assoc to remove
 */
void removeAssocCom(AssocCom *_assoc); 

/**
 * Free all the assoc
 *
 * %param _assoc : Assoc to free
 */
void freeAssocCom(AssocCom *_assoc); 

/**
 * Add a new assoc to the stack
 *
 * %param _start : Stack onto we will add
 * %param _assoc : Assoc to add
 */
void addAssocCom(AssocCom **_start, AssocCom *_assoc); 

/**
 * Looking for an assoc in the Assoc stack
 *
 * %param _start : Assoc to look into
 * %param _num_module : Num module to seek
 * %param _num_socket : Num socket to seek
 *
 * %return : Assoc find
 */
AssocCom* getAssoc(AssocCom *_start, int _num_module, int _num_socket); 
#endif /* ASSOC_COM included */
