#ifndef _TRAM_H
#define _TRAM_H

/*
   Tous les type de communication 
*/

/* Pour la tram initialisation, envoyé lors d'un appuie sur le bouton init ou si pas de ssid */
#define TRAM_INIT 0
/* Tram envoyé pour chaque connexion au serveur pour qu'il puisse associer l'ip au n°module */
#define TRAM_CONNEXION 1
/* Tram de communication le plus courant pour envoyer des données sur les mesures etc... */
#define TRAM_COMMUNICATION 2

#endif /* _TRAM_H included */
