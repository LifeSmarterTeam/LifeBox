#ifndef _TYPE_VALEUR_TRAM_H
#define _TYPE_VALEUR_TRAM_H

/*Tous les type dans les trams */

/* 0 a  20 reserve */
#define TYPE_SSID 0
#define TYPE_PASSWORD 1
#define TYPE_MODULE 2
#define TYPE_IP 3
#define TYPE_PORT 4
#define TYPE_PARAMETRE 5

/* 20 a 9999 ouvert */
/* Mesure */
#define TYPE_TIMESTAMP 20
#define TYPE_TEMPERATURE 21
#define TYPE_HUMIDITE 22
#define TYPE_ANGLE 23
#define TYPE_COULEUR 24
#define TYPE_LUMINOSITE 25
#define TYPE_PRESSION 27
#define TYPE_ALLTITUDE 28

/* Action */
#define TYPE_SWITCH_0 26
#endif /* _TYPE_VALEUR_TRAM_H included */
