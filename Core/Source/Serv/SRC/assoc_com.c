#include <stdio.h>
#include <stdlib.h>

#include "log.h"
#include "boolean.h"
#include "assoc_com.h"

AssocCom* newAssocCom(int _num_module, int _num_socket) 
{
    AssocCom* assoc;

    if ( (assoc = calloc(1, sizeof(*assoc))) == NULL ) {
        LOG_PRINT(LOG_FATAL, "Can't allocate Assoc com");
    }

    assoc->num_module = _num_module;
    assoc->num_socket = _num_socket;
    assoc->next = NULL;

    return(assoc);
}

void removeAssocCom(AssocCom *_assoc)
{
    free(_assoc);
}

void freeAssocCom(AssocCom *_assoc) 
{
    AssocCom *curr;
    AssocCom *old;

    curr = _assoc;
    while(curr != NULL) {
        old = curr;
        curr = curr->next;
        free(old);
    }
}

void addAssocCom(AssocCom **_start, AssocCom *_assoc) 
{
    if(*_start == NULL ) {
        *_start = _assoc;
        return;
    }

    _assoc->next = *_start;
    *_start = _assoc;
}

AssocCom* getAssoc(AssocCom *_start, int _num_module, int _num_socket) 
{
    AssocCom* assoc;

    assoc = _start;
    while( assoc != NULL ) {
        if(assoc->num_module == _num_module || assoc->num_socket == _num_socket) return(assoc);
        assoc = assoc->next;
    }
    
    return(NULL);
}

