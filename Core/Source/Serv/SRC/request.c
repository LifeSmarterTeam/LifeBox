#include <stdio.h>
#include <curl/curl.h>
 
#include "log.h"
#include "request.h"

int get_data(int _num_module, int _type_valeur_tram, char* _val)
{
  CURL *curl;
  char url[45];
  CURLcode res;
 
  curl = curl_easy_init();
  if(curl) {
    sprintf(url, "%s/%d/%d/%s", LOCAL_IP, _num_module ,_type_valeur_tram, _val);
    curl_easy_setopt(curl, CURLOPT_URL, url);
    /* example.com is redirected, so we tell libcurl to follow redirection */ 
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
 
    LOG_PRINT(LOG_INFO, "Acces url [%s]", url);
    /* Perform the request, res will get the return code */ 
    res = curl_easy_perform(curl);
    /* Check for errors */ 
    if(res != CURLE_OK) {
       LOG_PRINT(LOG_DEBUG, "Wrong url [%s]", url);
    }
 
    /* always cleanup */ 
    curl_easy_cleanup(curl);
  }

  curl_global_cleanup();

  return 0;
}


int update_status(int _num_module, int _type_tram, int _status)
{
  CURL *curl;
  char url[45];
  CURLcode res;
 
  curl = curl_easy_init();
  if(curl) {
    sprintf(url, "%s/update_status/%d/%d/%d", LOCAL_IP, _num_module, _type_tram, _status);
    curl_easy_setopt(curl, CURLOPT_URL, url);
    /* example.com is redirected, so we tell libcurl to follow redirection */ 
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    LOG_PRINT(LOG_INFO, "Acces url [%s]", url);
 
    /* Perform the request, res will get the return code */ 
    res = curl_easy_perform(curl);
    /* Check for errors */ 
    if(res != CURLE_OK) {
        LOG_PRINT(LOG_DEBUG, "Wrong url [%s]", url);
    }
 
    /* always cleanup */ 
    curl_easy_cleanup(curl);
  }

  curl_global_cleanup();

  return 0;
}
