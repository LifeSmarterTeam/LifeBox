#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

#include "log.h"
#include "boolean.h"
#include "readfile.h"
#include "type_tram.h"
#include "type_valeur_tram.h"

#include "assoc_com.h"
#include "message_queue.h" 
#include "struct_tram.h"
#include "request.h"

bool stop = FALSE; 
void signal_trap(int _ignored) { (void) _ignored; stop = TRUE; }

void initFd(Communication *_c, fd_set *_rdfs) 
{
    FD_ZERO(_rdfs);

    #ifdef __linux__
    FD_SET(STDIN_FILENO, _rdfs);
    #endif

    if( _c->from.parser.mqd != -1 ) 
        FD_SET(_c->from.parser.mqd, _rdfs);

    if( _c->sweb.server.mqd != -1 ) 
        FD_SET(_c->sweb.server.mqd, _rdfs);
}

void handle_data_type(int _type)
{
    switch(_type) {
        case TYPE_TIMESTAMP:
            LOG_PRINT(LOG_DEBUG, "TYPE_TIMESTAMP [%d]", _type);
            break;
            
        case TYPE_TEMPERATURE:
            LOG_PRINT(LOG_DEBUG, "TYPE_TEMPERATURE [%d]", _type);
            break;
            
        case TYPE_HUMIDITE:
            LOG_PRINT(LOG_DEBUG, "TYPE_HUMIDITE [%d]", _type);
            break;
            
        case TYPE_ANGLE:
            LOG_PRINT(LOG_DEBUG, "TYPE_ANGLE [%d]", _type);
            break;
            
        case TYPE_COULEUR:
            LOG_PRINT(LOG_DEBUG, "TYPE_COULEUR [%d]", _type);
            break;
            
        case TYPE_LUMINOSITE:
            LOG_PRINT(LOG_DEBUG, "TYPE_LUMINOSITE [%d]", _type);
            break;
            
        case TYPE_PRESSION:
            LOG_PRINT(LOG_DEBUG, "TYPE_PRESSION [%d]", _type);
            break;
            
        case TYPE_ALLTITUDE:
            LOG_PRINT(LOG_DEBUG, "TYPE_ALLTITUDE [%d]", _type);
            break;
            
        default:
            LOG_PRINT(LOG_ERROR, "Type [%d] inconnu.", _type);
            break;
    }
}

void handle_tram(Tram *_t, AssocCom* _assoc)
{
    int i;

    if(_t->type == TRAM_COMMUNICATION) {
        for (i = 0; i < _t->total_data; i++) {
            handle_data_type(_t->data[i].param);
            get_data(_assoc->num_module, _t->data[i].param, _t->data[i].value);
        }
    }
}

AssocCom* handle_assoc_fetch(int _from, Tram *_t, AssocCom** _assoc) 
{
    int i = -1;
	int look_for = -1;

    AssocCom* new_assoc;
	
    if(_t->type == TRAM_CONNEXION)
    {
        for (i = 0; i < _t->total_data; i++) {
            if(_t->data[i].param == TYPE_MODULE) {
                look_for = atoi(_t->data[i].value); break;
            }
        }

		i = look_for;
        /* Si on ne trouve pas le module on l'ajoute */
        LOG_PRINT(LOG_DEBUG, "Looking for [%d] --> [%d]!", i, _from);
        if((new_assoc = getAssoc(*_assoc, i, _from)) == NULL ) {
            if(_t->total_data > 0) { /* On ajoute le module si, et seulement si : Tram connexion & Data & module pas trouvé */
				LOG_PRINT(LOG_DEBUG, "Add new module [%d] --> [%d]!", i, _from);
				new_assoc = newAssocCom(i, _from);
				addAssocCom(_assoc, new_assoc);
			}
        } 
        else {
            if(_t->total_data > 0) {
                LOG_PRINT(LOG_DEBUG, "Update module [%d] --> [%d] ==> [%d] --> [%d] !", 
                    new_assoc->num_module, new_assoc->num_socket, i, _from);
                new_assoc->num_module = i;
                new_assoc->num_socket = _from;
            }
        }

        if(new_assoc != NULL) update_status(new_assoc->num_module, _t->type, ((_t->total_data) > 0));
    }

    if(_t->type == TRAM_COMMUNICATION) {
        if((new_assoc = getAssoc(*_assoc, -1, _from)) == NULL ) {
            LOG_PRINT(LOG_ERROR, "Module [%d], unknown !", _from); 
        }
    }

    return new_assoc;
}

int main(int _argc, char *_argv[]) 
{
    int timer;
    fd_set rdfs;
    struct timeval tval;
    AssocCom* assoc = NULL;
    AssocCom* tmp = NULL;

    int from;
    char filename[MAX_FILE_NAME] = {0};

    Tram t;
    Communication c = {0};

    (void) signal(SIGUSR1, &signal_trap);

   __REALPATH = callRealpath(_argv[0]);

	defineFileLogger("SERV");

	if( _argc > 1 ) 
		if(_argv[1][0] == '-')
			if(_argv[1][1] == 'v')
				printf("Version : %d.%d.%d\n", MAJOR_VERSION, MINOR_VERSION, REVISION_VERSION), exit(EXIT_SUCCESS);

    tval.tv_sec = 5;
    tval.tv_usec = 0;

    LOG_PRINT(LOG_DEBUG, "Press Enter to Stop the Server");  
    
    createMessageQueue(&c);

    for(;;) {
        connectMessageQueue(&c);
        initFd(&c, &rdfs);

        LOG_PRINT(LOG_INFO, "Waiting for message ...");  
        LOG_PRINT(LOG_DEBUG, "Selecting on [%d] ...", c.max_mdq);  
        if( (timer = select(c.max_mdq + 1, &rdfs, NULL, NULL, &tval)) == -1) {
            if(errno == EINTR && stop == TRUE ) break;
            LOG_PRINT(LOG_FATAL, "Can't select : ");
        }
    
        if( timer == 0 ) { /* Nothing */
            LOG_PRINT(LOG_INFO, "No message - Rechecking in few second");  
            tval.tv_sec = 5;
        }

        #ifdef __linux__
        if( FD_ISSET(STDIN_FILENO, &rdfs) ) {
            break;
        }
        #endif

        if( c.from.parser.mqd != -1) {
            if( FD_ISSET(c.from.parser.mqd, &rdfs) ) {
                LOG_PRINT(LOG_INFO, "Message from parser. Processing !");  

                messageQueueAction(c.from.parser.mqd, filename, sizeof(filename), 0, RECEIVED);
                loadTram(filename, &from, &t);
                    
                tmp = handle_assoc_fetch(from, &t, &assoc);
                if(t.total_data <= 0 || tmp != NULL) {
                    handle_tram(&t, tmp);
					/* Désactivation des messages de retour automatique 
                     * writeInTramFile(filename, from, &t);
                     * messageQueueAction(c.to.parser.mqd, filename, sizeof(filename), 0, SEND);
					 **/
                }

                free(t.data);
            }
        }

        if( c.sweb.server.mqd != -1) {
            if( FD_ISSET(c.sweb.server.mqd, &rdfs) ) {
                LOG_PRINT(LOG_INFO, "Message from server web. Processing !");  

                messageQueueAction(c.sweb.parser.mqd, filename, sizeof(filename), 0, RECEIVED);
                loadTram(filename, &from, &t);

                /* Rechercher a quel module on envoi */
                if ((tmp = getAssoc(assoc, from, -1)) != NULL) {
                    from = tmp->num_socket; 
                }

                writeInTramFile(filename, from, &t);
                messageQueueAction(c.to.parser.mqd, filename, sizeof(filename), 0, SEND);
                free(t.data);
            }
        }
    }
    
    LOG_PRINT(LOG_INFO, "Stopping Server.");  
    freeAssocCom(assoc);
    closeMessageQueue(&c);
    freeMessageQueue(&c);
    free(__REALPATH); 
#ifdef OUTPUT_FILE
	fclose(LOGGER_OUTPUT);
#endif
    exit(EXIT_SUCCESS);
}
