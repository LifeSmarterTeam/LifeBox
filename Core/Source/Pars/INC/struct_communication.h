#ifndef STRUCT_COMMUNICATION_H
#define STRUCT_COMMUNICATION_H

typedef struct Info {
	int mqd;
	unsigned *addr;
} Info;

typedef struct Canals {
	Info server;
	Info parser;
	Info communication;
} Canals;

typedef struct Communication {
	Canals from;
	Canals to;

	int max_mdq;
} Communication;

#endif /* STRUCT_COMMUNICATION_H included */
