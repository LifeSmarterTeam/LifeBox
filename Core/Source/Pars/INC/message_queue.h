#ifndef SHARED_MEMORY_H
#define SHARED_MEMORY_H

#define SEND 0
#define RECEIVED 1

#define SHARED_TO_COMM "/ParsToCommDom" /* Funny enougth to look like Condoms */
#define SHARED_TO_SERV "/ParsToServDom"

#define SHARED_FROM_COMM "/CommToParsDom"
#define SHARED_FROM_SERV "/ServToParsDom"

#define FLAG_MODE_OPEN 0777

#define MQ_FLAG O_NONBLOCK
#define MAX_MSG_TO_SERV 10
#define MAX_MSG_TO_COMM 10

#include "struct_communication.h"


/**
 * Create all message queue needed
 *
 * %param _c : Communication struct to fill
 */
void createMessageQueue(Communication *_c);

/**
 * Free and close message queue
 *
 * %param _c : Communication struct to empty
 */
void freeMessageQueue(Communication *_c); 

/**
 * Connect to message queue needed
 * 
 * %param _c : Communication struct to fill
 */
void connectMessageQueue(Communication *_c);

/**
 * Close the message queue
 *
 * %param _c : Message queue to empty
 */
void closeMessageQueue(Communication *_c);

/**
 * Action on message queue
 * 
 * %param _mqueue : mqueue onto send data
 * %param _data : Data to send
 * %param _data_length : Lenght of data
 * %param _prio : Priority off data on MQ
 * %param _type : Action to do SEND / RECEIVE
 */
int messageQueueAction(int _mqueue, void *_data, int _data_length, int _prio, int _type); 

#endif /* SHARED_MEMORY_H included */
