#ifndef CHECKER_PARSER_H
#define CHECKER_PARSER_H

#include "struct_tram.h"

#define SIZE_OF_ARRAY(X) sizeof(X) / sizeof(X[0])

typedef int (Checker)(char *_data, int _data_lng, Tram *_t);

/** Check the size of _data
 *
 *  %param _data : Data to check (UNUSED)
 *  %param _data_lng : Length of the data
 *  %param _t : Struct to fill (UNUSED)
 *
 *  %return TRUE upon success FALSE otherwise
 */
int checkSize(char *_data, int _data_lng, Tram *_t);

/** Check the start of tram
 *
 *  %param _data : Data to check
 *  %param _data_lng : Length of the data (UNUSED)
 *  %param _t : Struct to fill
 *
 *  %return TRUE upon success FALSE otherwise
 *  -> Fill 'type' in _t
 */
int checkStart(char *_data, int _data_lng, Tram *_t);

/** Check the end of the tram
 *
 *  %param _data : Data to check
 *  %param _data_lng : Length of the data
 *  %param _t : Struct to fill
 *
 *  %return TRUE upon success FALSE otherwise
 *  -> Fill 'total_data' in _t
 */
int checkEnd(char *_data, int _data_lng, Tram *_t); 

/** Parse the actual data
 *
 *  %param _data : Data to check
 *  %param _data_lng : Length of the data
 *  %param _t : Struct to fill
 *
 *  %return TRUE upon success FALSE otherwise
 *  -> Fill 'data' in _t
 */
int parseData(char *_data, int _data_lng, Tram *_t); 

/** Call all check / parsing function
 *
 *  %param _data : Data to check
 *  %param _data_lng : Length of the data
 *  %param _t : Struct to fill
 *
 *  %return TRUE upon success FALSE otherwise
 */
int parse(char *_data, int _data_lng, Tram *_t); 

int toTram(Tram *_t, char *_data); 
#endif /* CHECKER_PARSER_H included */
