#ifndef READFILE_H
#define READFILE_H

#include "struct_tram.h"

#define EXTENSION "PARS"
#define MAX_FILE_NAME 32

/* Global variable in order to start from everywhere */
char *__REALPATH;

/**
 * Get realpath according to argv0
 *
 * %param _argv0 : Path to app
 * 
 * %return : Path to app without bin name
 */
char *callRealpath(char *_argv0);

/**
 * Define the logger output
 *
 * %param _argv : Name of ouput if file
 */
void defineFileLogger(char *_argv); 

/**
 * Get realpath of the file, concat _path and _filename
 *
 * %param _path : Path to file
 * %param _filename : File name
 */
void getRealPath(char *_path, const char *_filename); 

/**
 * Create a new file name
 *
 * %param _filename : File name start
 * %param _total_file : Number to add to filename
 */
void getFileName(char *_filename, int _total_file);

/**
 * Create the new _filename
 * 
 * %param _filename : Name of the file
 *
 * %return : File pointer on the file
 */
FILE* createFile(char *_filename);

/**
 * Write data in the tram file
 *
 * %param _filename : Filename to write
 * %param _from : Socket number message come from
 * %param _t : Tram to send
 */
void writeInTramFile(char *_filename, int _from, Tram *_t);

/**
 * Load a tram from file
 *
 * %param _filename : Filename to read from
 * %param _from : Will countain from number
 * %param _t : Will contrain tram
 */
void loadTram(const char *filename, int *_from, Tram *_t);

/**
 * Write an Stram in file
 *
 * %param _filename : Filename to write
 * %param _from : Socket number mesage come from
 * %param _stram : Stram to write
 */
void writeStramInFile(char *_filename, int _from, Stram *_stram);

/**
 * Read an Stram from file
 *
 * %param _filename : Filename to read from
 * %param _from : Will contain from number
 * %param _stram : Will contain the Stram
 */
void loadStram(const char *_filename, int *_from, Stram _stram);

#endif
