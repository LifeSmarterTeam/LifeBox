#include <unistd.h>
#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <mqueue.h>


#include "log.h"
#include "boolean.h"
#include "readfile.h"
#include "message_queue.h"

#include "struct_tram.h"

int __openQueue(char *_location, int _flags, struct mq_attr *_attr) 
{
    int mqd = 0;

    if ((mqd = mq_open(_location, _flags, FLAG_MODE_OPEN, _attr)) == (mqd_t) -1) {
        if(errno == EEXIST) {
            LOG_PRINT(LOG_ERROR, "Message queue already existing");
        }
        if(errno != EEXIST && errno != ENOENT) LOG_PRINT(LOG_FATAL, "Failed to open message queue %s", _location);
    }

    return mqd;
}

struct mq_attr __initServerMQ()
{
    struct mq_attr attr;
    
    attr.mq_flags = MQ_FLAG;
    attr.mq_maxmsg = MAX_MSG_TO_SERV;
    attr.mq_msgsize = MAX_FILE_NAME;
    attr.mq_curmsgs = 0;

    return attr;
}

struct mq_attr __initCommMQ()
{
    struct mq_attr attr;
    
    attr.mq_flags = MQ_FLAG;
    attr.mq_maxmsg = MAX_MSG_TO_COMM;
    attr.mq_msgsize = MAX_FILE_NAME;
    attr.mq_curmsgs = 0;

    return attr;
}

int __setGetMaxMQD(Communication *_c) 
{
    _c->max_mdq = MAX(_c->max_mdq, _c->from.communication.mqd);
    _c->max_mdq = MAX(_c->max_mdq, _c->from.server.mqd);

    return _c->max_mdq;
}

void connectMessageQueue(Communication *_c) 
{
    struct stat sb;

    if ( _c->from.communication.mqd <= 0 ) { 
        _c->from.communication.mqd = __openQueue(SHARED_FROM_COMM, O_RDONLY, NULL); 
    } else {
        if (stat("/dev/mqueue" SHARED_FROM_COMM, &sb) != 0) _c->from.communication.mqd = -1;
    }
    LOG_PRINT(LOG_DEBUG, "Connect comm : %d [%s]", _c->from.communication.mqd, SHARED_FROM_COMM);    

    if ( _c->from.server.mqd <= 0 ) {
        _c->from.server.mqd        = __openQueue(SHARED_FROM_SERV, O_RDONLY, NULL); 
    } else {
        if (stat("/dev/mqueue" SHARED_FROM_SERV, &sb) != 0) _c->from.server.mqd = -1;
    }
    LOG_PRINT(LOG_DEBUG, "Connect server : %d [%s]", _c->from.server.mqd, SHARED_FROM_SERV);    

    __setGetMaxMQD(_c);
}

void createMessageQueue(Communication *_c)
{    
    struct mq_attr init_comm = __initCommMQ();
    struct mq_attr init_serv = __initServerMQ();

    _c->to.communication.mqd   = __openQueue(SHARED_TO_COMM, O_CREAT | O_EXCL | O_WRONLY, &init_comm);
    _c->to.server.mqd          = __openQueue(SHARED_TO_SERV, O_CREAT | O_EXCL | O_WRONLY, &init_serv);

    LOG_PRINT(LOG_DEBUG, "To communication : %d [%s]", _c->to.communication.mqd, SHARED_TO_COMM);    
    LOG_PRINT(LOG_DEBUG, "To server : %d [%s]", _c->to.server.mqd, SHARED_TO_SERV);    
    __setGetMaxMQD(_c);
}

int __freeQueue(char *_location) 
{
    int mqd;

    if ( (mqd = mq_unlink(_location)) == (mqd_t) -1)
        LOG_PRINT(LOG_FATAL, "Failed to close message queue %s", _location);

    return mqd;
}

void freeMessageQueue(Communication *_c) 
{
    mq_close(_c->to.communication.mqd);
    mq_close(_c->to.server.mqd);
    
    __freeQueue(SHARED_TO_COMM);
    __freeQueue(SHARED_TO_SERV);
}

void closeMessageQueue(Communication *_c)
{
    mq_close(_c->from.communication.mqd);
    mq_close(_c->from.server.mqd);
}

int messageQueueAction(int _mqueue, void *_data, int _data_length, int _prio, int _type) 
{   
    LOG_PRINT(LOG_DEBUG, "Message queue action [%d] To [%d] Size [%d] Prio [%d]", _type, _mqueue, _data_length, _prio);

    if(_mqueue != -1) {
        if ( _type == SEND ) {
            return mq_send(_mqueue, _data, _data_length, _prio);
        }
        else if ( _type == RECEIVED ) {
            return mq_receive(_mqueue, _data, _data_length, NULL);
        }
       }

    return -1;
}
