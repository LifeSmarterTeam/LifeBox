#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#include "log.h"
#include "boolean.h"
#include "readfile.h"

#include "message_queue.h" 
#include "struct_tram.h"
#include "checker_parser.h"

bool stop = FALSE; 
void signal_trap(int _ignored) { (void) _ignored; stop = TRUE; }

void initFd(Communication *_c, fd_set *_rdfs) 
{
    FD_ZERO(_rdfs);
    
    #ifdef __linux__
    FD_SET(STDIN_FILENO, _rdfs);
    #endif

    if (_c->from.server.mqd != -1)
        FD_SET(_c->from.server.mqd, _rdfs);

    if(_c->from.communication.mqd != -1 )
        FD_SET(_c->from.communication.mqd, _rdfs);
}

int main(int _argc, char *_argv[]) 
{
    int timer;
    fd_set rdfs;
    struct timeval tval;

    char filename[MAX_FILE_NAME] = {0};
    
    int from;

    Tram t;
    Communication c = {0};
    
    __REALPATH = callRealpath(_argv[0]);
	defineFileLogger("SERV");

    (void) signal(SIGUSR1, &signal_trap);

    #ifndef DEBUG 
        if( _argc > 1 ) 
            if(_argv[1][0] == '-')
                if(_argv[1][1] == 'v')
                     printf("Version : %d.%d.%d\n", MAJOR_VERSION, MINOR_VERSION, REVISION_VERSION), exit(EXIT_SUCCESS);
    #else
        (void) _argc;
        (void) _argv;
    #endif

    tval.tv_sec = 5;
    tval.tv_usec = 0;
    
    LOG_PRINT(LOG_INFO, "Press Enter to Stop the Parser");  

    createMessageQueue(&c);

    for(;;) {
        connectMessageQueue(&c);
        initFd(&c, &rdfs);

        LOG_PRINT(LOG_INFO, "Waiting for message ...");  
        LOG_PRINT(LOG_DEBUG, "Selecting on [%d] ...", c.max_mdq);  
        if( (timer = select(c.max_mdq + 1, &rdfs, NULL, NULL, &tval)) == -1) {
            if(errno == EINTR && stop == TRUE ) break;
            LOG_PRINT(LOG_FATAL, "Can't select : ");
        }

        if( timer == 0 ) { /* Nothing */
            LOG_PRINT(LOG_INFO, "No message - Rechecking in few second");  
            tval.tv_sec = 5;
        }

        #ifdef __linux__
        if( FD_ISSET(STDIN_FILENO, &rdfs) ) {
            break;
        }
        #endif

        if(c.from.communication.mqd != -1) {
            if( FD_ISSET(c.from.communication.mqd, &rdfs) ) {
                LOG_PRINT(LOG_INFO, "Message from communication. Processing !");  

                messageQueueAction(c.from.communication.mqd, filename, sizeof(filename), 0, RECEIVED);
                loadStram(filename, &from, t.string_tram);        
                if (parse(t.string_tram, strlen(t.string_tram) - 1, &t) != FALSE) {        
					/* On change le nom de fichier */
					writeInTramFile(filename, from, &t);
					
					messageQueueAction(c.to.server.mqd, filename, sizeof(filename), 0, SEND);
				}

                if(t.data != NULL) free(t.data); /* On verifi si le pointeur est alloué, car si le parseur échou avant l'étape '4' alors rien est alloc */
            }
        }

        if(c.from.server.mqd != -1) {
            if( FD_ISSET(c.from.server.mqd, &rdfs) ) {
                LOG_PRINT(LOG_INFO, "Message from server. Processing !");  

                messageQueueAction(c.from.server.mqd, filename, sizeof(filename), 0, RECEIVED);
                loadTram(filename, &from, &t);
                if (toTram(&t, t.string_tram) != FALSE) {	
					writeStramInFile(filename, from, &(t.string_tram));
					
					messageQueueAction(c.to.communication.mqd, filename, sizeof(filename), 0, SEND);
				}

                free(t.data);
            }
        }
    }
    
    LOG_PRINT(LOG_INFO, "Stopping Parser.");  
    closeMessageQueue(&c);
    freeMessageQueue(&c);
    free(__REALPATH); 
#ifdef OUTPUT_FILE
	fclose(LOGGER_OUTPUT);
#endif
    exit(EXIT_SUCCESS);
}
