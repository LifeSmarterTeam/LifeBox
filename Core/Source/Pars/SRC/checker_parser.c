#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "log.h"
#include "boolean.h"
#include "struct_tram.h"
#include "checker_parser.h"

int checkSize(char *_data, int _data_lng, Tram *_t)
{
    (void) _data;
    (void) _t;

    if (_data_lng < MIN_TRAM_SIZE && _data_lng > MAX_TRAM_SIZE) {
        LOG_PRINT(LOG_DEBUG, "Wrong data length : [%d] < [%d] < [%d]", MIN_TRAM_SIZE, _data_lng, MAX_TRAM_SIZE);
        return FALSE;
    }

    return TRUE;
}

int checkStart(char *_data, int _data_lng, Tram *_t)
{
    int tram_type;
    char tmp[TYPE_SIZE + 1] = {0}; 

    (void) _data_lng;

    if (_data[0] != OPEN_COM) {
        LOG_PRINT(LOG_DEBUG, "Wrong data openner [%c] waiting [%c]", _data[0], OPEN_COM);
        return FALSE;
    }
    
    if (_data[TYPE_SIZE + 1] != OPEN_TRAM) {
        LOG_PRINT(LOG_DEBUG, "Wrong tram openner [%c] waiting [%c]", _data[TYPE_SIZE + 1], OPEN_TRAM);
        return FALSE;
    }

    strncpy(tmp, _data + 1, TYPE_SIZE);
    tram_type = atoi(tmp);

    if (tram_type < 0) {
        LOG_PRINT(LOG_DEBUG, "Wrong type tram [%d] from [%s]", tram_type, tmp);
        return FALSE;
    }

    _t->type = tram_type;

    return TRUE;
}

int checkEnd(char *_data, int _data_lng, Tram *_t) 
{
    int total_data;
    char tmp[TYPE_SIZE + 1] = {0}; 
        
    if (_data[_data_lng] != CLOSE_COM) {
        LOG_PRINT(LOG_DEBUG, "Wrong data closer [%c] waiting [%c]", _data[_data_lng], CLOSE_COM);
        return FALSE;
    }

    if (_data[_data_lng - TOTAL_SIZE - 1] != CLOSE_TRAM) {
        LOG_PRINT(LOG_DEBUG, "Wrong tram closer [%c] waiting [%c]", _data[_data_lng - TOTAL_SIZE - 1], CLOSE_TRAM);
        return FALSE;
    }
    
    strncpy(tmp, _data + _data_lng - TOTAL_SIZE, TOTAL_SIZE);
    total_data = atoi(tmp);

    if (total_data < 0) {
        LOG_PRINT(LOG_DEBUG, "Wrong total data size [%d] from [%s]", total_data, tmp);
        return FALSE;
    }

    _t->total_data = total_data;

    return TRUE;
}

int parseData(char *_data, int _data_lng, Tram *_t) 
{
    int i, j;
    char *start, *substart;
    char *token, *subtoken;
    char *sv1, *sv2;    
    
    _data[_data_lng - TOTAL_SIZE - 1] = 0;
    start =_data + TYPE_SIZE + 2; /* SOH & ETX */

    _t->data = calloc(_t->total_data, sizeof(*(_t->data)));    

    for (i = 0; i < _t->total_data; i++, start = NULL) {
        token = strtok_r(start, DELIM_BLOCK, &sv1);
        if (token == NULL) {
            LOG_PRINT(LOG_DEBUG, "Something wrong [%d] token instead of [%d]", i, _t->total_data);
            return FALSE;
        }

        for (j = 0, substart = token; j < 2; j++, substart = NULL) {
            subtoken = strtok_r(substart, DELIM_DATA, &sv2);
            if (subtoken == NULL) { 
                LOG_PRINT(LOG_DEBUG, "Something wrong parsing [%s]", token);
                return FALSE;
            }

            if ( j == 0 ) _t->data[i].param = atoi(subtoken);
            else          strcpy(_t->data[i].value, subtoken);
        }
    }

    return TRUE;
}

int parse(char *_data, int _data_lng, Tram *_t) 
{
    int i = 0;

    /* Etape 1 - Verification des tailles 
     * Etape 2 - Verification de l'ouverture de la tram 
     * Etape 3 - Verification de la fin, extraction du nombre d'élement a parser 
     * Etape 4 - Parsing de la data
     **/
    Checker *checker[] = {checkSize, checkStart, checkEnd, parseData};    
    int checker_size = SIZE_OF_ARRAY(checker);
    
    LOG_PRINT(LOG_DEBUG, "Tram to parse [%s] size [%d]", _data, _data_lng);
    
    _t->data = NULL;
    for (i = 0; i < checker_size; i++) {
        if ( checker[i](_data, _data_lng, _t) == FALSE ) return FALSE;
    }

    LOG_PRINT(LOG_DEBUG, "Parsing OK - Type [%d] - Total [%d]", _t->type, _t->total_data);

    for (i = 0; i < _t->total_data; i++ )
        LOG_PRINT(LOG_DEBUG, "Param : [%d] - Value [%s]", _t->data[i].param, _t->data[i].value);

    return TRUE;
}

int toTram(Tram *_t, char *_data) 
{
    int i, pos;

    _data[pos] = OPEN_COM; pos++;
    pos += sprintf(_data + pos, "%*.*d", TYPE_SIZE, TYPE_SIZE, _t->type);
    _data[pos] = OPEN_TRAM; pos++;

    for (i = 0; i < _t->total_data; i++ ) {
        pos += sprintf(_data + pos, "%*.*d", SUBTYPE_SIZE, SUBTYPE_SIZE, _t->data[i].param);
        _data[pos] = DELIM_DATA[0]; pos++;
        pos += sprintf(_data + pos, "%s", _t->data[i].value);    
        
        if ( i < _t->total_data - 1) _data[pos++] = DELIM_BLOCK[0];
    }

    _data[pos] = CLOSE_TRAM; pos++;
    pos += sprintf(_data + pos, "%*.*d", TYPE_SIZE, TYPE_SIZE, _t->total_data);
    _data[pos] = CLOSE_COM; pos++;
    _data[pos] = 0; pos++;

    return TRUE;
}

