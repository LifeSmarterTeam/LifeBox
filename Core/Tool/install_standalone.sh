#!/bin/bash

function display_box {
    msg="# $* #"
    edge=$(echo "$msg" | sed 's/./=/g')

    echo ""
    echo "$edge"
    echo "$msg"
    echo "$edge"
    echo ""
}


url="https://gitlab.com/LifeSmarterTeam/LifeBox/raw/master/Core"
testing=(Serv Pars Comm)

mkdir -p Exe Tool

echo "Getting proper install"
wget "${url}/Tool/install.sh" -qO "./Tool/install.sh"
chmod +x "./Tool/install.sh"

display_box "Installing auto update watcher ?"
read -n 1 -p "Doing this step (Y/n)" -r

if [[ ! $REPLY =~ ^[Nn]$ ]]; then
	wget "${url}/Tool/check_update.sh" -qO "./Tool/check_update.sh"
	chmod 744 "./Tool/check_update.sh"
fi;

display_box "Installing requierement (if you want to compile) ?"
read -n 1 -p "Doing this step (Y/n)" -r

if [[ ! $REPLY =~ ^[Nn]$ ]]; then
	wget "${url}/Tool/install_requierement.sh" -qO "./Tool/install_requierement.sh"
	chmod +x "./Tool/install_requierement.sh"
fi;

for exe in "${!testing[@]}"; do 

	name=${testing[$exe]};
	echo "Getting ${name} Service And Exe"

	mkdir -p "./Exe/${name}"
	wget "${url}/Exe/${name}/Out" -qO "./Exe/${name}/Out"
	wget "${url}/Exe/${name}/${name}.service" -qO "./Exe/${name}/${name}.service"

done;


echo ""
echo "Needing root for real install"
echo "Running install script"
(cd Tool &&  sudo ./install.sh)

rm "install_standalone.sh"
