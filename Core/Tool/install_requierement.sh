#!/bin/bash

function display_box {
    msg="# $* #"
    edge=$(echo "$msg" | sed 's/./=/g')

    echo ""
    echo "$edge"
    echo "$msg"
    echo "$edge"
    echo ""
}

bash_check='which'
package_manager='apt install'

function check_command {
    eval ! $1 "'"$2"'";
    return $?;
}

package_bash=(curl-config)
package_install_bash=(libcurl4-openssl-dev)


display_box "Checking / installing bash package"

read -n 1 -p "Doing this step (Y/n)? " -r 
echo ""

if [[ ! $REPLY =~ ^[Nn]$ ]]; then
    for pb in "${!package_bash[@]}"; do 
        echo "Checking existing ${package_bash[$pb]}";
        if check_command "${bash_check}" "${package_bash[$pb]}"; then
            echo "Going to install ${package_install_bash[$pb]}";
            $package_manager ${package_install_bash[$pb]}
        fi
    done;
fi;


