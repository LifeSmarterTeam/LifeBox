#!/bin/bash

echo "Uninstalling ..";

if [ "$EUID" -ne 0 ]; then
    echo "This script must be run as root" 
    exit 1
fi

rm -f /etc/cron.daily/update_SL

echo "Checking service states"
systemctl is-active --quiet Comm.service && echo "Service 'Comm.service' is running, please stop it"
systemctl is-active --quiet Serv.service && echo "Service 'Serv.service' is running, please stop it"
systemctl is-active --quiet Pars.service && echo "Service 'Pars.service' is running, please stop it"

echo "Stopping all services"
systemctl stop Comm.service
systemctl stop Serv.service
systemctl stop Pars.service

echo "Removing exchange file"
rm -rf /tmp/SmarterLife/*

echo "Disabling service"
systemctl disable Comm.service
systemctl disable Pars.service
systemctl disable Serv.service

echo "Removing bin"
rm /usr/bin/Comm
rm /usr/bin/Pars
rm /usr/bin/Serv

echo "Removing services"
rm /etc/systemd/system/Comm.service
rm /etc/systemd/system/Pars.service
rm /etc/systemd/system/Serv.service

echo "Removing partition Exchange -- Creating backup file"
sed -i.bak "\@/tmp/SL_Exchange@d" /etc/fstab

echo "Uninstall done."
