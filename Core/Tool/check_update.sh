#!/bin/bash

if [ "$EUID" -ne 0 ]; then
    echo "This script must be run as root"
    exit 1
fi


testing=(Serv Pars Comm)

for exe in "${!testing[@]}"; do 
	echo "Checking version of ${testing[$exe]}";
	url="https://gitlab.com/LifeSmarterTeam/LifeBox/-/raw/master/Core/Exe/${testing[$exe]}"
	online_version=$(wget -qO- "${url}/VERSION")
	current_version=$(/usr/bin/${testing[$exe]} -v)
	echo "Current version : ${current_version}"
	if [ "${online_version}" != "${current_version}" ]; then
		echo "Going to update ${testing[$exe]} --> ${online_version}";

		# Stopping service
		systemctl stop ${testing[$exe]}.service

		# wgetting
		wget "${url}/Out" -qO /usr/bin/${testing[$exe]} 

		# Restart service
		systemctl start ${testing[$exe]}.service
	fi
	echo ""
done;
