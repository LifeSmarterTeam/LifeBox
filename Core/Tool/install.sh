#!/bin/bash

function display_box {
    msg="# $* #"
    edge=$(echo "$msg" | sed 's/./=/g')

    echo ""
    echo "$edge"
    echo "$msg"
    echo "$edge"
    echo ""
}

if [ "$EUID" -ne 0 ]; then
    echo "This script must be run as root" 
    exit 1
fi

action=(Serv Pars Comm)

display_box "Checking requierement ?"

read -n 1 -p "Doing this step (Y/n)? " -r 
echo ""

if [[ ! $REPLY =~ ^[Nn]$ ]]; then
	./install_requierement.sh
fi

display_box "Installing auto updater ?"

read -n 1 -p "Doing this step (Y/n)? " -r 
echo ""

if [[ ! $REPLY =~ ^[Nn]$ ]]; then
	cp check_update.sh /etc/cron.daily/updater_SL
fi

for exe in "${!action[@]}"; do
	name=${action[$exe]};

	echo "Copying ${name}, Chmod, Adding service and Enable it";
	cp ../Exe/${name}/Out /usr/bin/${name}
	chmod +x /usr/bin/${name}
	cp ../Exe/${name}/${name}.service /etc/systemd/system/${name}.service
	systemctl enable ${name}.service
done;

echo "Installation done"

echo ""
echo "Creating needed space for exchange file [50 Mo]"
grep -q '/tmp/SL_Exchange' /etc/fstab || echo "none     /tmp/SL_Exchange     tmpfs     defaults,size=50M     0 0" >> /etc/fstab

echo "Reboot in order to make all working, or" 
echo "Run : "
echo "mkdir /tmp/SL_Exchange"
echo "sudo mount -t tmpfs -o size=50M tmpfs /tmp/SL_Exchange"
echo ""
echo "sudo systemctl start Comm.service"
echo "sudo systemctl start Pars.service"
echo "sudo systemctl start Serv.service"
echo "In order to start services !"
