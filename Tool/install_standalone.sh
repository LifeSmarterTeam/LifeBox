#!/bin/bash

url="https://gitlab.com/LifeSmarterTeam/LifeBox/raw/master"

testing=(Core Web)

for exe in "${!testing[@]}"; do
	name=${testing[$exe]};
	mkdir -p "../$name"
	wget "${url}/$name/Tool/install_standalone.sh" -O "../$name/install_standalone.sh"
	chmod +x "../$name/install_standalone.sh"
	(cd "../${name}" && ./install_standalone.sh)
done;

rm "install_standalone.sh"
