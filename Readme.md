Pour l'installation voici les étapes : 
1. il faut aller sur : https://www.armbian.com/orange-pi-zero/
2. Puis télécharger : Armbian Bionic Ou Armbian Buster
3. Il faut le dézipper et vous aurez un Armbian_....img
4. Installer l'image sur une carte sd (sur windows logiciel Win32Disk imager exemple) 
5. Insérer la carte SD une fois l'image écrite sur la carte sd dans l'orange pi et connecter l'alimentation et le port ethernet à votre box
6. Une LED verte devrait être allumé sur orange pi, il faut trouver l'ip de votre orange pi dans votre sous réseau (via la page de votre box internet exemple free 192.168.0.254)
7. Ouvrer une connexion ssh avec votre orange pi (via un logiciel comme putty ou mobaxterne) l'utilisateur par défaut est __root__ et mot de passe par défaut est __1234__
> Plusieurs étapes seront à effectuer comme changer le mot de passe root, et créer un nouvel utilisateur. Laissez vous guider. 
8. Lancer la commande pour mettre a jour l'orange pi `sudo apt-get update && sudo apt-get upgrade -y`
9. Lancer la commande ` wget https://gitlab.com/LifeSmarterTeam/LifeBox/raw/master/Tool/install_standalone.sh `
10. Lancer la commande ` chmod +x install_standalone.sh `
11. Lancer la commande qui permet d'installer tous ce qui est nécessaire au fonctionnement de la LifeBox `./install_standalone.sh | yes Y`
12. Une fois tous insallé (ecrire -y partout sauf si `| yes Y` ajouté) fait la commande ```sudo reboot```
13. Vous venez d'installer l'os armbian et l'application LifeBox sur votre orange pi zero.

Une autre possibilité plus simple est de télécharger os fournit sur le site [LifeSmarter](https://lifesmarter.fr/fr/) et de faire juste la partie installation du .img