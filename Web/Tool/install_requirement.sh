#!/bin/bash

function display_box {
    msg="# $* #"
    edge=$(echo "$msg" | sed 's/./=/g')

    echo ""
    echo "$edge"
    echo "$msg"
    echo "$edge"
    echo ""
}


bash_check='which'
package_manager='apt install'
python_check="python3 -c"
python_install='pip3 install'

function check_command {
    eval ! $1 "'"$2"'";
    return $?;
}

# git will be removed
package_bash=(python3 python3-config pip3 nginx git)
package_install_bash=(python3 python3-dev python3-pip nginx git)
package_python=(setuptools django wheel posix_ipc requests subprocess socket gunicorn celery)
package_install_python=(setuptools django wheel posix_ipc requests subprocess socket gunicorn celery)

display_box "Checking / installing bash package"

read -n 1 -p "Doing this step (Y/n)? " -r 
echo ""

if [[ ! $REPLY =~ ^[Nn]$ ]]; then
    for pb in "${!package_bash[@]}"; do 
        echo "Checking existing ${package_bash[$pb]}";
        if check_command "${bash_check}" "${package_bash[$pb]}"; then
            echo "Going to install ${package_install_bash[$pb]}";
            $package_manager ${package_install_bash[$pb]}
        fi
    done;
fi;

display_box "Checking / installing python package"

read -n 1 -p "Doing this step (Y/n)? " -r 
echo ""

if [[ ! $REPLY =~ ^[Nn]$ ]]; then
    for pp in "${!package_python[@]}"; do 
        echo "Checking existing ${package_python[$pp]}";
        if check_command "${python_check}" "import ${package_python[$pp]}"; then
            echo "Going to install ${package_install_python[$pp]}";
            $python_install ${package_install_python[$pp]}
        fi
    done
fi

display_box "Creating ap0/wlan0 configuration"

read -n 1 -p "Doing this step (Y/n)? " -r 
echo ""

if [[ ! $REPLY =~ ^[Nn]$ ]]; then

	# Disable old network manager
	systemctl mask networking.service dhcpcd.service wpa_supplicant.service
	mv /etc/network/interfaces /etc/network/interfaces~
	sed -i '1i resolvconf=NO' /etc/resolvconf.conf

	# Enable new entwork manager
	systemctl enable systemd-networkd.service systemd-resolved.service
	ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf
	
    # Creating interface for eth0
    echo -e '[Match]\nName=e*\n[Network]\n# to use static IP (with your settings) toggle commenting the next 8 lines.\n#Address=192.168.50.60/24\n#DNS=84.200.69.80 1.1.1.1\n#[Route]\n#Gateway=192.168.50.1\n#Metric=10\nDHCP=yes\n[DHCP]\nRouteMetric=10' > /etc/systemd/network/04-eth.network
    chmod 644 /etc/systemd/network/04-eth.network
    
	# Creating interface for wlan0
	echo -e 'country=FR\nctrl_interface=/var/run/wpa_supplicant\nctrl_interface_group=netdev\nupdate_config=1\n\nnetwork={\n\tssid="Domdom"\n\tpsk="12345678"\n}' > /etc/wpa_supplicant/wpa_supplicant-wlan0.conf	
	chmod 644 /etc/wpa_supplicant/wpa_supplicant-wlan0.conf
	echo -e '[Match]\nName=wlan0\n[Network]\nDHCP=yes' > /etc/systemd/network/08-wlan0.network
	
	# Disable old wpa supplicant
	systemctl disable wpa_supplicant.service

	# Setup new wpa supplicant
	systemctl disable wpa_supplicant@wlan0.service

	# Creating ap0 interfaces
	echo -e 'country=FR\nctrl_interface=/var/run/wpa_supplicant\nctrl_interface_group=netdev\nupdate_config=1\n\nnetwork={\n\tssid="LifeSmarter"\n\tmode=2\n\tkey_mgmt=WPA-PSK\n\tproto=RSN WPA\n\tpsk="ChangeMoiVite123465789"\n\tfrequency=2412\n}' > /etc/wpa_supplicant/wpa_supplicant-ap0.conf
	chmod 644 /etc/wpa_supplicant/wpa_supplicant-ap0.conf
	echo -e '[Match]\nName=ap0\n[Network]\nAddress=192.168.4.1/24\nDHCPServer=yes\n[DHCPServer]\nDNS=84.200.69.80 1.1.1.1' > /etc/systemd/network/12-ap0.network 

	# Creating systemd service
	systemctl disable wpa_supplicant@ap0.service

	# Updating newly service 
	echo -e '[Unit]\nDescription=WPA supplicant daemon (SL)\nRequires=sys-subsystem-net-devices-wlan0.device\nAfter=sys-subsystem-net-devices-wlan0.device\nConflicts=wpa_supplicant@wlan0.service\nBefore=network.target\nWants=network.target\n\n[Service]\nType=simple\nExecStartPre=/sbin/iw dev wlan0 interface add ap0 type __ap\nExecStart=/sbin/wpa_supplicant -c/etc/wpa_supplicant/wpa_supplicant-%I.conf -i%I\nExecStopPost=/sbin/iw dev ap0 del\n\n[Install]\nAlias=multi-user.target.wants/wpa_supplicant@%i.service' > /etc/systemd/system/wpa_supplicant@ap0.service

	# Enable mode sur SL
	systemctl enable wpa_supplicant@ap0.service

	# Creating Nginx configuration
	echo -e " server {\n\tlisten 80;\tserver_name _;\n\n\tlocation /static/ {\n\t\talias /usr/bin/SWeb/static/;\n\t}\n\n\tlocation / {\n\t\tfastcgi_read_timeout 90;\n\t\tproxy_read_timeout 90;\n\t\tproxy_pass http://localhost:8000;\n\t}\n}" > /etc/nginx/sites-available/default
fi
