#!/bin/bash

# git will be removed
package_bash=(python3 python3-config pip3 nginx git)
package_install_bash=(python3 python3-dev python3-pip nginx git)
package_python=(setuptools django wheel posix_ipc requests subprocess socket gunicorn celery)
package_install_python=(setuptools django wheel posix_ipc requests subprocess socket gunicorn celery)



# Reenable old network manager
systemctl unmask networking.service dhcpcd.service wpa_supplicant.service
systemctl enable systemd-networkd.service systemd-resolved.service

mv /etc/network/interfaces~ /etc/network/interfaces
sed -i '1i resolvconf=YES' /etc/resolvconf.conf
	
systemctl enable wpa_supplicant.service
systemctl disable wpa_supplicant@wlan0.service
systemctl disable wpa_supplicant@ap0.service

rm /etc/systemd/network/04-eth.network
rm /etc/wpa_supplicant/wpa_supplicant-wlan0.conf
rm /etc/systemd/network/08-wlan0.network
rm /etc/wpa_supplicant/wpa_supplicant-ap0.conf
rm /etc/systemd/network/12-ap0.network 

# Remove all the part from SWeb
rm /usr/bin/SWeb_autoupdate.sh
rm /etc/cron.daily/updater_SWeb_SL.sh
rm -r /usr/bin/SWeb

rm /etc/systemd/system/SWeb.service
systemctl disable SWeb.service

