#!/bin/bash

if [ "$#" -lt 2 ]; then
    echo "$0 <SourceBranch> <DestBranch> (Path)"
	exit 2;
fi

SEARCH="Web/Source/"
TMP=json.tmp
PID=15663588
FROM=$1
TO=$2
finalPath=$3
if [ -z "$3" ]; then
	finalPath="/usr/bin/"
fi

URL="https://gitlab.com/api/v4/projects/${PID}/repository/compare?from=${FROM}&to=${TO}"
REP="https://gitlab.com/LifeSmarterTeam/LifeBox/-/raw/master/"

wget -q ${URL} -O $TMP
path=($(awk 'BEGIN { FS="\""; RS="," }; { if ($2 == "new_path") if($4 ~ var ) print $4 }' var="${SEARCH}" $TMP))
rm $TMP

echo "Processing ...."
for p in "${!path[@]}"; do 
	fullp_get=${path[$p]#${SEARCH}}
	fullp=${finalPath}${fullp_get}
	mkdir -p $(dirname ${fullp})
	wget -q -O "${fullp}" "${REP}${SEARCH}${fullp_get}"
	echo -en "\e[1A";
	echo -e "$p/${#path[@]}               "
done
