#!/bin/bash

name="Web"
url="https://gitlab.com/LifeSmarterTeam/LifeBox/raw/master/Web"

mkdir -p Exe Tool

echo "Getting proper install"
wget "${url}/Tool/install_requirement.sh" -qO "./Tool/install_requirement.sh"
chmod +x "./Tool/install_requirement.sh"

wget "${url}/Tool/update_SWeb_SL.sh" -qO "./Tool/update_SWeb_SL.sh"
chmod +x "./Tool/update_SWeb_SL.sh"

wget "${url}/Tool/auto_update.sh" -qO "./Tool/auto_update.sh"
chmod +x "./Tool/auto_update.sh"

wget "${url}/Tool/install.sh" -qO "./Tool/install.sh"
chmod +x "./Tool/install.sh"

echo ""
echo "Needing root for real install"
echo "Running install script"
(cd Tool &&  sudo ./install.sh)

rm "install_standalone.sh"
