#!/bin/bash

url="https://gitlab.com/LifeSmarterTeam/LifeBox/raw/master/Web/"
online_version=$(wget -qO- "${url}/VERSION")
current_version=$(/usr/bin/python3 /usr/bin/SWeb/setup.py --version)

echo "Current version : ${current_version}"
if [ "${online_version}" != "${current_version}" ]; then
	/usr/bin/SWeb_autoupdate.sh "SWEB${current_version}" "master" "/usr/bin/"
fi
