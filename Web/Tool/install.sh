#!/bin/bash

function display_box {
    msg="# $* #"
    edge=$(echo "$msg" | sed 's/./=/g')

    echo ""
    echo "$edge"
    echo "$msg"
    echo "$edge"
    echo ""
}

finalPath="/usr/bin"

echo "Checking requirement"
sudo ./install_requirement.sh

display_box "Installing auto updater ?"

read -n 1 -p "Doing this step (Y/n)? " -r
echo ""

if [[ ! $REPLY =~ ^[Nn]$ ]]; then
 	cp ./auto_update.sh /usr/bin/SWeb_autoupdate.sh
	cp ./update_SWeb_SL.sh /etc/cron.daily/updater_SWeb_SL.sh
fi

echo "Installing the Server"
# Even if it's not installed we need to use it in order to install all the thing
(./auto_update.sh Base master "$finalPath/") 2>&1 

cp "$finalPath/SWeb.service" /etc/systemd/system/SWeb.service
systemctl enable SWeb.service

echo "Reboot in order to make all working, or" 
echo "sudo systemctl start SWeb.service"
echo "In order to start services !"


