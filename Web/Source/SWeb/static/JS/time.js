function checkTime(i) {
	if (i >= 10) return i;
	return "0" + i;
}

function startTime() {
  var today = new Date();
  var h = checkTime(today.getHours());
  var m = checkTime(today.getMinutes());
  document.getElementById('heure').innerHTML = h + ":" + m ;
  setTimeout(function() { startTime() }, 30000);
}

startTime();


