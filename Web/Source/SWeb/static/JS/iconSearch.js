function dispo(searchID, disp) {
	document.getElementById(searchID).style.display = disp;
}

function searchSelect(searchForID, searchInID) {
  var ico = document.getElementById(searchInID);

  var input = document.getElementById(searchForID).value.toLowerCase();
  var output = document.getElementsByClassName(ico.firstElementChild.className);

  for (var i = output.length - 1; i >= 0 ; i--) {
    output[i].style.display = (output[i].firstChild.className.includes(input)) ? 'block' : 'none';
  }
}

function updateIcon(val) {
	val.nextSibling.firstChild.className = '';
	var split = val.value.split(" ");
	for(var i = 0; i < split.length; i++)
		val.nextSibling.firstChild.classList.add(split[i]);
}

document.addEventListener('click', function (event) {
	var classNameList = '';
	if (event.target.parentNode.classList.contains('icon')) {
		classNameList = event.target.className;
	} else if(event.target.classList.contains('icon')) {
		classNameList = event.target.firstChild.className;
	}

	if(classNameList != '') {
		document.getElementById('id_icon').value = classNameList;
		document.getElementById('id_icon').onchange();
	}
	
	if(event.target.id.indexOf('id_icon') != 0 
	   && event.target.id.indexOf('search') != 0)
		dispo('all', 'none');

}, false);

