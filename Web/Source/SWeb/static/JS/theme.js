var currentTheme = localStorage.getItem('theme') ? localStorage.getItem('theme') : 'dark';
const toggleSwitch = document.querySelector('.theme-switch-header input[type="checkbox"]');
document.documentElement.setAttribute('data-theme', currentTheme);

if (currentTheme == 'dark') toggleSwitch.checked = true;

function switchTheme(e) {
    if (currentTheme == 'dark') {
        document.documentElement.setAttribute('data-theme', 'light');
        localStorage.setItem('theme', 'light'); //add this
        currentTheme = 'light';
    }
    else {
        document.documentElement.setAttribute('data-theme', 'dark');
        localStorage.setItem('theme', 'dark'); //add this
        currentTheme = 'dark';
    }    
}
