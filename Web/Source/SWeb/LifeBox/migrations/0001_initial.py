# Generated by Django 3.0.6 on 2020-05-14 18:07

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
                ('icon', models.CharField(blank=True, max_length=100, null=True, verbose_name='icon')),
                ('description', models.TextField()),
                ('start_time', models.DateTimeField(verbose_name="Date de debut de l'evenement")),
                ('end_time', models.DateTimeField(verbose_name="Date de fin de l'evenement")),
            ],
        ),
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Nom du groupe')),
                ('icon', models.CharField(blank=True, max_length=100, null=True, verbose_name='icon')),
                ('date', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date created')),
            ],
            options={
                'verbose_name': 'group',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ntype', models.IntegerField(default=1, verbose_name='Type de notif')),
                ('level', models.IntegerField(verbose_name='Niveau de notif')),
                ('content', models.CharField(max_length=100, verbose_name='Contenu notif')),
                ('date', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Date creation')),
                ('end_date', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Endtime')),
            ],
            options={
                'verbose_name': 'Notification',
                'ordering': ['date'],
            },
        ),
        migrations.CreateModel(
            name='Parameter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Nom du parametre')),
                ('value', models.CharField(max_length=100, verbose_name='Valeur du parametre')),
                ('date', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date created')),
            ],
            options={
                'verbose_name': 'Parameter',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Regul',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Regulation name')),
                ('icon', models.CharField(blank=True, max_length=100, null=True, verbose_name='icon')),
                ('type_regul', models.IntegerField(blank=True, null=True, verbose_name='Regulation type (Temperature, ...)')),
                ('setpoint', models.CharField(blank=True, max_length=100, null=True, verbose_name='Regulation setpoint string')),
                ('measure', models.CharField(blank=True, max_length=100, null=True, verbose_name='Regulation measure')),
                ('action', models.CharField(blank=True, max_length=100, null=True, verbose_name='Regulation action')),
                ('on', models.BooleanField(default=False, verbose_name='On/Off')),
                ('date', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date created')),
                ('group', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LifeBox.Group')),
            ],
            options={
                'verbose_name': 'regul',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='RegulForType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type_value_tram', models.IntegerField(verbose_name='Type valeur tram')),
                ('name', models.CharField(max_length=100, verbose_name='Nom du parametre')),
                ('value', models.IntegerField(verbose_name='Value for that name')),
                ('signe', models.CharField(max_length=100, verbose_name='signe')),
                ('date', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date created')),
            ],
            options={
                'verbose_name': 'RegulForType',
                'ordering': ['type_value_tram'],
            },
        ),
        migrations.CreateModel(
            name='Sensor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('num_module', models.IntegerField(verbose_name='Numero du module')),
                ('type_value_tram', models.IntegerField(verbose_name='Type valeur tram')),
                ('name', models.CharField(max_length=100, verbose_name='Nom du capteur')),
                ('icon', models.CharField(blank=True, max_length=100, null=True, verbose_name='icon')),
                ('date', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date created')),
                ('online', models.BooleanField(default=False, verbose_name='Online / offline')),
            ],
            options={
                'verbose_name': 'sensor',
                'ordering': ['num_module'],
            },
        ),
        migrations.CreateModel(
            name='Regul_ass',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type_sensor', models.IntegerField(verbose_name='Le type du capteur si il est utilisé en 1 = Mesure, 2 = Action')),
                ('date', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date created')),
                ('regul', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LifeBox.Regul')),
                ('sensor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LifeBox.Sensor')),
            ],
            options={
                'verbose_name': 'Regulation association',
                'ordering': ['date'],
            },
        ),
        migrations.AddField(
            model_name='regul',
            name='sensor',
            field=models.ManyToManyField(related_name='reguls', through='LifeBox.Regul_ass', to='LifeBox.Sensor'),
        ),
        migrations.CreateModel(
            name='Group_ass',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date created')),
                ('group', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='reguls', to='LifeBox.Group')),
                ('sensor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LifeBox.Sensor')),
            ],
            options={
                'verbose_name': 'Group association',
                'ordering': ['date'],
            },
        ),
        migrations.AddField(
            model_name='group',
            name='sensor',
            field=models.ManyToManyField(related_name='groups', through='LifeBox.Group_ass', to='LifeBox.Sensor'),
        ),
        migrations.CreateModel(
            name='Data',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('num_module', models.IntegerField(verbose_name='Numero du module, un module est un microcontroleur')),
                ('type_value_tram', models.IntegerField(verbose_name='Type valeur tram est une des mesures fait par un module')),
                ('value', models.CharField(blank=True, max_length=100, null=True, verbose_name='value')),
                ('date', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date created')),
                ('sensor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LifeBox.Sensor')),
            ],
            options={
                'verbose_name': 'Data',
                'ordering': ['date'],
            },
        ),
    ]
