from django.core.management.base import BaseCommand
from LifeBox.models import Parameter, Sensor
from django.contrib.auth.models import User

class Command(BaseCommand):
    args = ''
    help = 'Populate the database with default parameters'

    def _create_tags(self):
        country = Parameter(name='Country', value="France")
        city = Parameter(name='City', value="Saint-Etienne")
        woeid = Parameter(name='Woeid', value="623868")
        ssid = Parameter(name='SSID', value="LifeSmarter")
        password = Parameter(name='Password', value="ChangeMoiVite123465789")
        ip = Parameter(name='Ip', value="192.168.4.1/24")
        pricing = Parameter(name='Pricing', value="100")
        User.objects.create_superuser(username='LifeSmarter', password = 'LifeSmarter')
        
        country.save()
        city.save()
        woeid.save()
        ssid.save()
        password.save()
        ip.save()
        pricing.save()

    def handle(self, *args, **options):
        self._create_tags()
