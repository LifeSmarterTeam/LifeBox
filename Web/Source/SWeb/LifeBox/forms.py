from django import forms
from .models import *
from LifeBox.utils import *
from LifeBox.regul import *
from django.utils.translation import gettext_lazy as _

class ConnexionForm(forms.Form):
    """ forms des users """
    username = forms.CharField(label=_("Username"), max_length=30)
    password = forms.CharField(label=_("Password"), widget=forms.PasswordInput)
    
class CreateGroupForm(forms.Form):
    """ forms des groupes"""
    name = forms.CharField(max_length=100, label=_("Group name"), required=True)
    icon = forms.CharField(max_length=100, label=_("Icon"), required=True)
       
class Group_assForm(forms.Form):
    """ forms des associations des groupes/capteurs"""
    group = forms.ModelChoiceField(queryset=Group.objects.all(),label = _("Group name"), required=True, empty_label = None)
    sensor = forms.ModelChoiceField(queryset=Sensor.objects.none(), label = _("Sensors"), required=True)   
    
    def __init__(self,*args,**kwargs):
        self.value = kwargs.pop('id', None)
        super(Group_assForm,self).__init__(*args,**kwargs)
        if self.value:
            group = Group.objects.filter(id=self.value)
            soffgrp = Group.objects.get(id=self.value).sensor.all()
            s = Sensor.objects.exclude(id__in=soffgrp)
            self.fields['group'].queryset = group
            self.fields['sensor'].queryset = s
            
class CreateRegulForm(forms.Form):
    """ forms des regulations """
    r = regul()
    name = forms.CharField(max_length=100, label=_("Regulation name"), required=True)
    icon = forms.CharField(max_length=100, label=_("Icon"), required=True)
    type = forms.IntegerField(label=_("Regulation type"), widget=forms.Select(choices=r.form_list_reg()), required=True)  
    
class Regul_assForm(forms.Form):
    """ forms des associations des regulations/capteurs"""
    regul = forms.ModelChoiceField(queryset=Regul.objects.all(),label = _("Regul name"), required=True, empty_label = None)
    sensor = forms.ModelChoiceField(queryset=Sensor.objects.none(), label = _("Sensors"), required=True)  
    
    def __init__(self,*args,**kwargs):
        self.value = kwargs.pop('id_regul', None)
        super(Regul_assForm,self).__init__(*args,**kwargs)
        if self.value:
            regul = Regul.objects.filter(id=self.value)
            soffgrp = Regul.objects.get(id=self.value).sensor.all()
            s = Sensor.objects.exclude(id__in=soffgrp)
            self.fields['regul'].queryset = regul
            self.fields['sensor'].queryset = s
            
class SensorForm(forms.Form):
    """ forms des capteurs"""
    sensor_name = forms.CharField(max_length=100, label = _("Sensor name"), required=True)
    icon = forms.CharField(max_length=100, label=_("Icon"), required=True)
 
class UpdateParameterForm(forms.Form):
    """ forms des parametres"""
    country = forms.CharField(max_length=100, label = _("Country name"), required=True)
    city = forms.CharField(max_length=100, label = _("Weather - City name or zip code"), required=True)
    ssid = forms.CharField(max_length=32, label = _("Wifi name (ssid)"), required=True)
    password = forms.CharField(min_length=8, max_length=63, label = _("Wifi password"), required=True)
    ip = forms.CharField(max_length=19, label = _("Acces point IP"), required=True)
    pricing = forms.CharField(max_length=10, label = _("Tarif Kw/h"), required=True)

class EventForm(forms.ModelForm):
    """ forms des evenements"""
    icon = forms.CharField(max_length=100, label=_("Icon"), required=True)

    class Meta:
        model = Event
        # datetime-local is a HTML5 input type, format to make date time show on fields
        widgets = {
            'start_time': forms.DateInput(attrs={'type': 'date'}, format='%Y-%m-%d'),
            'end_time': forms.DateInput(attrs={'type': 'date'}, format='%Y-%m-%d'),
            }
        fields = '__all__'

        def __init__(self, *args, **kwargs):
            super(EventForm, self).__init__(*args, **kwargs)
            # input_formats parses HTML5 datetime-local input to datetime field
            self.fields['start_time'].input_formats = ('%Y-%m-%d',)
            self.fields['end_time'].input_formats = ('%Y-%m-%d',)

class DictListForm(forms.Form):
    """ forms des dict list"""
    def __init__(self,*args,**kwargs):
        self.value = kwargs.pop('type_value_tram', None)
        super(DictListForm,self).__init__(*args,**kwargs)
        self.fields['dict'] = forms.CharField(max_length=100, label = _("Target"), widget=forms.Select(choices=get_list_dict_form(self.value)), required=True)

class RegulForTypeForm(forms.Form):
    """ forms des dicts lists signes"""
    manage_type = manage_type()
    type_value_tram = forms.CharField(max_length=100, label = _("Sensor type"), widget=forms.Select(choices=manage_type.get_list_form()), required=True)
    name = forms.CharField(max_length=100, label=_("Display name"), required=True)
    value = forms.IntegerField(label=_("Value"), required=True)    
    signe = forms.CharField(label=_("Sign"), widget=forms.Select(choices=[('<', _('Lower')), ('<=', _('Equal or lower')), ('==', _('Equal')), ('=!', _('Different ')), ('>=', _('Equal or superior')), ('>', _('Superior'))]), required=True)    
    