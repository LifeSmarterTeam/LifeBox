from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'LifeBox'
urlpatterns = [

    #MAIN
    path('', views.connexion, name ='connexion'),
    path('home', views.home, name = 'home'),
    path('logout', views.deconnexion, name='deconnexion'),
    path('parameter', views.parameter, name = 'parameter'),

    #CALENDAR
    path('calendar', views.CalendarView.as_view(), name = 'calendar'),
    path('event/new/', views.event, name='event_new'),
    path('event/new/<day>/<month>/<year>', views.event, name='event_new'),
    path('event/edit/<event_id>/', views.event, name='event_edit'),
    path('delete_event/<event_id>',views.delete_event, name='delete_event'),

    #CAPTEUR
    path('sensor', views.sensor, name = 'sensor'),
    path('delete_sensor/<num_module>', views.delete_sensor, name = 'delete_sensor'),
    path('sensor_status/<id_sensor>', views.sensor_status, name = 'sensor_status'),
    path('all_sensor_offline', views.all_sensor_offline, name = 'all_sensor_offline'),
    path('sensor_modif/<id_sensor>', views.sensor_modif, name = 'sensor_modif'),

    #GROUP
    path('create_group', views.create_group, name = 'create_group'),
    path('group/<id_group>', views.group, name = 'group'),
    path('modif_group/<id_group>', views.modif_group, name = 'modif_group'),
    path('delete_group/<id_group>', views.delete_group, name = 'delete_group'),
    path('create_group_association/<id_group>', views.create_group_association, name = 'create_group_association'),
    path('delete_group_association/<id_group>/<id_sensor>', views.delete_group_association, name = 'delete_group_association'),

    #REGUL
    path('create_regul/<id_group>', views.create_regul, name = 'create_regul'),
    path('modif_regul/<id_regul>', views.modif_regul, name = 'modif_regul'),
    path('delete_regul/<id_regul>', views.delete_regul, name = 'delete_regul'),
    path('regul/setpoint/<id_regul>/<value>', views.modif_setpoint, name = 'modif_setpoint'),
    path('regul/status/<id_regul>/<value>', views.regul_on, name = 'regul_on'),
    path('create_regul_association/<id_regul>/<type_sensor>', views.create_regul_association, name = 'create_regul_association'),
    path('delete_regul_association/<id_regul>/<id_sensor>', views.delete_regul_association, name = 'delete_regul_association'),

    #NOTIFICATION
    path('notif/delete/<id_notif>', views.delete_notif, name = 'delete_notif'),

    #DATA
    path('update_status/<num_module>/<type_tram>/<online>', views.update_status_sensor, name = 'update_status_sensor'),
    path('<num_module>/<type_value_tram>/<value>', views.insert_data, name = 'insert_data'),

    #OTHER
    path('404', views.error, name = 'error'),
    path('clean', views.clean, name = 'clean'),
    path('clean_data', views.clean_data, name = 'clean_data'),

    #REGUL TYPE DATA
    path('list_dict', views.list_dict, name = 'list_dict'),
    path('edit_dict/<id_dict>', views.modif_dict, name = 'modif_dict'),
    path('delete_dict/<id_dict>', views.delete_dict, name = 'delete_dict'),

    #WEATHER
    path('weather', views.weather, name = 'weather'),
]