from django.utils.translation import gettext_lazy as _
from LifeBox.models import *
from LifeBox.utils import util_data, manage_type
import time 

class regul:
    """ Classe pour la regulation des capteurs """ 
    def __init__(self): #Constructeur (variable dans cette classe global)
        pass
            
    def switcher(self, regul_group=None, measure={}, action=None):
        """Type_valeur_tram a ajouter ici et le nom correspondant"""
        switcher = {'1':{"name":_("Light On/Off"), "funct_name":'light',"sp_html":r""" ""","meas_html":r""" ""","action_html":r""" """},
                    '2':{"name":_("Light color"), "funct_name":'RGB',"sp_html":r""" ""","meas_html":r""" ""","action_html":r""" """},
                    '3':{"name":_("Temperature"), "funct_name":'temperature',"sp_html":r""" ""","meas_html":r""" ""","action_html":r""" """}}
        
        try: 
            if not(regul_group): 
                return switcher
                
            check = ""
            if regul_group.setpoint == '0.0':
                s_value = '1.0'
            elif regul_group.setpoint == '1.0': 
                s_value = '0.0'
                check = "checked"
            else:
                s_value = 1.0 

            switcher['1'].update({"sp_html":r"""<div class="switch-wrapper"><div class="switch-slider"><label class="label">
                                           <input type="checkbox" name="switch" id="box" value="{0}" {1} onclick="location.href='/regul/setpoint/{2}/{0}'" >
                                           <div class="slider round"></div></label></div></div>""".format(s_value, check, regul_group.id),
                                 "meas_html":r"""<div class="auto showing">
                                                <span><i class="{} mdi mdi-lightbulb fa-4x"></i></span>
                                            </div>""".format('light_color' if regul_group.measure == '1.0' else 'fas'),
                                 "action_html":""})
            switcher['2'].update({"sp_html":"",
            
                                  "meas_html":r"""<div class="progress-circle auto showing" class="temp">
                                                <span class="temp"><i class="mdi mdi-thermometer"></i>{}</span>
                                            </div>""".format(regul_group.measure),
                                  "action_html":""})
                                  
            switcher['3'].update({"sp_html":r""" <span class="ranger">
                                                <input type="range" id="qta_field-{}" value="{}" """.format(regul_group.id,regul_group.setpoint) + 
                                                r"""" min="0" max="30" oninput="showVal(this)" onchange="showValUpd(this, '/regul/setpoint/{}/' """.format(regul_group.id) +
                                                r""" + document.getElementById('qta_field-""" + str(regul_group.id) + r"""').value, true)" />
                                                <div class="progress-circle auto"> 	
                                                    <span class="temp"><i class="mdi mdi-thermometer"></i><br>{}</span>
                                                </div>
                                            </span>""".format(regul_group.setpoint),
                                  "meas_html":r"""<div class="progress-circle auto showing" class="temp">
                                                    <span class="temp"><i class="mdi mdi-thermometer"></i>{}</span>
                                                </div>""".format(regul_group.measure),
                                  "action_html":r""" """})
        except:
            pass
        return switcher
        
    def get_group_regul(self, group):
        manager = manage_type()
        res = []
        regul_group = group.regul_set.all()
        for rg in regul_group:
            action = []
            measure = []
            rg_ass = Regul_ass.objects.filter(regul = rg)
            
            for ass in rg_ass:
                s = Sensor.objects.get(id = ass.sensor.id)
                try:
                    data = Data.objects.filter(num_module = s.num_module, type_value_tram = s.type_value_tram).latest('date')
                    val = data.value
                except: 
                    val = None
                    data = {}
                if ass.type_sensor == 1:
                    measure.append({'sensor' : s, 'data' : data, "as_html":manager.convert_type( s.type_value_tram, group.id, s.id, val)['html']}) 
                elif ass.type_sensor == 2:
                    action.append({'sensor' : s, 'data' : data, "as_html":manager.convert_type(s.type_value_tram, group.id, s.id, val)['html']}) 
                else:
                    pass
            html = self.get_html(rg, measure, action)
            res.append({'regul_group': rg, 'sp_html': html['sp_html'] , 'meas_html':html['meas_html'], 'action_html': html['action_html'] ,'measure': measure, 'action': action})
        return res

    def get_html(self, rg, measure, action):
        return self.switcher(rg, measure, action).get(str(rg.type_regul), {"name":_("Error"), "funct_name":'Error', "sp_html":"Error", "meas_html":"Error", "action_html":"Error"})
    
    def form_list_reg(self):
        list_switcher = self.switcher()
        return [(i, _(list_switcher[i]['name'])) for i in list_switcher.keys()]
         
    def check_reg(self, reg):
        #Si aucun setpoint n'est definit 
        if not(reg.setpoint):
            return _("error, setpoint not define")
        elif reg.on == False: 
            return _("error, regulation is off")
        #Recuperer les mesures et faire une traitement dessus (depend de la regul ...)
        res = self.get_regul(reg)
        res['setpoint'] = reg.setpoint
        reg.setpoint=res['setpoint']
        reg.measure=res['measure']
        reg.action=res['action']
        reg.save()
        return res
        
    def get_regul(self, regul):
        func = getattr(self, self.switcher().get(str(regul.type_regul))['funct_name'], lambda: "Invalid regulation")
        #Amelioration avec un pk__in
        s_meas = [i.sensor for i in regul.regul_ass_set.all().filter(type_sensor = 1)]
        s_act = [j.sensor  for j in regul.regul_ass_set.all().filter(type_sensor = 2)]
        res = func(regul, s_meas, s_act)
        return res
        
    #Function regulation avec une partie mesure et une partie action ajouter les fonctions si besoin    
    def light(self, regul, ass_measure, ass_action):
        #Applique la valeur aux actionneur  
        d = util_data()
        for a in ass_action:
            d.send_data(a, regul.setpoint)
        return {'measure' : regul.setpoint, 'action' : regul.setpoint}
        
    def RGB(self, regul, ass_measure, ass_action):
        #TODO faire la regul RGB
        pass
        
    def temperature(self, regul, ass_measure, ass_action):
        #Temperautre calcul de la moyenne dans la regulation
        d = util_data()
        mes_moy = 0
        
        for s in ass_measure:
            mes_moy = mes_moy + float(Data.objects.filter(num_module = s.num_module, type_value_tram = s.type_value_tram).latest('date').value)
        mes_moy = mes_moy/len(ass_measure)
        
        #Regulation temperature
        err = mes_moy - float(regul.setpoint)
        if err < -0.5:
            #Actionner relais 
            actionneur = '1.0'
        elif err > 0.5:
            #Arret le relais 
            actionneur = '0.0'
        else:
            actionneur = '0.0'
        
        #Applique la valeur aux actionneur si ce elle est différent de précedement   
        for a in ass_action:
            if regul.action != actionneur:
                d.send_data(a, actionneur)
        return {'measure' : mes_moy, 'action' : actionneur}
