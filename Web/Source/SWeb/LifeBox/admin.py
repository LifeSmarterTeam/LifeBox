from django.contrib import admin
from .models import *
from django.utils.text import Truncator

class NotificationAdmin(admin.ModelAdmin):
    """ Notification page admin """
    list_display   = ('date', 'ntype', 'level', 'content', 'end_date', 'delta')
    list_filter    = ('date', 'ntype', 'level')
    date_hierarchy = 'date'
    ordering       = ('date', )
    search_fields  = ('content',)

    fieldsets = (
       ('Général', {
            'fields': ( 'ntype', 'level', 'content', 'end_date')
        }),
    )


class SensorAdmin(admin.ModelAdmin):
    """ Capteur page admin """
    list_display   = ('id','num_module', 'name', 'date', 'type_value_tram', 'online', 'icon')
    list_filter    = ('name', 'type_value_tram','icon')
    date_hierarchy = 'date'
    ordering       = ('date',)
    search_fields  = ('name',)
	
    fieldsets = (
       ('Général', {
            'fields': ( 'num_module', 'type_value_tram', 'online','icon')
        }),
        ('Nom du capteur', {
           'description': 'le nom du capteur associe',
           'fields': ('name', )
        }),
    )   

class GroupAdmin(admin.ModelAdmin):
    """ Group page admin """
    list_display   = ('id', 'name', 'icon', 'date')
    list_filter    = ('name',)
    date_hierarchy = 'date'
    ordering       = ('name',)
    search_fields  = ('name',)
	
    fieldsets = (
       ('Général', {
            'fields': ( 'name', 'icon',)
        }),
    )

class Group_assAdmin(admin.ModelAdmin):
    """ Group assocation page admin """
    list_display   = ('id', 'date')
    date_hierarchy = 'date'
    
class RegulAdmin(admin.ModelAdmin):
    """ Regulation page admin """
    list_display   = ('id', 'name', 'icon', 'type_regul', 'setpoint','measure','action', 'on', 'date')
    list_filter    = ('name',)
    date_hierarchy = 'date'
    ordering       = ('name',)
    search_fields  = ('name',)
	
    fieldsets = (
       ('Général', {
            'fields': ( 'name','type_regul', 'setpoint', 'on', 'icon','group')
        }),
    ) 
    
class Regul_assAdmin(admin.ModelAdmin):
    """ Regulation assocation page admin """
    list_display   = ('id','sensor','type_sensor', 'date')
    list_filter    = ('type_sensor',)
    date_hierarchy = 'date'
	
    fieldsets = (
       ('Général', {
            'fields': ('type_sensor','regul','sensor',)
        }),
    )
    
class DataAdmin(admin.ModelAdmin):
    """ Donnée page admin """
    list_display   = ('id', 'value', 'date', 'num_module', 'type_value_tram')
    list_filter    = ('num_module', 'type_value_tram')
    date_hierarchy = 'date'
    ordering       = ('-date',)
    search_fields  = ('name',)
	
    fieldsets = (
       ('Général', {
            'fields': ('sensor', 'num_module', 'type_value_tram', 'value')
        }),
    )

class ParameterAdmin(admin.ModelAdmin):
    """ Parametre page admin """
    list_display   = ('name', 'value', 'date')
    list_filter    = ('name', 'value',)
    date_hierarchy = 'date'
    ordering       = ('-date',)
    search_fields  = ('name',)

    fieldsets = (
       ('Général', {
            'fields': ( 'name', 'value')
        }),
    )
    
class EventAdmin(admin.ModelAdmin):
    """ Evenenement page admin """
    list_display   = ('title', 'icon', 'description', 'start_time','end_time')

class RegulForTypeAdmin(admin.ModelAdmin):
    """ Regulation type page admin """
    list_display   = ('type_value_tram', 'name', 'value', 'signe', 'date')

# Pour voir le modele dans la page admin
admin.site.register(Sensor, SensorAdmin)
admin.site.register(Group, GroupAdmin)
admin.site.register(Group_ass, Group_assAdmin)
admin.site.register(Regul, RegulAdmin)
admin.site.register(Regul_ass, Regul_assAdmin)
admin.site.register(Data, DataAdmin)
admin.site.register(Parameter, ParameterAdmin)
admin.site.register(Event, EventAdmin)
admin.site.register(RegulForType, RegulForTypeAdmin)
admin.site.register(Notification, NotificationAdmin)
