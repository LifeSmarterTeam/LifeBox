from django.utils.translation import gettext_lazy as _
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from LifeBox.models import *
from LifeBox.forms import *
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin
from LifeBox.utils import *
from LifeBox.regul import *
from LifeBox.wifi import *
from django.views import generic
from django.utils.safestring import mark_safe
from django.urls import reverse
import re
import locale

def connexion(request):
    """ Page de connexion """
    error = False
    if request.method == "POST":
        form = ConnexionForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)  # Nous vérifions si les données sont correctes
            if user:  # Si l'objet renvoyé n'est pas None
                login(request, user)  # nous connectons l'utilisateur
                return redirect('/home') # Redirection vers la page home si connecte
                
            else: # sinon une erreur sera affichée
                error = True
    else:
        form = ConnexionForm()
    return render(request, 'LifeBox/connexion.html', locals())
    
def handler404(request, exception):
    """ Page 404 """
    error_dict = get_info_base()
    error_dict['txt'] = _("Page not found")
    return render(request, 'LifeBox/404.html',  error_dict)
    
@login_required(redirect_field_name='rediriger_par')		
def home(request):
    """ Page home """
    home_dict = get_info_base()
    home_dict['agenda'] = Event.objects.filter(start_time__day__lte=get_date(False).day, end_time__day__gte=get_date(False).day)
    home_dict['error_sensor'] = Sensor.objects.filter(online = False)
    return render(request, 'LifeBox/home.html', home_dict)

@login_required(redirect_field_name='rediriger_par')
def deconnexion(request):
    """ Page deconnexion"""
    logout(request)
    return redirect('/', locals())
    
@login_required(redirect_field_name='rediriger_par')
def error(request, txt="404 Not Found"):
    """ Page d'erreur"""
    error_dict = get_info_base()
    error_dict['txt'] = str(txt)
    return render(request, 'LifeBox/404.html',  error_dict)
    
class CalendarView(LoginRequiredMixin ,generic.ListView):
    """ Page/Class du calendrier"""
    model = Event
    template_name = 'LifeBox/calendar.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        d = get_date(self.request.GET.get('month', None))
        cal = Calendar(d.year, d.month)
        html_cal = cal.formatmonth(withyear=True)
        context = get_info_base()
        context['calendar'] = mark_safe(html_cal)
        return context

@login_required(redirect_field_name='rediriger_par')
def event(request, event_id=None, day=None, month=None, year=None):
    """ Page des evenements"""
    instance = Event()
    if event_id:
        instance = get_object_or_404(Event, pk=event_id)
    else:
        instance = Event()
    data = {'icon' : instance.icon}
    if day != None and month != None and year != None:
        day_ok = day if int(day) > 9 else '0' + day
        month_ok = month if int(month) > 9 else '0' + month
        data = {'start_time' : year+'-'+month_ok+'-'+day_ok, 'end_time': year+'-'+month_ok+'-'+day_ok}
    form = EventForm(request.POST or None, instance=instance, initial=data)
    if request.POST and form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse('LifeBox:calendar'))
    ico = util_icon()
    event_dict = get_info_base()
    event_dict['form'] = form
    event_dict['event_id'] = event_id
    event_dict['icon'] = ico.form_list_icon()
    return render(request, 'LifeBox/event.html', event_dict)
    
@login_required(redirect_field_name='rediriger_par')
def delete_event(request, event_id):
    """ Page suppression d'evenements"""
    if event_id != "None":
        Event.objects.get(id = event_id).delete()
    return HttpResponseRedirect('/calendar')
    
@login_required(redirect_field_name='rediriger_par')
def parameter(request):
    """ Page des parametre """
    if request.method == "POST":
        form = UpdateParameterForm(request.POST)
        if form.is_valid():
            #Ajouter ici les paramêtres a recuperer
            #country
            country = form.cleaned_data['country']
            Parameter.objects.filter(name = 'Country').update(value=country)
            #city
            up = util_param()
            city = form.cleaned_data['city']
            city_name = up.update_woeid(city, country)
            Parameter.objects.filter(name = 'City').update(value=city_name)
            #ssid
            ssid = form.cleaned_data['ssid']
            Parameter.objects.filter(name = 'SSID').update(value=ssid)
            #password
            password = form.cleaned_data['password']
            Parameter.objects.filter(name = 'Password').update(value=password)
            #pricing
            pricing = form.cleaned_data['pricing']
            Parameter.objects.filter(name = 'Pricing').update(value=pricing)
            #ip
            address_ip = form.cleaned_data['ip']

            if re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/\d{1,5}$", address_ip):
                Parameter.objects.filter(name = 'Ip').update(value=address_ip)
            else: 
                return error(request, _("Invalid ip format. Format : XXX.XXX.XXX.XXX/XXXXXX"))

            #Save ssid & pass in file
            maj_ap0 = init()
            maj_ap0.maj_ssid_psk_ap0(ssid, password)
            maj_ap0.maj_ip(address_ip)
            return HttpResponseRedirect('/parameter')
        else:
            msg = _("Error, update parameter please contact LifeSmarter team")
            return error(request, msg)
    else:
        all_group = Group.objects.all() # Nous sélectionnons tous nos articles
        
        #Ajouter ici les paramêtres a préremplir
        data = {'country' : Parameter.objects.filter(name = 'Country')[0].value,
                'city' : Parameter.objects.filter(name = 'City')[0].value,
                'ssid' : Parameter.objects.filter(name = 'SSID')[0].value,
                'password' : Parameter.objects.filter(name = 'Password')[0].value,
                'ip' : Parameter.objects.filter(name = 'Ip')[0].value,
                'pricing' : Parameter.objects.filter(name = 'Pricing')[0].value
                }
        form = UpdateParameterForm(initial=data) 
        parameter_dict = get_info_base()
        parameter_dict['form'] = form
    return render(request, 'LifeBox/parameter.html', parameter_dict)
    
@login_required(redirect_field_name='rediriger_par')
def clean(request):
    """ Page pour nettoyer toutes les données """
    if request.method == "POST":
        Data.objects.all().delete()
        Sensor.objects.all().delete()
        Group_ass.objects.all().delete()
        Group.objects.all().delete()
    return redirect('/parameter')
    
@login_required(redirect_field_name='rediriger_par')
def clean_data(request):
    """ Page pour nettoyer toutes les données des capteurs"""
    if request.method == "POST":
        Data.objects.all().delete()
        a = Sensor.objects.all()
    return redirect('/parameter')    

@login_required(redirect_field_name='rediriger_par')
def sensor(request):
    """ Page des capteurs"""
    if request.method == "POST":
        form = SensorForm(request.POST)
        if form.is_valid():
            #Initialisation class 
            ini = init()
            hote_pi = Parameter.objects.get(name = 'Ip').value.split('/')[0]
            
            #Connect to new module and send ssid et pass of pi
            if (ini.manage_wifi_esp('abcdefg', '123456789') == False):
                msg = "Impossible de se connecter au wifi"
                ini.mode_acces_point()
                return error(request, msg)
            
            # recuperation de la tram d'init pour creer le module
            num_module = ini.get_num_module()
            
            type_value_tram = ini.parse(ini.send_tram_initialisation(num_module, hote_pi))
            if type_value_tram == None:
                msg = "Impossible d'envoyer la tram au capteur"
                ini.mode_acces_point()
                return error(request, msg)
                
            name = form.cleaned_data['sensor_name']
            manager = manage_type()
            for typet in type_value_tram:
                name_sensor = name + " " + str(manager.convert_type(typet)['name'])
                icon = manager.convert_type(typet)['icon']
                Sensor.objects.create(num_module = num_module, type_value_tram = typet, name = name_sensor, icon = icon)
            
            #Retour en mode normal               
            ini.mode_acces_point()
            return redirect('/sensor')
        else: 
            msg = _("Error form is invalid, please contact LS team")
            return error(request, msg)
    else:
        manager = manage_type()
        sensor = Sensor.objects.all()  
        data = {'sensor_name' : _('Sensor name')}
        form = SensorForm(initial=data)
        ico = util_icon()
        sensor_dict = get_info_base()
        sensor_dict['form'] = form
        sensor_dict['icon'] = ico.form_list_icon()
        sensor_dict['uniq_num_module'] = list(set([i.num_module for i in sensor]))
        sensor_dict['all_sensor'] = [{'sensor': i, 'type':manager.convert_type(str(i.type_value_tram))['name']} for i in sensor]
        return render(request, 'LifeBox/sensor.html', sensor_dict)

@login_required(redirect_field_name='rediriger_par')
def delete_sensor(request, num_module):
    """ Page pour supprimer capteurs"""
    s = Sensor.objects.filter(num_module = num_module)
    s.delete()
    return HttpResponseRedirect('/sensor') 

@login_required(redirect_field_name='rediriger_par')
def sensor_status(request, id_sensor):
    """ Page changer le status d'un capteur"""
    manager = manage_type()
    sensor_dict = get_info_base()
    sensor = Sensor.objects.get(id = id_sensor)
    sensor_dict['sensor'] = sensor
    try: 
        sensor_dict['last_data'] = Data.objects.filter(num_module = sensor.num_module, type_value_tram = sensor.type_value_tram).latest('date')
        sensor_dict['data'] = [[int(i.date.timestamp()*1000), float(i.value)] for i in Data.objects.filter(num_module = sensor.num_module, type_value_tram = sensor.type_value_tram)]
    except:
        sensor_dict['last_data'] = {'num_module':sensor.num_module, 'type_value_tram':sensor.type_value_tram, 'value':None, 'date': None}
        sensor_dict['data'] = {'num_module':sensor.num_module, 'type_value_tram':sensor.type_value_tram, 'value': None}
    sensor_dict['info'] = {'type' : manager.convert_type(str(sensor.type_value_tram))['name']}
    return render(request, 'LifeBox/sensor_status.html', sensor_dict)
    
@login_required(redirect_field_name='rediriger_par')
def sensor_modif(request, id_sensor):
    """ Page pour modifier un capteur"""
    if request.method == "POST":
        form = SensorForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['sensor_name']
            icon = form.cleaned_data['icon']
            Sensor.objects.filter(id = id_sensor).update(name=name, icon=icon)
            redirec = '/sensor'
            return HttpResponseRedirect(redirec)
    else:
        ico = util_icon()
        sensor = Sensor.objects.get(id = id_sensor)
        data = {'sensor_name' : sensor.name, 'icon' : sensor.icon}
        form = SensorForm(initial=data) 
        sensor_dict = get_info_base()
        sensor_dict['icon'] = ico.form_list_icon()
        sensor_dict['form'] = form
        sensor_dict['sensor'] = sensor
        return render(request, 'LifeBox/sensor_modif.html', sensor_dict)
	    
@login_required(redirect_field_name='rediriger_par')
def create_group_association(request, id_group):
    """ Page pour creer une association entre un groupe et un capteur"""
    if request.method == "POST":
        form = Group_assForm(request.POST, id = id_group)
        if form.is_valid():
            group = form.cleaned_data['group']
            sensor = form.cleaned_data['sensor']
            Group_ass.objects.create(sensor=sensor, group=group)
            redirect = '/group/' + str(id_group)
            return HttpResponseRedirect(redirect)
    else:
        form = Group_assForm(id = id_group) 
        group = Group.objects.get(id = id_group) # Selection le group avec id 
        """ Fonction utilise dans la page d'acceuil"""
        assoc_dict = get_info_base()
        assoc_dict['form'] = form
        assoc_dict['group'] = group
        return render(request, 'LifeBox/group_create_association.html', assoc_dict)

@login_required(redirect_field_name='rediriger_par')
def delete_group_association(request, id_group, id_sensor):
    """ Page pour supprimer une association entre un groupe et un capteur"""
    sensor = Sensor.objects.get(id = id_sensor)
    group = Group.objects.get(id = id_group)
    group.sensor.remove(sensor)
    redirec = '/group/' + id_group
    return HttpResponseRedirect(redirec)
    
@login_required(redirect_field_name='rediriger_par')
def create_group(request):
    """ Page pour creer un groupe """
    if request.method == "POST":
        form = CreateGroupForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            icon = form.cleaned_data['icon']
            a = Group.objects.create(name = name, icon = icon)
            redirec = '/group/' + str(a.id)
            return HttpResponseRedirect(redirec)
        else:
            msg = _("Error form is invalid, please contact LS team")
            return error(request, msg)
    else:
        ico = util_icon()
        form = CreateGroupForm()
        group_dict = get_info_base()
        group_dict['form'] = form
        group_dict['icon'] = ico.form_list_icon()
        return render(request, 'LifeBox/group_create.html', group_dict)
 
@login_required(redirect_field_name='rediriger_par')
def delete_group(request, id_group):
    """ Page pour supprimer un groupe """
    Group.objects.filter(id = id_group).delete()
    return HttpResponseRedirect('/home')
 
@login_required(redirect_field_name='rediriger_par')
def modif_group(request, id_group):
    """ Page pour modifier un groupe """
    if request.method == "POST":
        form = CreateGroupForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            icon = form.cleaned_data['icon']
            Group.objects.filter(id = id_group).update(name=name, icon=icon)
            redirec = '/group/' + str(id_group)
            return HttpResponseRedirect(redirec)
    else:
        ico = util_icon()
        group = Group.objects.get(id = id_group)
        data = {'name' : group.name,
                'icon' : group.icon
                }
        form = CreateGroupForm(initial=data)
        group_dict = get_info_base()
        group_dict['icon'] = ico.form_list_icon()
        group_dict['form'] = form
        group_dict['group'] = group
        return render(request, 'LifeBox/group_modif.html', group_dict)

@login_required(redirect_field_name='rediriger_par')   
def modif_setpoint(request, id_regul, value):
    """ Page pour modifier une consigne """
    re = Regul.objects.get(id = id_regul)
    re.setpoint = value
    re.save()
    r = regul()
    reg = r.check_reg(re)
    if isinstance(reg, type(_("test"))):
        return error(request, reg)
    redirec = '/group/' + str(re.group.id)
    return HttpResponseRedirect(redirec)

@login_required(redirect_field_name='rediriger_par')
def create_regul(request, id_group):
    """ Page pour creer une regulation """
    form = CreateRegulForm(request.POST)
    if request.method == "POST":
        if form.is_valid():
            name = form.cleaned_data['name']
            icon = form.cleaned_data['icon']
            type_regul = form.cleaned_data['type']
            Regul.objects.create(group = Group.objects.get(id = id_group), name = name, icon = icon, type_regul = type_regul)  
            redirec = '/group/' + str(id_group)
            return HttpResponseRedirect(redirec)
        else:
            msg = _("Error form is invalid, please contact LS team")
            return error(request, msg)
    else:
        ico = util_icon()
        form = CreateRegulForm()
        regul_dict = get_info_base()
        regul_dict['form'] = form
        regul_dict['id_group'] = id_group
        regul_dict['icon'] = ico.form_list_icon()
        return render(request, 'LifeBox/regul_create.html', regul_dict)
        
@login_required(redirect_field_name='rediriger_par')
def modif_regul(request, id_regul):
    """ Page pour modifier une regulation """
    if request.method == "POST":
        form = CreateRegulForm(request.POST)
        if form.is_valid():
            reg = Regul.objects.get(id = id_regul)
            reg.name = form.cleaned_data['name']
            reg.icon = form.cleaned_data['icon']
            reg.type = form.cleaned_data['type']
            reg.save()
            redirec = '/group/' + str(reg.group.id)
            return HttpResponseRedirect(redirec)
    else:
        ico = util_icon()
        regul = Regul.objects.get(id = id_regul)
        data = {'name' : regul.name,
                'icon' : regul.icon,
                'type' : regul.type_regul,
                }
        form = CreateRegulForm(initial=data)
        regul_dict = get_info_base()
        regul_dict['icon'] = ico.form_list_icon()
        regul_dict['form'] = form
        regul_dict['regul'] = regul
        return render(request, 'LifeBox/regul_modif.html', regul_dict)

@login_required(redirect_field_name='rediriger_par')
def delete_regul(request, id_regul):
    """ Page pour supprimer une regulation """
    reg = Regul.objects.get(id = id_regul)
    id_group = reg.group.id
    reg.delete()
    return HttpResponseRedirect('/group/'+str(id_group))
   
def create_regul_association(request, id_regul, type_sensor):
    """ Page pour associer une regulation et un capteur """
    if request.method == "POST":
        form = Regul_assForm(request.POST, id_regul = id_regul)
        if form.is_valid():
            regul = form.cleaned_data['regul']
            sensor = form.cleaned_data['sensor']
            Regul_ass.objects.create(regul = regul, sensor = sensor, type_sensor=type_sensor)
            redirect = '/group/' + str(Regul.objects.get(id = id_regul).group.id)
            return HttpResponseRedirect(redirect)
    else:
        form = Regul_assForm(id_regul = id_regul) 
        regul = Regul.objects.get(id = id_regul) # Selection le group avec id 
        """ Fonction utilise dans la page d'acceuil"""
        assoc_dict = get_info_base()
        assoc_dict['form'] = form
        assoc_dict['regul'] = regul
        assoc_dict['type_sensor'] = type_sensor
        return render(request, 'LifeBox/regul_create_association.html', assoc_dict)

@login_required(redirect_field_name='rediriger_par')
def delete_regul_association(request, id_regul, id_sensor):
    """ Page pour associer une regulation et un capteur """
    sensor = Sensor.objects.get(id = id_sensor)
    regul = Regul.objects.get(id = id_regul)
    regul.sensor.remove(sensor)
    redirec = '/group/' + str(regul.group.id)
    return HttpResponseRedirect(redirec)
    
@login_required(redirect_field_name='rediriger_par')
def regul_on(request, id_regul, value):
    """ Page pour activer/desactiver une regulation """
    reg = Regul.objects.get(id = id_regul)
    reg.on = value
    reg.save()
    r = regul()
    if value == True:
        check_reg = r.check_reg(reg)
        if isinstance(check_reg, type(_("test"))):
            return error(request, check_reg)
    redirec = '/group/' + str(reg.group.id)
    return HttpResponseRedirect(redirec)
    # else:
        # msg = _("Error, to switch on regulation contact LifeSmarter team")
        # return error(request, msg)
     
@login_required(redirect_field_name='rediriger_par')
def group(request, id_group):
    """ Page pour afficher un groupe """
    if request.method == "POST":
        id_sensor = request.POST['id_sensor']
        val = request.POST['value']
        try :
            s = Sensor.objects.get(id = id_sensor)
        except:
            msg = _("Error, to update sensor data please contact LifeSmarter team")
            return error(request, msg)
            
        """ Valeur d'un capteur venant du serveur"""
        ud = util_data()
        ud.send_data(s, val)
        url = '/group/' + id_group
        return HttpResponseRedirect(url)
    else:
        group = Group.objects.get(id = id_group) # Selection le group avec id
        sensor = group.sensor.all()
        #Partie groupe 
        group_dict = get_info_base()
        group_dict['group'] = group

        mtype = manage_type()
        res = []
        for s in sensor: 
            try : 
                val = s.data_set.all().last().value
            except : 
                val = None
            tmp = mtype.convert_type(s.type_value_tram, id_group = id_group, id_sensor = s.id, value = val)
            res.append({'sensor':s ,'switcher':tmp})
        group_dict['sensor_group'] = res

        #Partie regul 
        r = regul()
        group_dict['regul'] = r.get_group_regul(group)
        return render(request, 'LifeBox/group.html', group_dict)
     
#Partie DATA    
def insert_data(request, num_module, type_value_tram, value):
    """ Page pour inserer une donnée (changement dans la prochaine version) """
    try :
        s = Sensor.objects.get(num_module = num_module, type_value_tram = type_value_tram)
    except:
        return HttpResponse("False")
        
    """ Valeur d'un capteur venant du serveur"""
    data = Data.objects.create(sensor = s, num_module = num_module, type_value_tram = type_value_tram, value = value)
    r = regul()
    msg = "True"
    for i in Regul_ass.objects.filter(sensor = s):
        if not r.check_reg(i.regul):
            msg = "Data ok, Error with regulation"
    return HttpResponse(msg)
        
def update_status_sensor(request, num_module, type_tram, online = False):
    """ Page pour mettre un capteur en ligne (changement dans la prochaine version ?) """
    if int(type_tram) == 1:
        try:
            if online in ["True", "False", "0", "1"]:
                sensor = Sensor.objects.filter(num_module = num_module)
                for i in sensor:
                    i.online = online
                    i.save()
                return HttpResponse("True")
            else:
                return HttpResponse("False")
        except Sensor.DoesNotExist:
            return HttpResponse("False")
    return HttpResponse("Error : " + type_tram + " Not handle")

def all_sensor_offline(request):
    """ Page pour mettre tous les capteurs hors_ligne """
    Sensor.objects.all().update(online = False)
    return HttpResponse("True")

@login_required(redirect_field_name='rediriger_par')
def weather(request, woeid = '619640'):
    """ Page afficher la météo """
    weather_dict = get_info_base(all_data = True)
    return render(request, 'LifeBox/weather.html', weather_dict)

@login_required(redirect_field_name='rediriger_par')
def modif_dict(request, id_dict):
    """ Page la gestion du dictlist """
    if request.method == "POST":
        form = RegulForTypeForm(request.POST)
        if form.is_valid():
            type_value_tram = form.cleaned_data['type_value_tram']
            name = form.cleaned_data['name']
            value = form.cleaned_data['value']
            signe = form.cleaned_data['signe']
            RegulForType.objects.filter(id = id_dict).update(type_value_tram = type_value_tram, name=name, value = value, signe = signe)
            redirec = '/list_dict' 
            return HttpResponseRedirect(redirec)
    else:
        regultype = RegulForType.objects.get(id = id_dict)
        
        data = {'type_value_tram' : regultype.type_value_tram,
                'name' : regultype.name,
                'value' : regultype.value
                }
        form = RegulForTypeForm(initial=data) 
        regultype_dict = get_info_base()
        regultype_dict['form'] = form
        regultype_dict['regultype'] = regultype
        return render(request, 'LifeBox/modif_dict.html', regultype_dict)

@login_required(redirect_field_name='rediriger_par')
def delete_dict(request, id_dict):
    """ Page pour supprimer une dict liste """
    url = '/list_dict'
    t_dict = RegulForType.objects.get(id = id_dict).delete()
    return HttpResponseRedirect(url)

@login_required(redirect_field_name='rediriger_par')
def list_dict(request):
    """ Page pour afficher une dict liste """
    if request.method == "POST":        
        form = RegulForTypeForm(request.POST)
        if form.is_valid():
            type_value_tram = form.cleaned_data['type_value_tram']
            name = form.cleaned_data['name']
            value = form.cleaned_data['value']
            signe = form.cleaned_data['signe']
            RegulForType.objects.create(type_value_tram = type_value_tram, name = name, value = value, signe = signe)
            return redirect('/list_dict')
        
    else:
        manager = manage_type()
        dict_list = get_info_base()
        temp_dict = {}
        dict_list['form'] = RegulForTypeForm()
        for x in manager.switcher():
            t = RegulForType.objects.filter(type_value_tram = x)
            if t:
                temp_dict[manager.convert_type(x)['name']] = t
        dict_list['dict'] = temp_dict

        return render(request, 'LifeBox/list_dict.html', dict_list)

@login_required(redirect_field_name='rediriger_par')   
def delete_notif(request, id_notif):
    """ Page pour supprimer une notification """
    res = Notification.objects.filter(id = id_notif)
    if res.count() == 1:
        res.delete()
        return HttpResponse("True")
    else:
        msg = _("Error, to switch on regulation contact LifeSmarter team")
        return error(request, msg)
