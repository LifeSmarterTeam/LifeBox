from django.apps import AppConfig

class SmarterlifeConfig(AppConfig):
    name = 'LifeBox'
