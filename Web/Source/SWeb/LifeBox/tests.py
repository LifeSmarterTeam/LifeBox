from django.test import TestCase
from django.utils import timezone
from django.test.client import Client
from django.contrib.auth.models import User
from django.urls import reverse
from LifeBox.models import *

class SensorTest(TestCase):
    """ Test pour les capteurs""" 
    def test_create_sensor(self):
        Sensor.objects.create(num_module = 999, type_value_tram = 26, name = "test")
    
    def test_online_sensor(self):
        a = Sensor.objects.create(num_module = 999, type_value_tram = 26, name = "test")
        self.assertFalse(a.online)
     
    def test_date_sensor(self):
        a = Sensor.objects.create(num_module = 999, type_value_tram = 26, name = "test")
        self.assertTrue(type(a.date) == type(timezone.now()))
        
class GroupTest(TestCase):
    """ Test pour les groupes""" 
    def test_create_groupe(self):
        Group.objects.create(name = "test")

    def test_date_groupe(self): 
        a = Group.objects.create(name = "test")
        self.assertTrue(type(a.date) == type(timezone.now()))

class Group_assTest(TestCase):
    """ Test pour les associations groupes""" 
    def test_create_association(self):
        Group_ass.objects.create(id_group = 0, num_module = 999, type_value_tram = 26, type_regul_sensor = 999)

    def test_date_association(self):
        a = Group_ass.objects.create(id_group = 0, num_module = 999, type_value_tram = 26, type_regul_sensor = 999)
        self.assertTrue(type(a.date) == type(timezone.now()))

class EventTest(TestCase):
    """ Test pour les evenements""" 
    pass

class DataTest(TestCase):
    """ Test pour les données""" 
    def test_create_data(self):
        Data.objects.create(num_module = 999, type_value_tram = 26, value = 99.9)

    def test_date_data(self):
        a = Data.objects.create(num_module = 999, type_value_tram = 26, value = 99.9)
        self.assertTrue(type(a.date) == type(timezone.now()))

class ParameterTest(TestCase):
    """ Test pour les parametres""" 
    def test_parameter(self):
        a = Parameter.objects.all()
        for param in a: 
            assertIn(param.name, ['City', 'Country', 'Ip', 'Password', 'SSID', 'Woeid'])

class LoginTestCase(TestCase):
    """ Test pour l'authentification """ 
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user('test-user', 'password')

    def testLogin(self):
        self.client.login(username='test-user', password='password')
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)
