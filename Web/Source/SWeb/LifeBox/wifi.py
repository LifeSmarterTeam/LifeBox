from django.utils.translation import gettext_lazy as _
from LifeBox.models import *
import time, subprocess, socket

class init:
    """ Classe pour la gestion de l'initialisation des capteurs """
    def __init__(self): 
        self.name_wpa_wlan0 = "wpa_supplicant-wlan0.conf"
        self.file_wlan0 = subprocess.check_output(['cat', '/etc/wpa_supplicant/' + self.name_wpa_wlan0]).decode("utf-8")
        self.name_wpa_ap0 = "wpa_supplicant-ap0.conf"
        self.file_ap0 = subprocess.check_output(['cat', '/etc/wpa_supplicant/' + self.name_wpa_ap0]).decode("utf-8") 
        pass
    
    def mode_acces_point(self):
        subprocess.check_output(['sudo', 'systemctl','start','wpa_supplicant@ap0.service']).decode("utf-8")
        return True
    
    def mode_wifi_client(self):
        subprocess.check_output(['sudo', 'systemctl','start','wpa_supplicant@wlan0.service']).decode("utf-8")
        return True
    
    def get_num_module(self):
        """ get new num module """
        list = set([i['num_module'] for i in Sensor.objects.filter().values('num_module')])
        for j in range(0,999):
            if j not in list:
                return j
        print("Aucun numero de module disponible")
        return None
           
    def esp_wifi_wpa(self):
        """ insert in first ssid of init """
        path = "/etc/wpa_supplicant/" + self.name_wpa_wlan0
        try:
            f = open(path, "r")
            line = f.readlines()
            f.close()
        except:
            print("Can't open " + path)
            return False

        i = 0
        for i in iter(range(len(line))):
            if line[i].find("network") != -1:
                break
            i = i + 1
            
        line.insert(i,'network={\n')
        line.insert(i+1,'\tssid="{}"\n'.format(self.ssid))
        line.insert(i+2,'\tpsk="{}"\n'.format(self.key))
        line.insert(i+3,'\tscan_ssid=1\n')
        line.insert(i+3,'\tpriority=1\n')
        line.insert(i+4,'}\n')
        line.insert(i+5,'\n')
        del line[i+6:]
        
        try:
            f = open(path, "w")
            line = "".join(line)
            f.write(line)
            f.close()
        except:
            print("Can't open " + path)
            return False

        return True
        
    def wpa_file_back(self):
        path = "/etc/wpa_supplicant/" + self.name_wpa_wlan0
        try:
            f = open(path, "w")
            f.write(self.file_wlan0)
            f.close()
        except:
            print("Can't open " + path)
    
    def wpa_ap0_save(self):
        path = "/etc/wpa_supplicant/" + self.name_wpa_ap0
        try:
            f = open(path, "w")
            f.write(self.file_ap0)
            f.close()
        except:
            print("Can't open " + path)
           
    def get_ssid(self):
        start = self.file_ap0.find("ssid") + 4 + self.file_ap0[self.file_ap0.find("ssid") + 4 :].find('"') + 1
        end = start + self.file_ap0[start:].find('"\n')
        return self.file_ap0[start:end].lstrip().rstrip()
        
    def maj_ssid_psk_ap0(self, ssid, psk):
        last_ssid =  self.get_ssid()
        last_psk = self.get_psk()
        if ((last_ssid == ssid) and (last_psk == psk)):
          return True
        else:
          new_file = self.file_ap0[:self.file_ap0.find(last_ssid)] + ssid + self.file_ap0[self.file_ap0.find(last_ssid)+len(last_ssid):self.file_ap0.find(last_psk)] + psk + self.file_ap0[self.file_ap0.find(last_psk)+len(last_psk):]
          self.file_ap0 = new_file
          self.wpa_ap0_save()
          self.mode_wifi_client()
          self.mode_acces_point()
          return True
          
    def maj_ip(self, ip):
        path = "/etc/systemd/network/12-ap0.network"
        file_ip = subprocess.check_output(['cat',path]).decode("utf-8")
        start = file_ip.find("Address=") + 8
        end = start + file_ip[start:].find('\n')
        last_ip = file_ip[start:end]
        if last_ip == ip:
          return True
        else:
          new_file = file_ip[:start] + ip + file_ip[end:]
          f = open(path, "w")
          f.write(new_file)
          f.close()
          subprocess.check_output(['sudo','reboot']).decode("utf-8")
          return True
    
    def get_psk(self):
        start = self.file_ap0.find("psk") + 3 + self.file_ap0[self.file_ap0.find("psk") + 3 :].find('"') + 1
        end = start + self.file_ap0[start:].find('"\n')
        return self.file_ap0[start:end].lstrip().rstrip()
    
    def get_interface_ipaddress(self):
        """ Find ip adresse of ap0 """
        try:
            ipconfig = subprocess.check_output(['ifconfig', 'ap0']).decode("utf-8")
            start = ipconfig.find('inet') + 4
            end = ipconfig.find('netmask')  
        except: 
            print("Error while getting ifconfig")
            return ""
        return ipconfig[start:end].lstrip().rstrip()
        
    def wifi_esp_ok(self):
        """ True if pi connect to wifi """
        try:
            if subprocess.check_output(['sudo', 'systemctl','status','wpa_supplicant@wlan0.service']).decode("utf-8").find("CTRL-EVENT-CONNECTED") > 0:
                return True
            else:
                return False
        except:
            return False
            
    def wifi_ip_ok(self):
        """ True if pi ip ok """
        try:
            if subprocess.check_output(['sudo', 'ifconfig','wlan0']).decode("utf-8").find("inet 192.168.2.") > 0:
                return True
            else:
                return False
        except:
            return False
    
    def manage_wifi_esp(self, ssid, key):
        """ Connect to wifi with profile or ssid + pass """
        self.ssid = ssid
        self.key = key
             
        if self.esp_wifi_wpa() == False: return False
        
        #Change de mode pour se connecter au module
        self.mode_wifi_client()
        
        for a in range(1,25):
            time.sleep(5)
            if self.wifi_esp_ok() and self.wifi_ip_ok():
                break
            if a == 20:
                return False
                
        time.sleep(15)  
        return True


    def send_tram_initialisation(self, num_module, hote_pi):
        """ send tram to esp and get all values need  """
        self.hote_init = "192.168.2.5"
        self.port_init = 2018
        self.num_module = num_module
        self.hote_pi = hote_pi
        self.port_pi = 2019
        self.ssid = self.get_ssid()
        self.password = self.get_psk()
        
        self.wpa_file_back() #remet ancien fichier wpa
        msg = "1" + "000" + "2" + "000:" + str(self.ssid) + "|001:"+ str(self.password) + "|002:" + str(self.num_module) +"|003:"+ str(self.hote_pi) +"|004:"+ str(self.port_pi) + "3" + "005" + "4"
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(30)
        try: 
            sock.connect((self.hote_init, self.port_init))
            res = sock.recv(1024).decode("utf-8")
            sock.send(str.encode(msg))
            sock.close()
            return res
        except:
            print("Timeout send tram aucun capteur trouvé")
            return None

    def parse(self, tram):
        if ((tram == None) or not(tram[0:5] == "10002" and tram[-1] == "4" and tram[-5] == "3")):
            print("erreur tram lecture")
            return None
        res = []
        msg = tram[5:-5]
        for i in msg.split('|'):
            if i.split(':')[0] == "005": #TYPE_PARAMETRE
                res.append(i.split(':')[1])
        return res   