from django.db import models
from django.utils import timezone
from django.urls import reverse

class Sensor(models.Model):
    """ Tables des capteures"""
    num_module = models.IntegerField(verbose_name = "Numero du module")
    type_value_tram = models.IntegerField(verbose_name = "Type valeur tram")
    name = models.CharField(max_length=100, verbose_name = "Nom du capteur")
    icon = models.CharField(max_length=100, null=True, blank=True, verbose_name = "icon")
    date = models.DateTimeField(default=timezone.now, verbose_name="date created")
    online = models.BooleanField(default=False, verbose_name = "Online / offline" )

    class Meta:
        verbose_name = "sensor"
        ordering = ['num_module']

    def __str__(self):
        return self.name
        
class Group(models.Model):
    """ Tables des groupes"""
    name = models.CharField(max_length=100, verbose_name = "Nom du groupe")
    icon = models.CharField(max_length=100, null=True, blank=True, verbose_name = "icon")
    date = models.DateTimeField(default=timezone.now, verbose_name="date created")
    sensor = models.ManyToManyField(Sensor, through='Group_ass', related_name="groups")
    
    class Meta:
        verbose_name = "group"
        ordering = ['name']

    def __str__(self):
        return self.name
        
class Notification(models.Model):
    """ Tables des notifications"""
    ntype = models.IntegerField(default=1, verbose_name = "Type de notif")
    level = models.IntegerField(verbose_name = "Niveau de notif")
    content = models.CharField(max_length=100, verbose_name = "Contenu notif")
    date = models.DateTimeField(default=timezone.now, verbose_name = "Date creation")
    def _get_delta(self):
        "Returns delta time."
        return (self.end_date - timezone.now()).days

    delta = property(_get_delta)
    end_date = models.DateTimeField(default=timezone.now, verbose_name = "Endtime")

    class Meta:
        verbose_name = "Notification"
        ordering = ['date']
     
class Regul(models.Model):
    """ Tables des regulations"""
    name = models.CharField(max_length=100, verbose_name = "Regulation name")
    icon = models.CharField(max_length=100, null=True, blank=True, verbose_name = "icon")
    type_regul = models.IntegerField(blank=True, null=True, verbose_name = "Regulation type (Temperature, ...)")
    setpoint = models.CharField(blank=True, null=True, max_length=100, verbose_name = "Regulation setpoint string")
    measure = models.CharField(blank=True, null=True, max_length=100, verbose_name = "Regulation measure")
    action = models.CharField(blank=True, null=True, max_length=100, verbose_name = "Regulation action")
    on = models.BooleanField(default=False, verbose_name = "On/Off" )
    sensor = models.ManyToManyField(Sensor, through='Regul_ass', related_name="reguls")
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    date = models.DateTimeField(default=timezone.now, verbose_name="date created")
    
    class Meta:
        verbose_name = "regul"
        ordering = ['name']

    def __str__(self):
        return self.name
        
class Group_ass(models.Model):
    """ Tables des association groupes"""
    group = models.ForeignKey(Group, on_delete=models.CASCADE, related_name="reguls")
    sensor = models.ForeignKey(Sensor, on_delete=models.CASCADE)
    date = models.DateTimeField(default=timezone.now, verbose_name="date created")
    
    class Meta:
        verbose_name = "Group association"
        ordering = ['date']
                
class Regul_ass(models.Model):
    """ Tables des association regulation"""
    regul = models.ForeignKey(Regul, on_delete=models.CASCADE)
    sensor = models.ForeignKey(Sensor, on_delete=models.CASCADE)
    type_sensor = models.IntegerField(verbose_name = "Le type du capteur si il est utilisé en 1 = Mesure, 2 = Action")
    # expert_mode = models.BooleanField(default=False, verbose_name = "Mode expert" )
    date = models.DateTimeField(default=timezone.now, verbose_name="date created")

    class Meta:
        verbose_name = "Regulation association"
        ordering = ['date']
                
class Data(models.Model):
    """ Tables des données des capteurs"""
    sensor = models.ForeignKey(Sensor, on_delete=models.CASCADE)
    num_module = models.IntegerField(verbose_name = "Numero du module, un module est un microcontroleur")
    type_value_tram = models.IntegerField(verbose_name = "Type valeur tram est une des mesures fait par un module")
    value = models.CharField(blank=True, null=True, max_length=100, verbose_name = "value")
    date = models.DateTimeField(default=timezone.now, verbose_name="date created")

    class Meta:
            verbose_name = "Data"
            ordering = ['date']

class Parameter(models.Model):
    """ Tables des paramètres"""
    name = models.CharField(max_length=100, verbose_name = "Nom du parametre")
    value = models.CharField(max_length=100, verbose_name = "Valeur du parametre")
    date = models.DateTimeField(default=timezone.now, verbose_name="date created")
    
    class Meta:
        verbose_name = "Parameter"
        ordering = ['name']

class Event(models.Model):
    """ Tables des evenements"""
    title = models.CharField(max_length=200)
    icon = models.CharField(max_length=100, null=True, blank=True, verbose_name = "icon")
    description = models.TextField()
    start_time = models.DateTimeField(verbose_name="Date de debut de l'evenement")
    end_time = models.DateTimeField(verbose_name="Date de fin de l'evenement")

    @property
    def get_html_url(self):
        url = reverse('LifeBox:event_edit', args=(self.id,))
        return f'<a href="{url}"> {self.title} </a>'

class RegulForType(models.Model):
    """ Tables des type de regul"""
    type_value_tram = models.IntegerField(verbose_name = "Type valeur tram")
    name = models.CharField(max_length=100, verbose_name = "Nom du parametre")
    value = models.IntegerField(verbose_name = "Value for that name")
    signe = models.CharField(max_length=100, verbose_name = "signe")
    date = models.DateTimeField(default=timezone.now, verbose_name="date created")

    class Meta:
            verbose_name = "RegulForType"
            ordering = ['type_value_tram']
