from django.utils.translation import gettext_lazy as _
from django.utils import translation
from datetime import datetime, timedelta, date
from django.utils.safestring import mark_safe
from LifeBox.models import *
import calendar
import os, requests, time, re

#Messsage queue (Change in next version) 
try:
    import posix_ipc
    mq = posix_ipc.MessageQueue('/SWebToServDom')
except:
    print(" /!\ start serveur before serveur web need to create message queue /!\ ")
    print(" ")

class notif_util:
    """ classe util pour les notifications """
    def __init__(self):
        pass

    def getNotif(self, limit=10):
        Notification.objects.filter(end_date__lte=timezone.now(), ntype=2).delete()
        return Notification.objects.all()[:limit]

    def clearNotif(self, notif):
        return Notification.objects.get(id=notif).delete() 

    def clearAll(self):
        return Notification.objects.all().delete()

    def countNotif(self):
        return Notification.objects.count()

    def importantCountNotif(self):
        return Notification.objects.filter(level__gt=4).count()
 
def get_info_base(all_data = False):
    """ Recuperation info de base"""
    notif = notif_util()
    up = util_param()
    res = {'all_group': Group.objects.filter(), 'weather': up.weather_data(all_data) , 'date' : up.actual_date(), 'all_notif': notif.getNotif(), 'count': notif.countNotif(), 'important': notif.importantCountNotif()}
    return res
        
class util_param:
    """ classe util pour les informations de base (météo, date, ...) """
    time_last = datetime(1970,1,1)
    data_weather = ""
    try:
        woeid = Parameter.objects.filter(name = 'Woeid')[0].value
    except:
        woeid = "000000"
    
    def __init__(self):
        pass

    def update_woeid(self, city, country):
        """Update woeid en base """
        url = 'https://www.yahoo.com/news/_tdnews/api/resource/WeatherSearch;text=' + city
        woeid_city = requests.get(url).json() #request the API data and convert the JSON to Python data types
        i = 0 
        #Precision nécessaire
        if len(woeid_city) > 0:  
            for i in iter(range(len(woeid_city))):
                if woeid_city[i]['country'].lower() == country.lower():
                    Parameter.objects.filter(name = 'Woeid').update(value=woeid_city[i]['woeid'])
                    return woeid_city[i]['city']
            return "Inconnu"
        else:
            return "Error"
            
    def actual_date(self):
        dat = datetime.now().date()
        return {'day_date' : dat}
        
    def weather_data(self, all_data):
        """ Weather with woeid parameter. Weather only reaload once time until 10 minutes """
        if (datetime.now() - util_param.time_last).total_seconds() < 10*60 and all_data == False and util_param.woeid == Parameter.objects.filter(name = 'Woeid')[0].value:
            return util_param.data_weather
        util_param.time_last = datetime.now()
        util_param.woeid = Parameter.objects.filter(name = 'Woeid')[0].value
        url = 'https://www.yahoo.com/news/_td/api/resource/WeatherService;woeids=[{}]'.format(util_param.woeid)
        try:
            city_weather = requests.get(url, timeout=1).json() #request the API data and convert the JSON to Python data types
        except:
            return {}
        if all_data == False : 
            weather = {
                    'city' : city_weather['weathers'][0]['location']['displayName'],
                    'temperature' : round((float(city_weather['weathers'][0]['observation']['temperature']['now']) - 32) * 5/9),
                    'icon_url' :  "https://s.yimg.com/zz/combo?a/i/us/nws/weather/gr/{}d.png".format(city_weather['weathers'][0]['observation']['conditionCode'])
            }
        else: 
            date = []
            min_temp = []
            max_temp = []
            
            for i in city_weather['weathers'][0]['forecasts']['daily']:
                date.append(datetime.strptime(i['observationTime']['timestamp'][:-1], '%Y-%m-%dT%H:%M:%S.%f').strftime("%a %d %b"))
                min_temp.append(round((float(i['temperature']['low']) - 32) * 5/9))
                max_temp.append(round((float(i['temperature']['high']) - 32) * 5/9))
            
            date_day = []
            temp = []
            temp_ress = []
            humidity = []
            rain = []
            wind_speed = []
            
            for j in city_weather['weathers'][0]['forecasts']['hourly']:
                date_day.append(datetime.strptime(j['observationTime']['timestamp'][:-1], '%Y-%m-%dT%H:%M:%S.%f').strftime("%d %b - %H:%M"))
                temp.append(round((float(j['temperature']['now']) - 32) * 5/9))
                temp_ress.append(round((float(j['temperature']['feelsLike']) - 32) * 5/9))
                humidity.append(j['humidity'])
                rain.append(j['precipitationProbability'])
                wind_speed.append(j['windSpeed'])
                
            weather = {
                'city' : city_weather['weathers'][0]['location']['displayName'],
                'temperature' : round((float(city_weather['weathers'][0]['observation']['temperature']['now']) - 32) * 5/9),
                'icon_url' :  "https://s.yimg.com/zz/combo?a/i/us/nws/weather/gr/{}d.png".format(city_weather['weathers'][0]['observation']['conditionCode']),
                'week' : zip(date, city_weather['weathers'][0]['forecasts']['daily'], min_temp, max_temp),
                'day' : zip(date_day, city_weather['weathers'][0]['forecasts']['hourly'], temp, temp_ress),
                'data' : city_weather['weathers'][0],
                'pressure' :  round((float(city_weather['weathers'][0]['observation']['barometricPressure'])*33.929)),
                'temp_hours' : zip(date_day, temp, temp_ress),
                'rain_hours' : zip(date_day, humidity, rain, wind_speed),
            }
            
        util_param.data_weather = weather
        return weather
        
class util_data:
    """ Class des utils pour les données """
    def send_data(self, s, value):
        """ Fonction qui envoi les données a un capteur
            1 ok message transmit 
            0 valeur update pas transmit """
        data = Data.objects.create(sensor = s, num_module = s.num_module, type_value_tram = s.type_value_tram, value = value)
        file_name = str(int(time.time())) + "_0_" + "SWEB" 
        path_file = "/tmp/SL_Exchange/" + file_name
        try:
            f= open(path_file,"w+")
            f.write("${}$\n".format(data.num_module)) #Num module 
            f.write("$2$\n") #Type Tram
            f.write("${}$\n".format(1)) #Total data in tram 
            f.write("${}:{}$\n".format(s.type_value_tram, data.value)) #Decompose code par |
            f.close() 
            mq.send(file_name)
        except: 
            print ('Be carefull, is' + path_file + 'exist ?') 
            return 0
        return 1
        
    def get_last_data(self, id_group, sensor):
        data = Data.objects.filter(num_module = sensor.num_module, type_value_tram = sensor.type_value_tram)
        try : 
            d = data.latest('date')
            val = d.value
        except :
            val = None
        manager = manage_type()
        res = {'num_module': sensor.num_module, 'type_value_tram': sensor.type_value_tram, 'value': val, "as_html":manager.convert_type( sensor.type_value_tram, id_group, sensor.id, val)['html']}
        return res
        
class Calendar(calendar.HTMLCalendar):
    """ Class utils calendrier """
    def __init__(self, year=None, month=None):
        self.year = year
        self.month = month
        self.day = timezone.now().day
        super(Calendar, self).__init__()

    # formats a day as a td
    # filter events by day
    def formatday(self, day):
        if day == 0:
            return '<td></td>'

        start = timezone.datetime(day=day, month=self.month, year = self.year)
        end = timezone.datetime(day=day, month=self.month, year = self.year)
        events_per_day = Event.objects.filter(start_time__lte=start, end_time__gte=end)

        d = '<ul>'
        for event in events_per_day:
            d += f'<li> {event.get_html_url[:-4]} <i class="{event.icon}"></i></li>'
        d += '</ul>'

        if day != 0:
            if ((day == self.day) and (datetime.now().month == self.month) and (datetime.now().year == self.year)):
                day_format = f"<td bgcolor= #999999 ><span class='date'><a href='event/new/{day}/{self.month}/{self.year}'>{day}</a></span>{d}</td>"
            else:
                day_format = f"<td><span class='date'><a href='event/new/{day}/{self.month}/{self.year}'>{day}</a></span>{d}</td>"
            return day_format
        return '<td></td>'

    # formats a week as a tr
    def formatweek(self, theweek):
        week = ''
        for d, weekday in theweek:
            week += self.formatday(d)
        return f'<tr> {week} </tr>'


    def formatmonthname(self, year, month, withyear):
        next_month = month + 1 if month < 12 else 1

        prev_month = month - 1 if month > 1 else 12
        next_year = year  if month < 12 else year + 1
        prev_year = year  if month > 1 else year - 1 

        header = '<tr>'
        header += f'<th><a class="btn left" href="?month={prev_year}-{prev_month}"><i class="mdi mdi-chevron-left space"></i>' + _(calendar.month_name[prev_month]).capitalize() + '</a></th>'
        header += f'<th class="month" colspan="5">'
        header += ' ' + _(calendar.month_name[month]).capitalize() + ' '
        if withyear:
            header += ' ' + str(year) + ' '

        header += f'<a href="event/new"><i class="mdi mdi-plus"></i><span class="bubble">Add</span></a></th>'
        header += f'<th><a class="btn right" href="?month={next_year}-{next_month}">' + _(calendar.month_name[next_month]).capitalize() + '<i class="mdi mdi-chevron-right space"></i></a></th>'
        header += '</tr>'
        return f'{header}'

    def formatweekheader(self):
        header = f'<tr>'
        for i in self.iterweekdays():
            header += f'<th>' + _(calendar.day_name[i])[:3].capitalize() + '.</th>'
        header += f'</tr>'
        return f'{header}'

    # formats a month as a table
    # filter events by year and month
    def formatmonth(self, withyear=True):

        cal = f'<table border="0" cellpadding="0" cellspacing="0" class="calendar">\n'
        cal += f'{self.formatmonthname(self.year, self.month, withyear=withyear)}\n'
        cal += f'{self.formatweekheader()}\n'
        for week in self.monthdays2calendar(self.year, self.month):
            cal += f'{self.formatweek(week)}\n'
        cal += f'</table>'
        return cal

class util_icon: 
    """ Class utils icones """
    def __init__(self):
        self.path_icon = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),"static","other","icon_list.txt")
    
    def form_list_icon(self):
        list_icon = []
        with open(self.path_icon) as FileObj:
            for lines in FileObj:
                #list_icon.append(lines[:lines.find('{')].lstrip().rstrip())
                list_icon = [ 'mdi ' + sub for sub in re.split(';', lines)]
        return [mark_safe(icon.lower()) for icon in list_icon]
                   
class manage_type:
    """ Classe pour la gestion des type_valeur_tram """
    def __init__(self): #Constructeur (variable dans cette classe global)
        pass
        
    def convert_type(self, type, id_group = None, id_sensor = None, value = None):
        """Type_valeur_tram a ajouter ici et le nom correspondant"""
        return self.switcher(id_group, id_sensor, value).get(str(type), {"name":"Type inconnu", "icon":"fas,question-circle", "html":"No html"})
    
    def switcher(self, id_group = None, id_sensor = None, value = None):
        """Type_valeur_tram a ajouter ici et le nom correspondant"""
        check = ""
        if value == '0.0':
            s_value = '1.0'
        elif value == '1.0': 
            s_value = '0.0'
            check = "checked"
        else:
            s_value = 1.0 
            
        switcher = { 
           '20': {"name":_("Timestamp"), "icon":"mdi mdi-clock",
                  "html":r"""<span class="timestamp">""" + str(value) + r"""</span>"""
                  },
                  
           '21': {"name":_("Temperature"), "icon":"mdi mdi-thermometer",
                  "html":r"""<span class="temp">""" + str(value) + r"""</span>"""
                  },
                  
           '22': {"name":_("Humidity"), "icon":"mdi mdi-water",
                  "html":r"""<span class="humidity">""" + str(value) + r"""</span>"""
                  },
                  
           '23': {"name":_("Angles"), "icon":"mdi mdi-angle-obtuse",
                  "html":r"""<span class="angle">""" + str(value) + r"""</span>"""
                  },
                  
           '24': {"name":_("Color"), "icon":"mdi mdi-palette",
                  "html": r"""<input type="color" name="value" id="value" value="{}" onchange="this.parentNode.submit()">
                              <input type="hidden" name="id_sensor" id="id_sensor" value="{}">""".format(value, id_sensor)
                  },
                  
           '25': {"name":_("Luminosity"), "icon":"mdi mdi-white-balance-sunny",
                  "html":r"""<span class="luminosity">""" + str(value) + r"""</span>"""
                  },

           '27': {"name":_("Pressure"), "icon":"mdi mdi-weight",
                  "html":r"""<span class="pressure">""" + str(value) + r"""</span>"""
                  },
                  
           '28': {"name":_("altitude"), "icon":"mdi mdi-image-filter-hdr",
                  "html":r"""<span class="altitude">""" + str(value) + r"""</span>"""
                  },
                  
           '26': {"name":"On/Off", "icon":"mdi mdi-power", 
           "html": r"""<input type="hidden" name="id_sensor" id="id_sensor" value="{2}">
                       <input type="hidden" name="value" id="value" value="{0}">
                       <div class="switch-wrapper"><div class="switch-slider"><label class="label">
                       <input type="checkbox" name="switch" id="box" value="{0}" onclick="submit();" {1}>
                       <div class="slider round"></div></label></div></div>""".format(s_value, check, id_sensor)},         
           
           '29': {"name":_("value"), "icon":"mdi mdi-crop-square",
           "html":r"""<input type="text" name="value" id="value" value="{}" onchange="this.parentNode.submit()">
                      <input type="hidden" name="id_sensor" id="id_sensor" value="{}">""".format(value, id_sensor)},
        } 
        return switcher
        
    def get_list_form(self):
        list_switcher = self.switcher()
        return [(i, _(list_switcher[i]['name'])) for i in list_switcher.keys()]


def get_list_dict_form(type):
    return [(i.value, _(i.name)) for i in RegulForType.objects.filter(type_value_tram=type)]

def get_date(req_month):
    if req_month:
        year, month = (int(x) for x in req_month.split('-'))
        return date(year, month, day=1)
    return datetime.today()
