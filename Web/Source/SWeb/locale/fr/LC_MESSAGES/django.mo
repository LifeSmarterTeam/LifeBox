��    �      T  �   �      `     a     q     �  	   �     �  
   �     �     �     �     �     �     �     �  
   �     �     	       	         *     7     >     Q     ^  #   p  	   �     �     �     �     �     �     �  
   �     �     �       	     
        "     2     >     T     c     k     q     �     �  -   �     �     �     �     �     �     �       
   
       *     
   E     P     Y     e  	   j     t     �     �     �     �     �     �     �  
   �     �     �     �  	     
             &     5     O     \     d     k       
   �  
   �     �     �     �  
   �  
   �     �     �     �     �                     *     6     B     J     O     X     _     k  
   {  	   �     �     �     �     �     �     �     �     �     �               +     <     J     a     t     }     �     �     �     �     �     �     �     �     �  A  �     0     G     ]     f     u     �     �     �     �     �     �  
   �     �     �          /     =  	   E     O     [     b     t     �  (   �     �     �  	   �     �     �            	        !     4     D     M     V     f     |     �     �     �     �     �     �     �  .   �     %  	   6     @     S  	   `     j     s     z     �  $   �     �  	   �  	   �     �  
   �     �                "     8     N     [  	   g     q     }     �     �     �     �     �     �  .   �     $  
   1     <     E     Z     k     w     �     �     �     �  
   �  
   �  
   �     �     �     �     �                $     3     ;  
   A     L     R     ^  
   n     y  
   ~  	   �     �     �     �     �     �     �  (   �       "        :     M  #   _     �     �  %   �     �     �     �     �  
                  !     (     g      Y   n                 A       h   [       �   I                 )   x   @   ;              �   .   ,   4           |   f   J   a       '   m             y       7               9   V                 8   P   1      e       l   Z         "   L   s   W   ^   d      j   X             o   ~   E          c   _       �   %   z       F      #   i   �   /          >       3   \       ]                  q   }   ?      
       <   =   0      *      G   :   w      !   p       H   �       k   �   �       r   +      b       K   U   Q       -                 C   $      6           B   S       t   R   2   D   O           u   `      �       {           T   (   v   M   5   &   	       N        10-day forecast Acces point IP Action Add group Add regulation Add sensor Administration Agenda All sensor are Ok Angles Back Calendar Change Clean data Clean database Clean everything Color Connexion Country name Create Create association Create group Create regulation Created and Powered by LifeBox Team Dark mode Day Delete Delete regulation Deleting event Deleting group Dict Different  Disconnect sensor Display name Edit Edit dict Edit group Edit regulation Edit sensor Emergengy information Energy savings English Equal Equal or lower Equal or superior Error Error form is invalid, please contact LS team Error message Event Everything is ok ! Feel °C French German Group Group name HOME Home Automation expertise - Made in France Home state Humidity Humidity %% Icon Important Known sensor Known translate Light On/Off Light color List of dictionnary translation Log in Logout Lower Luminosity Measure Module New Dict New group New sensor No captor saved No event today No important notification Notification Offline Online Our social networks Page not found Parameters Parametres Password Pressure Rain %% Regul name Regulation Regulation name Regulation type Save Sensor Sensor  Sensor Status Sensor modification Sensor name Sensor type Sensors Sign Superior Target Temperature Temperature °C The sensor Timestamp Type of sensor :  UV index Update Username Value Values Weather Weather & Forecast Weather - City name or zip code Weather of day Welcome on your Homepage Wifi name (ssid) Wifi password Wrong user or password Your are connecte  altitude error, regulation is off error, setpoint not define is last value is  received save in saved type value wind m/s Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Prévision à 10 jours Ip pour l'acces point Resultat Ajouter groupe Ajouter Regularisation Ajouter capteur Administration Agenda Tous les capteurs sont Ok Angles Retour Calendrier Changer d'état Nettoyer la base de données Nettoyer la base de données Tout nettoyer Couleur Connexion Nom du pays Créer Creer association Ajouter groupe Creer regulation Crée et propulsé par l'équipe LifeBox Mode sombre Jour Supprimer Supprimer Regularisation Supprimer event Supprimer groupe Dict Different Capteur hors ligne Nom d'affichage Modifier Modifier Modifier groupe Editer Regularisation Modifer capteur Information d'urgences Économies d'énergie English Egale Inferieur ou égale Superieur ou égale Erreur Erreur, formulaire invalide, contactez LifeBox Message d'erreur Evenement Tout est correct ! Ressenti °C Français Allemand Groupe Nom du groupe MENU Expertise domotique - Made in France Status de la maison Humidité Humidité Icône Importante Capteur enregistré Association connue Lumiere On/Off Couleur de la lumiere Liste des traductions Se connecter Deconnexion Inferieur Luminosité Mesure Module Nouvelle association Ajouter groupe Nouveau capteur Aucun capteur enregistré Aucun evenement aujourd'hui Aucune notification importantes pour le moment Notification Hors ligne En ligne Nos réseaux sociaux Page introuvable Paramètres Paramètres Mot de passe Pression Pluie %% Nom du groupe Regulation Regulation Regulation Enregistrer Capteur Capteur État des capteurs Modification capteur Nom du capteur Nom du capteur Capteur Signe Supérieur Cible Temperature Temperature °C Le capteur Date Le capteur Indice UV Mise à jour Nom d'utilisateur Valeur Valeur Météo Météo & Prévisions Météo - Nom de la ville ou code postal Météo du jour Bienvenue sur votre page d'accueil Nom du Wifi (ssid) Mot de passe Wifi Mauvais utilisateur ou mot de passe Vous êtes connecté Altitude erreur, la regulation est désactivé Erreur, cible non définie est La dernière valeur est  reçu Savegardé Sauvegardé type Valeur Vent m/s 